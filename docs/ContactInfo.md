
# ContactInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**websites** | [**List&lt;Website&gt;**](Website.md) |  |  [optional]
**familyName** | **String** |  |  [optional]
**fullName** | **String** |  |  [optional]
**givenName** | **String** |  |  [optional]



