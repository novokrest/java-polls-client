
# CustomNotificationViewModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**noticationType** | **String** | Gets or sets the type of the notication. |  [optional]
**notificationTitle** | **String** | Gets or sets the notification title. |  [optional]
**notificationMessage** | **String** | Gets or sets the notification message. |  [optional]
**dateCreated** | [**DateTime**](DateTime.md) | Gets or sets the date created. |  [optional]
**pollId** | **Long** | Gets or sets the poll identifier. |  [optional]
**isDemographicCompleted** | **Boolean** | Gets or sets the is demographic completed. |  [optional]
**pollCompletedDate** | [**DateTime**](DateTime.md) | Gets or sets the poll completed date. |  [optional]
**demographicCompletedDate** | [**DateTime**](DateTime.md) | Gets or sets the demographic completed date. |  [optional]
**id** | **Long** | Gets or sets the identifier. |  [optional]
**isActive** | **Boolean** | Gets or sets the is active. |  [optional]



