
# FullContactModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **String** | The email |  [optional]
**contactId** | [**UUID**](UUID.md) | The contact identifier |  [optional]



