
# Organization

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  |  [optional]
**startDate** | **String** |  |  [optional]
**title** | **String** |  |  [optional]
**current** | **Boolean** |  |  [optional]
**endDate** | **String** |  |  [optional]



