
# PublicPollViewModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pollId** | **Long** | the poll identifier. |  [optional]
**pollTypeId** | **Long** | The poll type Id value can be 1&#x3D;SingleText, 2&#x3D;BestText, 3&#x3D; SingleImage, 4&#x3D; BestImage. |  [optional]
**createDate** | [**DateTime**](DateTime.md) | the poll create date. |  [optional]
**pollStatus** | **String** | The poll status can be \&quot;InProgress\&quot;,             \&quot;Pending\&quot; ,             \&quot;Completed\&quot;,             \&quot;Expired\&quot; ,             \&quot;Suspended\&quot;,             \&quot;Draft\&quot; , |  [optional]
**pollTitle** | **String** | the poll title. |  [optional]
**question** | **String** | the question. |  [optional]
**mainCatName** | **String** | Gets or sets the name of the main cat. |  [optional]
**catName** | **String** | Gets or sets the name of the cat. |  [optional]
**userId** | [**UUID**](UUID.md) | Gets or sets the user identifier. |  [optional]
**expirationDate** | [**DateTime**](DateTime.md) | the expiration date. |  [optional]
**pollOverview** | **String** | the poll overview. |  [optional]
**firstOption** | **String** | the first option. |  [optional]
**secondOption** | **String** | the second option. |  [optional]
**firstImagePath** | **String** | the first image path. |  [optional]
**secondImagePath** | **String** | the second image path. |  [optional]
**userName** | **String** | Gets or sets the name of the user. |  [optional]
**maxAssignments** | **Long** | Gets or sets the maximum assignments. |  [optional]
**isPublic** | **Boolean** | Gets or sets a value indicating whether this instance is public. |  [optional]
**filtersJson** | **String** | Gets or sets the filters json. |  [optional]
**responseCompleted** | **Long** | Gets or sets the response completed. |  [optional]



