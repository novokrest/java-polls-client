
# FileObject

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**UUID**](UUID.md) |  |  [optional]
**fileID** | **Long** |  |  [optional]
**contentType** | **String** |  |  [optional]
**url** | **String** |  |  [optional]
**fileName** | **String** |  |  [optional]
**fileDeleted** | **Boolean** |  |  [optional]
**lastUpdate** | [**DateTime**](DateTime.md) |  |  [optional]



