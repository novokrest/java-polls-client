
# Photo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**height** | **Long** | Gets or sets the height. |  [optional]
**htmlAttributions** | **List&lt;String&gt;** | Gets or sets the HTML attributions. |  [optional]
**photoReference** | **String** | Gets or sets the photo reference. |  [optional]
**width** | **Long** | Gets or sets the width. |  [optional]



