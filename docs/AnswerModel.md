
# AnswerModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pollId** | **Long** | the poll identifier. | 
**answerChoice** | **Integer** | the answer choice. | 
**workerUserId** | [**UUID**](UUID.md) | the user identifier. | 
**answerText** | **String** | the answer text. |  [optional]



