
# BestText

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pollTypeId** | **Integer** | The PollTypeId is integer value SingleText&#x3D;1, BestText&#x3D;2,SingleImage&#x3D;3,BestImage&#x3D;4 |  [optional]
**firstOption** | **String** | the first option. | 
**secondOption** | **String** | the second option. | 
**pollId** | **Long** | the poll identifier. |  [optional]
**pollStatus** | **String** | the poll status. |  [optional]
**pollTitle** | **String** | the poll title. | 
**question** | **String** | the question. | 
**pollOverview** | **String** | the polloverview is required to be provided. | 
**assignmentDurationInSeconds** | **Long** | the assignment duration in seconds must be between 30 and 31536000\&quot; | 
**keywords** | **String** | the keywords comma separated values. | 
**isEnabledComments** | **Boolean** | indicating whether this instance is enabled comments. |  [optional]
**expirationDate** | [**DateTime**](DateTime.md) | the expiration date. |  [optional]
**filterNameValue** | [**List&lt;FilterPairs&gt;**](FilterPairs.md) | Gets or sets the filter category ids. |  [optional]
**filterMainCategory** | **String** | Gets or sets the filter main category. |  [optional]
**pUserName** | **String** | partners UserName the username of partners user at his end. |  [optional]
**isEnablePublic** | **Boolean** | Gets or sets the is enable public. |  [optional]
**catName** | **String** | Gets or sets the name of the category. |  [optional]
**userName** | **String** | Gets or sets the name of the user. |  [optional]
**maxAssignments** | **Long** | the maximum assignments. |  [optional]
**tags** | **String** | Gets or sets the tags. | 
**lifeTimeInSeconds** | **Long** | the life time in seconds LifeTimeInSeconds must be between 30 and 31536000. | 
**isFilterOption** | **Boolean** | Gets or sets the is filter option. |  [optional]
**catId** | **Long** | Gets or sets the category identifier. |  [optional]
**isCompanyPoll** | **Boolean** | The is company poll can be selected in case of business user |  [optional]
**alllowedAnonymously** | **Integer** | The alllowed anonymously |  [optional]
**isAdult** | **Boolean** | If the poll is for adults then true else false |  [optional]
**isPublished** | **Boolean** | the is published. IsPublished &#x3D; false for Draft else true |  [optional]



