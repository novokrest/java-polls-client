
# Viewport

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**northeast** | [**Northeast**](Northeast.md) | Gets or sets the northeast. |  [optional]
**southwest** | [**Southwest**](Southwest.md) | Gets or sets the southwest. |  [optional]



