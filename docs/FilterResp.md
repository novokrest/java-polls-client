
# FilterResp

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**filterName** | **String** |  |  [optional]
**displayName** | **String** |  |  [optional]
**filterValues** | [**List&lt;FilterValues&gt;**](FilterValues.md) |  |  [optional]
**showGraph** | **Boolean** |  |  [optional]
**showFilter** | **Boolean** |  |  [optional]



