
# PaymentPlanViewModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**levelName** | **String** | Gets or sets the name of the level. |  [optional]
**levelDescription** | **String** | Gets or sets the level description. |  [optional]
**numberResponses** | **Long** | Gets or sets the number responses. |  [optional]
**credits** | **Long** | Gets or sets the credits. |  [optional]
**creditPrice** | **Double** | Gets or sets the credit price. |  [optional]
**planId** | [**UUID**](UUID.md) | Gets or sets the plan identifier. |  [optional]
**productId** | **String** | Gets or sets the product identifier. |  [optional]
**isPremium** | **Boolean** | IsPremium Level and have subscription for the plan. |  [optional]
**subscribedMethod** | **String** | Gets or sets the subscribed method. |  [optional]
**isSubsribed** | **Boolean** | Gets or sets the is subsribed. |  [optional]
**payPalPlanId** | **String** | Gets or sets the pay pal plan identifier. |  [optional]
**googlePlanId** | **String** | Gets or sets the google plan identifier. |  [optional]
**stripePlanId** | **String** | Gets or sets the stripe plan identifier. |  [optional]
**iosPlanId** | **String** | Gets or sets the ios plan identifier. |  [optional]
**osxPlanId** | **String** | Gets or sets the osx plan identifier. |  [optional]
**windowsStorePlanId** | **String** | Gets or sets the windows store plan identifier. |  [optional]



