# CheckInApi

All URIs are relative to *http://demo.honeyshyam.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**checkinLocation**](CheckInApi.md#checkinLocation) | **POST** /v1/CheckIn/CheckinLocation | Checkin Location of the user
[**getNearByLocations**](CheckInApi.md#getNearByLocations) | **POST** /v1/CheckIn/GetNearByLocations | Gets  NearBy Locations search results for the requset latitude, longiture with radius.
[**postLocation**](CheckInApi.md#postLocation) | **POST** /v1/CheckIn/PostLocation | Posts the location.


<a name="checkinLocation"></a>
# **checkinLocation**
> LocationsList checkinLocation(requestId)

Checkin Location of the user

Checkin Location of the user

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.CheckInApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

CheckInApi apiInstance = new CheckInApi();
RequestId requestId = new RequestId(); // RequestId | 
try {
    LocationsList result = apiInstance.checkinLocation(requestId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CheckInApi#checkinLocation");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestId** | [**RequestId**](RequestId.md)|  |

### Return type

[**LocationsList**](LocationsList.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getNearByLocations"></a>
# **getNearByLocations**
> GoogleLocationViewModel getNearByLocations(locationViewModel)

Gets  NearBy Locations search results for the requset latitude, longiture with radius.

Gets  NearBy Locations search results for the requset latitude, longiture with radius.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.CheckInApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

CheckInApi apiInstance = new CheckInApi();
LocationViewModel locationViewModel = new LocationViewModel(); // LocationViewModel | 
try {
    GoogleLocationViewModel result = apiInstance.getNearByLocations(locationViewModel);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CheckInApi#getNearByLocations");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **locationViewModel** | [**LocationViewModel**](LocationViewModel.md)|  |

### Return type

[**GoogleLocationViewModel**](GoogleLocationViewModel.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postLocation"></a>
# **postLocation**
> StatusResponse postLocation(checkinLocationViewModel)

Posts the location.

Posts the location.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.CheckInApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

CheckInApi apiInstance = new CheckInApi();
CheckinLocationViewModel checkinLocationViewModel = new CheckinLocationViewModel(); // CheckinLocationViewModel | 
try {
    StatusResponse result = apiInstance.postLocation(checkinLocationViewModel);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CheckInApi#postLocation");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **checkinLocationViewModel** | [**CheckinLocationViewModel**](CheckinLocationViewModel.md)|  |

### Return type

[**StatusResponse**](StatusResponse.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

