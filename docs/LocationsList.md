
# LocationsList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**list** | [**List&lt;CheckinLocationModel&gt;**](CheckinLocationModel.md) |  |  [optional]



