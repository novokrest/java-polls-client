
# MetaTagViewModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pollId** | **Long** | Gets or sets the poll identifier. | 
**title** | **String** | Gets or sets the title. |  [optional]
**author** | **String** | Gets or sets the author. |  [optional]
**subject** | **String** | Gets or sets the subject. |  [optional]
**description** | **String** | Gets or sets the description. | 
**keywords** | **String** | Gets or sets the keywords. | 
**classification** | **String** | Gets or sets the classification. |  [optional]
**geography** | **String** | Where are you located? |  [optional]
**language** | **String** | Gets or sets the language. |  [optional]
**expires** | **String** | Gets or sets the expires. |  [optional]
**copyright** | **String** | Gets or sets the copyright. |  [optional]
**designer** | **String** | Gets or sets the designer. |  [optional]
**publisher** | **String** | Gets or sets the publisher. |  [optional]
**distribution** | **String** | Gets or sets the distribution. |  [optional]
**zipcode** | **String** | Gets or sets the zipcode. |  [optional]
**city** | **String** | Gets or sets the city. |  [optional]
**country** | **String** | Gets or sets the country. |  [optional]



