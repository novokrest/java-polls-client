
# BillingAgreementViewModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**billingId** | **Long** | Gets or sets the billing identifier. |  [optional]
**userId** | [**UUID**](UUID.md) | Gets or sets the user identifier. |  [optional]
**planId** | [**UUID**](UUID.md) | Gets or sets the plan identifier. |  [optional]
**agreementId** | **String** | Gets or sets the agreement identifier. |  [optional]
**agreementStatus** | **String** | Gets or sets the agreement status. |  [optional]
**payerEmail** | **String** | Gets or sets the payer email. |  [optional]
**firstName** | **String** | Gets or sets the first name. |  [optional]
**lastName** | **String** | Gets or sets the last name. |  [optional]
**timeStamp** | **String** | Gets or sets the time stamp. |  [optional]
**dateCreated** | [**DateTime**](DateTime.md) | Gets or sets the date created. |  [optional]
**transactionType** | **String** | Gets or sets the type of the transaction. |  [optional]
**closeDate** | [**DateTime**](DateTime.md) | Gets or sets the close date. |  [optional]
**isActive** | **Boolean** | Gets or sets the is active. |  [optional]
**nextBillingDay** | **Long** | Gets or sets the next billing day. |  [optional]
**payerId** | **String** | Gets or sets the payer identifier. |  [optional]
**paymentGross** | **Double** | Gets or sets the payment gross. |  [optional]
**maxAmount** | **Double** | Gets or sets the maximum amount. |  [optional]
**paymentStatus** | **String** | Gets or sets the payment status. |  [optional]
**mpStatus** | **Long** | Gets or sets the mp status. |  [optional]
**reasonCode** | **String** | Gets or sets the reason code. |  [optional]
**mpDesc** | **String** | Gets or sets the mp desc. |  [optional]
**paymentMethod** | **String** | Gets or sets the payment method. |  [optional]
**ccBrand** | **String** | Gets or sets the c c brand. |  [optional]
**ccLAst4** | **Long** | Gets or sets the c cl ast4. |  [optional]
**ccExpMonth** | **Long** | Gets or sets the c c exp month. |  [optional]
**cCExpYear** | **Long** | Gets or sets the c c exp year. |  [optional]
**cardId** | **String** | Gets or sets the card identifier. |  [optional]



