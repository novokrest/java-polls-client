
# WorkerApiModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**workerId** | **String** |  |  [optional]
**gender** | **String** |  |  [optional]
**educationId** | **Long** |  |  [optional]
**education** | **String** |  |  [optional]
**age** | **String** |  |  [optional]
**income** | **String** |  |  [optional]
**thumbsUp** | **Long** |  |  [optional]
**thumbsDown** | **Long** |  |  [optional]



