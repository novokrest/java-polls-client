
# VerifyPaymentViewModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**receiptData** | **String** | Gets or sets the receipt_data. | 
**paymentMethod** | **String** | Gets or sets the payment method. | 
**token** | **String** | Gets or sets the token recieved from payment gateway currently required for Google. |  [optional]
**planId** | [**UUID**](UUID.md) | Gets or sets the system plan id. |  [optional]
**transactionId** | **String** | Gets or sets the transaction identifier. |  [optional]
**productId** | **String** | Gets or sets the product id for Google. |  [optional]
**appType** | **Integer** | The application type |  [optional]
**intent** | **String** | The intent |  [optional]



