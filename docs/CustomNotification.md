
# CustomNotification

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** | Gets or sets the identifier. |  [optional]
**isActive** | **Boolean** | Gets or sets the is active. |  [optional]



