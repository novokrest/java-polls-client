
# Device

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**snsDeviceId** | **String** |  | 
**channelId** | **String** |  |  [optional]
**deviceName** | **String** |  |  [optional]
**devicePlatform** | **String** |  |  [optional]
**deviceVersion** | **String** |  |  [optional]
**browserName** | **String** |  |  [optional]
**browserVersion** | **String** |  |  [optional]
**browserUserAgent** | **String** |  |  [optional]
**status** | **String** |  |  [optional]
**snsType** | **String** |  |  [optional]



