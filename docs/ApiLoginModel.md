
# ApiLoginModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**userName** | **String** | The username | 
**password** | **String** | The password | 
**newDevice** | [**DeviceModel**](DeviceModel.md) | the new device. | 
**deviceId** | [**UUID**](UUID.md) | The device identifier |  [optional]
**isPartner** | **Boolean** | Optiional a value indicating whether this instance is partner. |  [optional]



