
# ExternalProviderRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**appId** | **String** | Gets or sets the application identifier. |  [optional]
**provider** | **Integer** | Gets or sets the provider. | 
**id** | **String** | Gets or sets the identifier. | 
**email** | **String** | Gets or sets the email. |  [optional]
**phoneNumber** | **String** | Gets or sets the phone number. |  [optional]
**verifiedEmail** | **Boolean** | Gets or sets a value indicating whether [verified email]. | 
**name** | **String** | Gets or sets the name. |  [optional]
**givenName** | **String** | Gets or sets the name of the given. | 
**familyName** | **String** | Gets or sets the name of the family. | 
**accessToken** | **String** | The access token | 
**gender** | **String** | Gets or sets the gender. |  [optional]
**newDevice** | [**Device**](Device.md) | Gets or sets the new device. |  [optional]
**deviceId** | [**UUID**](UUID.md) | The device identifier |  [optional]



