
# HttpRequestMessage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**version** | [**Version**](Version.md) |  |  [optional]
**content** | [**HttpContent**](HttpContent.md) |  |  [optional]
**method** | [**HttpMethod**](HttpMethod.md) |  |  [optional]
**requestUri** | **String** |  |  [optional]
**headers** | **List&lt;String&gt;** |  |  [optional]
**properties** | [**java.util.Map**](java.util.Map.md) |  |  [optional]



