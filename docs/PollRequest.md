
# PollRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pollId** | **Long** |  |  [optional]
**pageSize** | **Long** |  |  [optional]
**pageNumber** | **Long** |  |  [optional]
**pagingOrder** | **String** |  |  [optional]
**searchParameter** | **String** |  |  [optional]
**searchType** | **String** |  |  [optional]
**viewAll** | **Boolean** |  |  [optional]



