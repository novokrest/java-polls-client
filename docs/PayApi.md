# PayApi

All URIs are relative to *http://demo.honeyshyam.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**billingHistory**](PayApi.md#billingHistory) | **POST** /v1/Pay/BillingHistory | Billings the history.
[**billingInvoice**](PayApi.md#billingInvoice) | **POST** /v1/Pay/BillingInvoice | Billings the invoice.
[**buyingHistory**](PayApi.md#buyingHistory) | **POST** /v1/Pay/BuyingHistory | Buying Credits history.
[**chargePayment**](PayApi.md#chargePayment) | **POST** /v1/Pay/ChargePayment | Charges the payment.
[**getAccessToken**](PayApi.md#getAccessToken) | **POST** /v1/Pay/GetAccessToken | Verifies the authentication code.
[**verifyPayment**](PayApi.md#verifyPayment) | **POST** /v1/Pay/VerifyPayment | Verifies the Payment.
[**windowsCollectionToken**](PayApi.md#windowsCollectionToken) | **POST** /v1/Pay/WindowsCollectionToken | Gets the windows collection token.


<a name="billingHistory"></a>
# **billingHistory**
> ListBillingHistory billingHistory(requestId)

Billings the history.

Billings the history.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PayApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PayApi apiInstance = new PayApi();
RequestId requestId = new RequestId(); // RequestId | 
try {
    ListBillingHistory result = apiInstance.billingHistory(requestId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PayApi#billingHistory");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestId** | [**RequestId**](RequestId.md)|  |

### Return type

[**ListBillingHistory**](ListBillingHistory.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="billingInvoice"></a>
# **billingInvoice**
> BillingInvoiceModel billingInvoice(invoiceRequest)

Billings the invoice.

Billings the invoice.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PayApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PayApi apiInstance = new PayApi();
InvoiceRequest invoiceRequest = new InvoiceRequest(); // InvoiceRequest | 
try {
    BillingInvoiceModel result = apiInstance.billingInvoice(invoiceRequest);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PayApi#billingInvoice");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoiceRequest** | [**InvoiceRequest**](InvoiceRequest.md)|  |

### Return type

[**BillingInvoiceModel**](BillingInvoiceModel.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="buyingHistory"></a>
# **buyingHistory**
> List&lt;BillingHistoryModel&gt; buyingHistory(requestId)

Buying Credits history.

Buying Credits history.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PayApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PayApi apiInstance = new PayApi();
RequestId requestId = new RequestId(); // RequestId | 
try {
    List<BillingHistoryModel> result = apiInstance.buyingHistory(requestId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PayApi#buyingHistory");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestId** | [**RequestId**](RequestId.md)|  |

### Return type

[**List&lt;BillingHistoryModel&gt;**](BillingHistoryModel.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="chargePayment"></a>
# **chargePayment**
> StatusResponse chargePayment(stripeViewModel)

Charges the payment.

Charges the payment.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PayApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PayApi apiInstance = new PayApi();
StripeViewModel stripeViewModel = new StripeViewModel(); // StripeViewModel | 
try {
    StatusResponse result = apiInstance.chargePayment(stripeViewModel);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PayApi#chargePayment");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **stripeViewModel** | [**StripeViewModel**](StripeViewModel.md)|  |

### Return type

[**StatusResponse**](StatusResponse.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getAccessToken"></a>
# **getAccessToken**
> AccessTokenViewModel getAccessToken(authTokenViewModel)

Verifies the authentication code.

Verifies the authentication code.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PayApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PayApi apiInstance = new PayApi();
AuthTokenViewModel authTokenViewModel = new AuthTokenViewModel(); // AuthTokenViewModel | 
try {
    AccessTokenViewModel result = apiInstance.getAccessToken(authTokenViewModel);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PayApi#getAccessToken");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authTokenViewModel** | [**AuthTokenViewModel**](AuthTokenViewModel.md)|  |

### Return type

[**AccessTokenViewModel**](AccessTokenViewModel.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="verifyPayment"></a>
# **verifyPayment**
> PayResponse verifyPayment(verifyPaymentViewModel)

Verifies the Payment.

Verifies the Payment.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PayApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PayApi apiInstance = new PayApi();
VerifyPaymentViewModel verifyPaymentViewModel = new VerifyPaymentViewModel(); // VerifyPaymentViewModel | 
try {
    PayResponse result = apiInstance.verifyPayment(verifyPaymentViewModel);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PayApi#verifyPayment");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **verifyPaymentViewModel** | [**VerifyPaymentViewModel**](VerifyPaymentViewModel.md)|  |

### Return type

[**PayResponse**](PayResponse.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="windowsCollectionToken"></a>
# **windowsCollectionToken**
> String windowsCollectionToken()

Gets the windows collection token.

Gets the windows collection token.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PayApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PayApi apiInstance = new PayApi();
try {
    String result = apiInstance.windowsCollectionToken();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PayApi#windowsCollectionToken");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**String**

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

