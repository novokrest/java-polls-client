
# GoogleLocationViewModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**List&lt;Result&gt;**](Result.md) | Gets or sets the results. |  [optional]
**status** | **String** | Gets or sets the status. |  [optional]



