
# LocationDeduced

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**normalizedLocation** | **String** |  |  [optional]
**deducedLocation** | **String** |  |  [optional]
**state** | [**State**](State.md) |  |  [optional]
**country** | [**Country**](Country.md) |  |  [optional]
**continent** | [**Continent**](Continent.md) |  |  [optional]
**likelihood** | **Double** |  |  [optional]



