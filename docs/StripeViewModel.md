
# StripeViewModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**stripeToken** | **String** | Gets or sets the stripe token. | 
**stripeEmail** | **String** | Gets or sets the stripe email. | 
**planId** | [**UUID**](UUID.md) | The plan identifier | 
**userId** | [**UUID**](UUID.md) | The user identifier | 
**chargeAmount** | **Long** | The charge amount | 
**firstName** | **String** | Gets or sets the first name. |  [optional]
**lastName** | **String** | Gets or sets the last name. |  [optional]



