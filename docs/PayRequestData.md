
# PayRequestData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**receiptData** | **String** |  |  [optional]
**appType** | **String** |  |  [optional]



