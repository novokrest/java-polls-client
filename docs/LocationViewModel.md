
# LocationViewModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**latitude** | **Double** | Gets or sets the latitude. | 
**longitude** | **Double** | Gets or sets the longitude. | 
**radius** | **Long** | Gets or sets the radius. | 



