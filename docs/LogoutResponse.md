
# LogoutResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**isLoggedOut** | **Boolean** |  |  [optional]
**status** | **String** |  |  [optional]



