
# FilterCountModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**filterNameValue** | [**List&lt;FilterPairs&gt;**](FilterPairs.md) | Gets or sets the filter value. |  [optional]



