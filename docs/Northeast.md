
# Northeast

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lat** | **Double** | Gets or sets the lat. |  [optional]
**lng** | **Double** | Gets or sets the LNG. |  [optional]



