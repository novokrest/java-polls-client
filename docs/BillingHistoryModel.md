
# BillingHistoryModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  |  [optional]
**transactionId** | **String** |  |  [optional]
**paymentDate** | [**DateTime**](DateTime.md) |  |  [optional]
**grossAmount** | **Double** |  |  [optional]
**credits** | **Long** |  |  [optional]
**paymentMethod** | **String** |  |  [optional]
**payerEmail** | **String** |  |  [optional]
**receiverEmail** | **String** |  |  [optional]
**paymentStatus** | **String** |  |  [optional]



