
# ProfileViewModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**userId** | [**UUID**](UUID.md) | Gets or sets the user identifier. |  [optional]
**companyName** | **String** | The company name | 
**jobTitle** | **String** | The job title | 
**countryCode** | **Long** | Gets or sets the country code. | 
**phoneNumber** | **String** | The phone number | 
**gender** | **String** | The gender Male/Female or M or F | 
**birthDate** | [**DateTime**](DateTime.md) | The birth date Format {0:yyyy-MM-dd} | 
**educationLevel** | **String** | The education level Values are College, Associates Degree, Bachelors /Degree, Graduate Degree, Masters Degree, Doctoral Degree (PHD), Trade/Technical/Vocational | 
**addressLine1** | **String** | Gets or sets the address line1. | 
**addressLine2** | **String** | Gets or sets the address line2. | 
**addressCity** | **String** | Gets or sets the address city. | 
**addressCountry** | **String** | Gets or sets the address country. | 
**addressState** | **String** | Gets or sets the state of the address. | 
**addressZip** | **Long** | Gets or sets the address zipCode. | 
**ownerId** | [**UUID**](UUID.md) | The owner identifier for owner account it is userId |  [optional]
**pictureUrl** | **String** | Gets or sets the picture URL. |  [optional]
**profilePictures** | [**java.util.Map**](java.util.Map.md) | Gets or sets the profile pictures. |  [optional]
**isEmailVerified** | **Boolean** | Gets a value indicating whether this instance is email verified. |  [optional]
**isPhoneVerified** | **Boolean** | Gets a value indicating whether this instance is phone verified. |  [optional]
**publicPolls** | **Long** | Gets or sets the public polls. |  [optional]
**businessPolls** | **Long** | Gets or sets the business polls. |  [optional]
**privatePolls** | **Long** | Gets or sets the private polls. |  [optional]
**totalPolls** | **Long** | Gets or sets the total polls. |  [optional]
**deletedPictures** | [**List&lt;UUID&gt;**](UUID.md) | Gets or sets the deleted pics array. |  [optional]
**lastLogin** | [**DateTime**](DateTime.md) | Get the last login. |  [optional]
**memberSince** | [**DateTime**](DateTime.md) | Gets the member since. |  [optional]
**firstName** | **String** | Gets or sets the first name. | 
**lastName** | **String** | The last name | 
**payPalEmail** | **String** | The paypal email | 
**id** | [**UUID**](UUID.md) | The identifier guid for the contact |  [optional]
**relationshipStatus** | **String** | The relationship status Pending, Accepted,None Etc. |  [optional]
**maritalStatus** | **String** | The marital status Married, Single, In Relationship Etc. |  [optional]
**pictureId** | [**UUID**](UUID.md) | The picture identifier, it is used to update the picture info, and for retrieve filename, you need to remove all (-) and add file extention to .png all files on server are saved into png format. |  [optional]
**isPublicProfile** | **Boolean** | The profile is public              value is true else false |  [optional]
**isPhonePublic** | **Boolean** | The  phone is public             value is true else false |  [optional]
**isEmailPublic** | **Boolean** | The  email is public             value is true else false |  [optional]
**viewCounter** | **Long** | The view counter for the public profile |  [optional]
**phoneType** | **Integer** | Gets or sets the type of the phone. |  [optional]



