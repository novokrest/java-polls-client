
# BillingInvoiceModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  |  [optional]
**transactionId** | **String** |  |  [optional]
**paymentDate** | [**DateTime**](DateTime.md) |  |  [optional]
**grossAmount** | **Double** |  |  [optional]
**credits** | **Long** |  |  [optional]
**paymentMethod** | **String** |  |  [optional]
**payerEmail** | **String** |  |  [optional]
**receiverEmail** | **String** |  |  [optional]
**paymentStatus** | **String** |  |  [optional]
**customerFirstName** | **String** |  |  [optional]
**customerLastName** | **String** |  |  [optional]
**billingCompanyName** | **String** |  |  [optional]
**billingAddressLine1** | **String** |  |  [optional]
**billingAddressLine2** | **String** |  |  [optional]
**billingState** | **String** |  |  [optional]
**zipCode** | **String** |  |  [optional]
**product** | **String** |  |  [optional]
**isPremium** | **Boolean** |  |  [optional]
**billingPhone** | **String** |  |  [optional]
**customerCompanyName** | **String** |  |  [optional]
**countryCode** | **Long** |  |  [optional]
**customerPhoneNumber** | **String** |  |  [optional]
**customerPostalAddress** | **String** |  |  [optional]
**orgLogoUrl** | **String** |  |  [optional]



