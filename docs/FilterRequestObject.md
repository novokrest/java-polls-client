
# FilterRequestObject

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**filterNameValue** | [**List&lt;FilterPairs&gt;**](FilterPairs.md) |  |  [optional]
**orgId** | [**UUID**](UUID.md) |  |  [optional]



