
# UpdatePollRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pollId** | **Long** |  | 
**keywords** | **String** |  | 
**pollOverview** | **String** |  | 



