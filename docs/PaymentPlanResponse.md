
# PaymentPlanResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**levelName** | **String** |  |  [optional]
**levelDescription** | **String** |  |  [optional]
**numberResponses** | **Long** |  |  [optional]
**credits** | **Long** |  |  [optional]
**creditPrice** | **Double** |  |  [optional]
**planId** | [**UUID**](UUID.md) |  |  [optional]
**productId** | **String** |  |  [optional]
**isPremium** | **Boolean** |  |  [optional]
**isSubsribed** | **Boolean** |  |  [optional]



