
# HttpResponseMessage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**version** | [**Version**](Version.md) |  |  [optional]
**content** | [**HttpContent**](HttpContent.md) |  |  [optional]
**statusCode** | **Integer** |  |  [optional]
**reasonPhrase** | **String** |  |  [optional]
**headers** | **List&lt;String&gt;** |  |  [optional]
**requestMessage** | [**HttpRequestMessage**](HttpRequestMessage.md) |  |  [optional]
**isSuccessStatusCode** | **Boolean** |  |  [optional]



