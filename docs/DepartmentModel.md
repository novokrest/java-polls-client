
# DepartmentModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**departmentId** | **Long** |  |  [optional]
**departmentName** | **String** |  |  [optional]
**createDate** | [**DateTime**](DateTime.md) |  |  [optional]
**createBy** | **String** |  |  [optional]
**isActive** | **Boolean** |  |  [optional]



