
# NotificationTrackingModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  |  [optional]
**userId** | [**UUID**](UUID.md) |  |  [optional]
**pollId** | **Long** |  |  [optional]
**messageTitle** | **String** |  |  [optional]
**messageText** | **String** |  |  [optional]
**alertType** | **String** |  |  [optional]
**dateCreated** | [**DateTime**](DateTime.md) |  |  [optional]
**channelId** | **String** |  |  [optional]



