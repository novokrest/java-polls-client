
# Contact

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**userId** | [**UUID**](UUID.md) |  |  [optional]
**countryCode** | **Long** |  | 
**phoneNumber** | **String** |  | 
**gender** | **String** |  | 
**birthDate** | **String** |  | 
**addressLine1** | **String** |  | 
**addressLine2** | **String** |  | 
**addressCity** | **String** |  | 
**addressCountry** | **String** |  | 
**addressState** | **String** |  | 
**addressZip** | **String** |  | 
**ownerId** | [**UUID**](UUID.md) |  |  [optional]
**pictureUrl** | **String** |  |  [optional]
**profilePictures** | [**java.util.Map**](java.util.Map.md) |  |  [optional]
**isEmailVerified** | **Boolean** |  |  [optional]
**isPhoneVerified** | **Boolean** |  |  [optional]
**publicPolls** | **Long** |  |  [optional]
**businessPolls** | **Long** |  |  [optional]
**privatePolls** | **Long** |  |  [optional]
**totalPolls** | **Long** |  |  [optional]
**deletedPictures** | [**List&lt;UUID&gt;**](UUID.md) |  |  [optional]
**lastLogin** | [**DateTime**](DateTime.md) |  |  [optional]
**memberSince** | [**DateTime**](DateTime.md) |  |  [optional]
**firstName** | **String** |  | 
**lastName** | **String** |  | 
**payPalEmail** | **String** |  | 
**phoneVerifiedDateTime** | [**DateTime**](DateTime.md) |  |  [optional]
**phoneVerificationMethod** | **String** |  |  [optional]
**emailVerifiedDateTime** | [**DateTime**](DateTime.md) |  |  [optional]
**id** | [**UUID**](UUID.md) |  |  [optional]
**companyName** | **String** |  |  [optional]
**jobTitle** | **String** |  |  [optional]
**educationLevel** | **String** |  |  [optional]
**relationshipStatus** | **String** |  |  [optional]
**maritalStatus** | **String** |  |  [optional]
**pictureId** | [**UUID**](UUID.md) |  |  [optional]
**isPublicProfile** | **Boolean** |  |  [optional]
**isPhonePublic** | **Boolean** |  |  [optional]
**isEmailPublic** | **Boolean** |  |  [optional]
**viewCounter** | **Long** |  |  [optional]
**phoneType** | **Integer** |  |  [optional]



