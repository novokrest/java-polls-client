
# BillingAgreementModal

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**billingId** | **Long** |  |  [optional]
**userId** | [**UUID**](UUID.md) |  |  [optional]
**planId** | [**UUID**](UUID.md) |  |  [optional]
**agreementId** | **String** |  |  [optional]
**agreementStatus** | **String** |  |  [optional]
**payerEmail** | **String** |  |  [optional]
**firstName** | **String** |  |  [optional]
**lastName** | **String** |  |  [optional]
**timeStamp** | **String** |  |  [optional]
**dateCreated** | [**DateTime**](DateTime.md) |  |  [optional]
**transactionType** | **String** |  |  [optional]
**closeDate** | [**DateTime**](DateTime.md) |  |  [optional]
**isActive** | **Boolean** |  |  [optional]
**nextBillingDay** | **Long** |  |  [optional]
**payerId** | **String** |  |  [optional]
**paymentGross** | **Double** |  |  [optional]
**maxAmount** | **Double** |  |  [optional]
**paymentStatus** | **String** |  |  [optional]
**mpStatus** | **Long** |  |  [optional]
**reasonCode** | **String** |  |  [optional]
**mpDesc** | **String** |  |  [optional]
**paymentMethod** | **String** |  |  [optional]
**ccBrand** | **String** |  |  [optional]
**ccLAst4** | **Long** |  |  [optional]
**ccExpMonth** | **Long** |  |  [optional]
**ccExpYear** | **Long** |  |  [optional]
**cardId** | **String** |  |  [optional]



