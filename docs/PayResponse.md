
# PayResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **String** |  |  [optional]
**creditBalance** | **Long** |  |  [optional]
**pollsBalance** | **Long** |  |  [optional]



