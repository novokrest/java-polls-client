
# PollsStatsCounts

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**completed** | **Long** |  |  [optional]
**draft** | **Long** |  |  [optional]
**expired** | **Long** |  |  [optional]
**inProgress** | **Long** |  |  [optional]
**pending** | **Long** |  |  [optional]
**totalPolls** | **Long** |  |  [optional]



