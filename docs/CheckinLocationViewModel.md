
# CheckinLocationViewModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** | Gets the identifier. |  [optional]
**latitude** | **Double** | Gets or sets the latitude. | 
**longitude** | **Double** | Gets or sets the longitude. | 
**ipAddress** | **String** | Gets or sets the ip address. |  [optional]
**timeStamp** | [**DateTime**](DateTime.md) | Gets or sets the time stamp. |  [optional]
**userId** | [**UUID**](UUID.md) | Gets or sets the user identifier. | 
**locationName** | **String** | Gets or sets the name of the location. |  [optional]



