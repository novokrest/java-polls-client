# BusinessApi

All URIs are relative to *http://demo.honeyshyam.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addNewEmployee**](BusinessApi.md#addNewEmployee) | **POST** /v1/Business/AddNewEmployee | Add Comapny Employee
[**companyEmployees**](BusinessApi.md#companyEmployees) | **POST** /v1/Business/CompanyEmployees | List of Company&#39;s employees.
[**createCompany**](BusinessApi.md#createCompany) | **POST** /v1/Business/CreateCompany | Creates the company.
[**getPollCounters**](BusinessApi.md#getPollCounters) | **POST** /v1/Business/GetPollCounters | Gets the poll counters.
[**postPollAnswer**](BusinessApi.md#postPollAnswer) | **POST** /v1/Business/PostPollAnswer | Posts the poll answer.CompanyEmployees
[**removeEmployee**](BusinessApi.md#removeEmployee) | **POST** /v1/Business/RemoveEmployee | Remove Employee.
[**requestBusinessAccount**](BusinessApi.md#requestBusinessAccount) | **POST** /v1/Business/RequestBusinessAccount | Creates the business account.
[**responseInvitation**](BusinessApi.md#responseInvitation) | **POST** /v1/Business/ResponseInvitation | Accept Decline Invitation.
[**updateEmployee**](BusinessApi.md#updateEmployee) | **POST** /v1/Business/UpdateEmployee | Updates the user.
[**viewEmployee**](BusinessApi.md#viewEmployee) | **POST** /v1/Business/ViewEmployee | Views the employee.


<a name="addNewEmployee"></a>
# **addNewEmployee**
> StatusResponse addNewEmployee(companyUsers)

Add Comapny Employee

Add Comapny Employee

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.BusinessApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

BusinessApi apiInstance = new BusinessApi();
CompanyUsers companyUsers = new CompanyUsers(); // CompanyUsers | 
try {
    StatusResponse result = apiInstance.addNewEmployee(companyUsers);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BusinessApi#addNewEmployee");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **companyUsers** | [**CompanyUsers**](CompanyUsers.md)|  |

### Return type

[**StatusResponse**](StatusResponse.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="companyEmployees"></a>
# **companyEmployees**
> ContactList companyEmployees(userIdModel)

List of Company&#39;s employees.

List of Company&#39;s employees.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.BusinessApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

BusinessApi apiInstance = new BusinessApi();
UserIdModel userIdModel = new UserIdModel(); // UserIdModel | 
try {
    ContactList result = apiInstance.companyEmployees(userIdModel);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BusinessApi#companyEmployees");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userIdModel** | [**UserIdModel**](UserIdModel.md)|  |

### Return type

[**ContactList**](ContactList.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="createCompany"></a>
# **createCompany**
> StatusResponse createCompany()

Creates the company.

Creates the company.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.BusinessApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

BusinessApi apiInstance = new BusinessApi();
try {
    StatusResponse result = apiInstance.createCompany();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BusinessApi#createCompany");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**StatusResponse**](StatusResponse.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getPollCounters"></a>
# **getPollCounters**
> PollsStatsCounts getPollCounters(userId)

Gets the poll counters.

Gets the poll counters.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.BusinessApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

BusinessApi apiInstance = new BusinessApi();
UUID userId = new UUID(); // UUID | The user identifier.
try {
    PollsStatsCounts result = apiInstance.getPollCounters(userId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BusinessApi#getPollCounters");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **UUID**| The user identifier. |

### Return type

[**PollsStatsCounts**](PollsStatsCounts.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postPollAnswer"></a>
# **postPollAnswer**
> StatusResponse postPollAnswer(answerModel)

Posts the poll answer.CompanyEmployees

Posts the poll answer.CompanyEmployees

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.BusinessApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

BusinessApi apiInstance = new BusinessApi();
AnswerModel answerModel = new AnswerModel(); // AnswerModel | 
try {
    StatusResponse result = apiInstance.postPollAnswer(answerModel);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BusinessApi#postPollAnswer");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **answerModel** | [**AnswerModel**](AnswerModel.md)|  |

### Return type

[**StatusResponse**](StatusResponse.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="removeEmployee"></a>
# **removeEmployee**
> StatusResponse removeEmployee(userRelation)

Remove Employee.

Remove Employee.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.BusinessApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

BusinessApi apiInstance = new BusinessApi();
UserRelation userRelation = new UserRelation(); // UserRelation | 
try {
    StatusResponse result = apiInstance.removeEmployee(userRelation);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BusinessApi#removeEmployee");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userRelation** | [**UserRelation**](UserRelation.md)|  |

### Return type

[**StatusResponse**](StatusResponse.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="requestBusinessAccount"></a>
# **requestBusinessAccount**
> StatusResponse requestBusinessAccount()

Creates the business account.

Creates the business account.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.BusinessApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

BusinessApi apiInstance = new BusinessApi();
try {
    StatusResponse result = apiInstance.requestBusinessAccount();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BusinessApi#requestBusinessAccount");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**StatusResponse**](StatusResponse.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="responseInvitation"></a>
# **responseInvitation**
> StatusResponse responseInvitation(userInvitation)

Accept Decline Invitation.

Accept Decline Invitation.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.BusinessApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

BusinessApi apiInstance = new BusinessApi();
UserInvitation userInvitation = new UserInvitation(); // UserInvitation | 
try {
    StatusResponse result = apiInstance.responseInvitation(userInvitation);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BusinessApi#responseInvitation");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userInvitation** | [**UserInvitation**](UserInvitation.md)|  |

### Return type

[**StatusResponse**](StatusResponse.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateEmployee"></a>
# **updateEmployee**
> CompanyUsers updateEmployee(companyUsers)

Updates the user.

Updates the user.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.BusinessApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

BusinessApi apiInstance = new BusinessApi();
CompanyUsers companyUsers = new CompanyUsers(); // CompanyUsers | 
try {
    CompanyUsers result = apiInstance.updateEmployee(companyUsers);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BusinessApi#updateEmployee");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **companyUsers** | [**CompanyUsers**](CompanyUsers.md)|  |

### Return type

[**CompanyUsers**](CompanyUsers.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="viewEmployee"></a>
# **viewEmployee**
> CompanyUsers viewEmployee(userIdModel)

Views the employee.

Views the employee.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.BusinessApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

BusinessApi apiInstance = new BusinessApi();
UserIdModel userIdModel = new UserIdModel(); // UserIdModel | 
try {
    CompanyUsers result = apiInstance.viewEmployee(userIdModel);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BusinessApi#viewEmployee");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userIdModel** | [**UserIdModel**](UserIdModel.md)|  |

### Return type

[**CompanyUsers**](CompanyUsers.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

