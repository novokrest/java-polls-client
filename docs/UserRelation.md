
# UserRelation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ownerId** | [**UUID**](UUID.md) | Gets or sets the owner identifier. |  [optional]
**contactId** | [**UUID**](UUID.md) | Gets or sets the user identifier. |  [optional]



