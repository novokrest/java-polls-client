
# DigitalFootprint

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**scores** | [**List&lt;Score&gt;**](Score.md) |  |  [optional]
**topics** | [**List&lt;Topic&gt;**](Topic.md) |  |  [optional]



