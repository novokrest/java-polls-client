
# User

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**emailAddress** | **String** |  |  [optional]
**userId** | [**UUID**](UUID.md) |  |  [optional]
**isActive** | **Boolean** |  |  [optional]
**userRole** | **String** |  |  [optional]
**isCanRunFreePoll** | **Boolean** |  |  [optional]
**isBuyCredits** | **Boolean** |  |  [optional]
**isPartner** | **Boolean** |  |  [optional]
**departmentId** | **Long** |  |  [optional]
**lastLogin** | [**DateTime**](DateTime.md) |  |  [optional]
**createDate** | [**DateTime**](DateTime.md) |  |  [optional]
**accessLevel** | **Long** |  |  [optional]
**id** | [**UUID**](UUID.md) |  |  [optional]
**userName** | **String** |  |  [optional]
**profileId** | [**UUID**](UUID.md) |  |  [optional]
**status** | **String** |  |  [optional]
**isEmailVerified** | **Boolean** |  |  [optional]
**token** | [**UUID**](UUID.md) |  |  [optional]
**isPhoneVerified** | **Boolean** |  |  [optional]
**isCallVerified** | **Boolean** |  |  [optional]
**source** | **String** |  |  [optional]
**displayName** | **String** |  |  [optional]
**userType** | **Integer** |  |  [optional]
**buyCompanyPolls** | **Boolean** |  |  [optional]
**buyTurkPolls** | **Boolean** |  |  [optional]
**isRequiedApproval** | **Boolean** |  |  [optional]
**org** | **String** |  |  [optional]
**emailInvites** | **Boolean** |  |  [optional]
**newProfileContact** | [**Contact**](Contact.md) |  |  [optional]
**newDevice** | [**Device**](Device.md) |  |  [optional]



