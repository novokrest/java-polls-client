
# PaymentPlansList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**item** | [**List&lt;PaymentPlanViewModel&gt;**](PaymentPlanViewModel.md) | Gets or sets the item. |  [optional]



