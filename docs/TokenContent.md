
# TokenContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**extExpiresIn** | **String** |  |  [optional]
**expiresIn** | **String** |  |  [optional]
**accessToken** | **String** |  |  [optional]
**expiresOn** | **String** |  |  [optional]
**resource** | **String** |  |  [optional]
**notBefore** | **String** |  |  [optional]
**tokenType** | **String** |  |  [optional]



