
# LoginResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**userName** | **String** |  |  [optional]
**deviceId** | [**UUID**](UUID.md) |  |  [optional]
**isPartner** | **Boolean** |  |  [optional]
**accessLevel** | **Long** |  |  [optional]
**emailAddress** | **String** |  |  [optional]
**userId** | [**UUID**](UUID.md) |  |  [optional]
**token** | [**UUID**](UUID.md) |  |  [optional]
**displayName** | **String** |  |  [optional]
**isEmailVerified** | **Boolean** |  |  [optional]
**isPhoneVerified** | **Boolean** |  |  [optional]
**isCallVerified** | **Boolean** |  |  [optional]
**userType** | **Integer** |  |  [optional]
**secretAccessKey** | **String** |  |  [optional]



