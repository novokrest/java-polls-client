
# UserInvitation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**userId** | [**UUID**](UUID.md) | Gets or sets the owner identifier. |  [optional]
**relationId** | [**UUID**](UUID.md) | Gets or sets the user identifier. |  [optional]
**isAccepted** | **Boolean** | Gets or sets a value indicating whether this instance is accepted. |  [optional]



