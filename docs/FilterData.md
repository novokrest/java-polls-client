
# FilterData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**filters** | [**List&lt;FilterResp&gt;**](FilterResp.md) |  |  [optional]



