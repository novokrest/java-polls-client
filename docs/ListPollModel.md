
# ListPollModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pollId** | **Long** | the poll identifier. |  [optional]
**hitId** | **String** | the hit identifier. |  [optional]
**pollTypeId** | **Long** | The poll type Id value can be 1&#x3D;SingleText, 2&#x3D;BestText, 3&#x3D; SingleImage, 4&#x3D; BestImage. |  [optional]
**createDate** | [**DateTime**](DateTime.md) | the poll create date. |  [optional]
**pollStatus** | **String** | The poll status can be \&quot;InProgress\&quot;,              \&quot;Pending\&quot; ,             \&quot;Completed\&quot;,              \&quot;Expired\&quot; ,             \&quot;Suspended\&quot;,              \&quot;Draft\&quot; , |  [optional]
**pollTitle** | **String** | the poll title. |  [optional]
**question** | **String** | the question. |  [optional]
**keywords** | **String** | the keywords. |  [optional]
**expirationDate** | [**DateTime**](DateTime.md) | the expiration date. |  [optional]
**pollOverview** | **String** | the poll overview. |  [optional]
**firstOption** | **String** | the first option. |  [optional]
**secondOption** | **String** | the second option. |  [optional]
**firstImagePath** | **String** | the first image path. |  [optional]
**secondImagePath** | **String** | the second image path. |  [optional]
**assignmentDurationInSeconds** | **Long** | the assignment duration in seconds. |  [optional]
**maxAssignments** | **Long** | the maximum assignments. |  [optional]
**isPublished** | **Boolean** | the is published. IsPublished &#x3D; false for Draft else true |  [optional]
**lifeTimeInSeconds** | **Long** | the life time in seconds the time of expiration from creation. |  [optional]
**choiceFirstCounter** | **Long** | the choice first counter. |  [optional]
**choiceSecondCounter** | **Long** | the choice second counter. |  [optional]
**resultCounter** | **Long** | Result Counter |  [optional]
**isFilterOption** | **Boolean** | true if this instance is filter option; otherwise, false. |  [optional]
**filterMainCategory** | **String** | the filter main category. |  [optional]
**filtersJson** | **String** | the filters json. |  [optional]
**isEnablePublic** | **Boolean** | a value indicating whether [enable public]. |  [optional]
**catId** | **Long** | the identifier cat. |  [optional]
**catName** | **String** | the name of the category. |  [optional]
**userName** | **String** | the name of the user. |  [optional]
**ownerId** | [**UUID**](UUID.md) | the owner identifier. |  [optional]
**totalCount** | **Long** | the total count. |  [optional]
**isAdult** | **Boolean** | the is adult. |  [optional]
**completedOn** | [**DateTime**](DateTime.md) | the completed on date. |  [optional]
**tags** | **String** |  |  [optional]
**alllowedAnonymously** | **Integer** | The alllowed anonymously |  [optional]
**isEnabledComments** | **Boolean** | The is enabled comments |  [optional]
**demographicCompleted** | **Long** | The demographic completed |  [optional]



