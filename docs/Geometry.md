
# Geometry

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**location** | [**Location**](Location.md) | Gets or sets the location. |  [optional]
**viewport** | [**Viewport**](Viewport.md) | Gets or sets the viewport. |  [optional]



