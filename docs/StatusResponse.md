
# StatusResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **String** |  |  [optional]
**statusMessage** | **String** |  |  [optional]



