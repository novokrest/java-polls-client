
# RemoteImageViewModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**remoteImageUrl** | **String** | Gets or sets the remote URL. | 
**accessKey** | **String** | Gets or sets the access key. | 
**isProfilePic** | **Boolean** | The is profile pic |  [optional]



