
# FilterCategoryModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**categoryName** | **String** |  |  [optional]
**filter1** | **String** |  |  [optional]
**filter2** | **String** |  |  [optional]
**filter3** | **String** |  |  [optional]
**filter4** | **String** |  |  [optional]



