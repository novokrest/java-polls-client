
# PollResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pollId** | **Long** |  |  [optional]
**ownerId** | [**UUID**](UUID.md) |  |  [optional]
**pollDetailId** | **Long** |  |  [optional]
**workerId** | **String** |  |  [optional]
**acceptTime** | [**DateTime**](DateTime.md) |  |  [optional]
**choice** | **Integer** |  |  [optional]
**answerText** | **String** |  |  [optional]
**thumbsUp** | **Long** |  |  [optional]
**thumbsDown** | **Long** |  |  [optional]
**enablePublic** | **Boolean** |  |  [optional]
**idCat** | **Long** |  |  [optional]
**gender** | **String** |  |  [optional]
**age** | **String** |  |  [optional]
**income** | **String** |  |  [optional]
**ethnicity** | **String** |  |  [optional]
**education** | **String** |  |  [optional]
**relationshipStatus** | **String** |  |  [optional]
**numberofChildren** | **String** |  |  [optional]
**smoking** | **String** |  |  [optional]
**dietType** | **String** |  |  [optional]
**homeType** | **String** |  |  [optional]
**community** | **String** |  |  [optional]
**politics** | **String** |  |  [optional]
**mobileDevice** | **String** |  |  [optional]
**literaryPreference** | **String** |  |  [optional]
**booksReadPerMonth** | **String** |  |  [optional]
**employmentStatus** | **String** |  |  [optional]
**thumbsStatus** | **Long** |  |  [optional]



