# PublicPollApi

All URIs are relative to *http://demo.honeyshyam.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getPollResult**](PublicPollApi.md#getPollResult) | **POST** /v1/PublicPoll/GetPollResult | Get the data from our service. It will requires hearder kyes userId and token and key pollId
[**getPublic**](PublicPollApi.md#getPublic) | **POST** /v1/PublicPoll/GetPublic | This is a public API to Get Public Polls for the request parameter             View all Public polls, View by Category, View by Username etc.
[**listPublicUsers**](PublicPollApi.md#listPublicUsers) | **POST** /v1/PublicPoll/ListPublicUsers | Lists the public users.
[**viewPublicProfile**](PublicPollApi.md#viewPublicProfile) | **POST** /v1/PublicPoll/ViewPublicProfile | View Public Profile details for the unique identifier, rest follow the ViewProfile.


<a name="getPollResult"></a>
# **getPollResult**
> PollResult getPollResult(pollRequest)

Get the data from our service. It will requires hearder kyes userId and token and key pollId

Get the data from our service. It will requires hearder kyes userId and token and key pollId

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PublicPollApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PublicPollApi apiInstance = new PublicPollApi();
PollRequest pollRequest = new PollRequest(); // PollRequest | 
try {
    PollResult result = apiInstance.getPollResult(pollRequest);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PublicPollApi#getPollResult");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pollRequest** | [**PollRequest**](PollRequest.md)|  |

### Return type

[**PollResult**](PollResult.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getPublic"></a>
# **getPublic**
> ListPoll getPublic(searchOptions)

This is a public API to Get Public Polls for the request parameter             View all Public polls, View by Category, View by Username etc.

This is a public API to Get Public Polls for the request parameter             View all Public polls, View by Category, View by Username etc.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PublicPollApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PublicPollApi apiInstance = new PublicPollApi();
SearchOptions searchOptions = new SearchOptions(); // SearchOptions | 
try {
    ListPoll result = apiInstance.getPublic(searchOptions);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PublicPollApi#getPublic");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **searchOptions** | [**SearchOptions**](SearchOptions.md)|  |

### Return type

[**ListPoll**](ListPoll.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="listPublicUsers"></a>
# **listPublicUsers**
> ContactList listPublicUsers(searchOptions)

Lists the public users.

Lists the public users.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PublicPollApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PublicPollApi apiInstance = new PublicPollApi();
SearchOptions searchOptions = new SearchOptions(); // SearchOptions | 
try {
    ContactList result = apiInstance.listPublicUsers(searchOptions);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PublicPollApi#listPublicUsers");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **searchOptions** | [**SearchOptions**](SearchOptions.md)|  |

### Return type

[**ContactList**](ContactList.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="viewPublicProfile"></a>
# **viewPublicProfile**
> Contact viewPublicProfile(userIdModel)

View Public Profile details for the unique identifier, rest follow the ViewProfile.

View Public Profile details for the unique identifier, rest follow the ViewProfile.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PublicPollApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PublicPollApi apiInstance = new PublicPollApi();
UserIdModel userIdModel = new UserIdModel(); // UserIdModel | 
try {
    Contact result = apiInstance.viewPublicProfile(userIdModel);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PublicPollApi#viewPublicProfile");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userIdModel** | [**UserIdModel**](UserIdModel.md)|  |

### Return type

[**Contact**](Contact.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

