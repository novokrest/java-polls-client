# PollsApi

All URIs are relative to *http://demo.honeyshyam.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**bestImagePoll**](PollsApi.md#bestImagePoll) | **POST** /v1/Polls/BestImagePoll | Create Best Image poll. Here you can compare two Images for any purpose.
[**cancelBillingAgreement**](PollsApi.md#cancelBillingAgreement) | **POST** /v1/Polls/CancelBillingAgreement | Cancels the billing agreement.
[**changePassword**](PollsApi.md#changePassword) | **POST** /v1/Polls/ChangePassword | Change Current Password
[**citiesByState**](PollsApi.md#citiesByState) | **POST** /v1/Polls/CitiesByState | Cities the by state identifier or Name.
[**countries**](PollsApi.md#countries) | **POST** /v1/Polls/Countries | Countries list
[**createBestTextPoll**](PollsApi.md#createBestTextPoll) | **POST** /v1/Polls/CreateBestTextPoll | Creates the best text poll.
[**createSingleImagePoll**](PollsApi.md#createSingleImagePoll) | **POST** /v1/Polls/CreateSingleImagePoll | Creates the Single Image Poll.
[**createTextPoll**](PollsApi.md#createTextPoll) | **POST** /v1/Polls/CreateTextPoll | Create the text poll. Here you can get views e.g. Book Titles, movie title, punch line and many more.
[**deleteDraftPoll**](PollsApi.md#deleteDraftPoll) | **POST** /v1/Polls/DeleteDraftPoll | Deletes the draft poll required PollId integer value
[**deletePoll**](PollsApi.md#deletePoll) | **POST** /v1/Polls/DeletePoll | Deletes the poll from everywhere.
[**deleteProfilePic**](PollsApi.md#deleteProfilePic) | **POST** /v1/Polls/DeleteProfilePic | Views the profile pics.
[**deleteUser**](PollsApi.md#deleteUser) | **POST** /v1/Polls/DeleteUser | Deletes the user.
[**expirePoll**](PollsApi.md#expirePoll) | **POST** /v1/Polls/ExpirePoll | Expires the poll Required PollId integer value.
[**extendExpireTime**](PollsApi.md#extendExpireTime) | **POST** /v1/Polls/ExtendExpireTime | Extends the expire time.              Rules of extend.             1. Can only extend if not complete             2. Can only extend if last response is within 120 days.
[**filterViewByPollId**](PollsApi.md#filterViewByPollId) | **POST** /v1/Polls/FilterViewByPollId | Gets Filtered View By PollId
[**fullFilterViewByPollId**](PollsApi.md#fullFilterViewByPollId) | **POST** /v1/Polls/FullFilterViewByPollId | Gets Full Filter View By PollId
[**getAllDepartment**](PollsApi.md#getAllDepartment) | **POST** /v1/Polls/GetAllDepartment | Gets All Department.
[**getAllFilterCategories**](PollsApi.md#getAllFilterCategories) | **POST** /v1/Polls/GetAllFilterCategories | Gets all  Filter Categories.
[**getAllFilters**](PollsApi.md#getAllFilters) | **POST** /v1/Polls/GetAllFilters | Gets all filters.
[**getBalance**](PollsApi.md#getBalance) | **POST** /v1/Polls/GetBalance | Get Credit Balance to create Poll.
[**getCategories**](PollsApi.md#getCategories) | **POST** /v1/Polls/GetCategories | Get all categories.
[**getDemographicStats**](PollsApi.md#getDemographicStats) | **POST** /v1/Polls/GetDemographicStats | Gets the demographic stats and status.
[**getEducations**](PollsApi.md#getEducations) | **POST** /v1/Polls/GetEducations | Get the educations.
[**getEmployeeFilterCount**](PollsApi.md#getEmployeeFilterCount) | **POST** /v1/Polls/GetEmployeeFilterCount | Gets the employee filter count.
[**getFileDetails**](PollsApi.md#getFileDetails) | **POST** /v1/Polls/GetFileDetails | Gets the file details.
[**getFilterDataCount**](PollsApi.md#getFilterDataCount) | **POST** /v1/Polls/GetFilterDataCount | Gets the filter data count.
[**getFilterOptions**](PollsApi.md#getFilterOptions) | **POST** /v1/Polls/GetFilterOptions | Gets all filters.
[**getFilterOptionsList**](PollsApi.md#getFilterOptionsList) | **POST** /v1/Polls/GetFilterOptionsList | Gets the filter options list.
[**getListPublicProfile**](PollsApi.md#getListPublicProfile) | **POST** /v1/Polls/GetListPublicProfile | Gets the list public profile.
[**getLoginHistory**](PollsApi.md#getLoginHistory) | **POST** /v1/Polls/GetLoginHistory | Gets the login history.
[**getMetaTags**](PollsApi.md#getMetaTags) | **POST** /v1/Polls/GetMetaTags | Gets the meta tags.
[**getMyAllPolls**](PollsApi.md#getMyAllPolls) | **POST** /v1/Polls/GetMyAllPolls | Gets my all polls.
[**getMyBillingAgreement**](PollsApi.md#getMyBillingAgreement) | **POST** /v1/Polls/GetMyBillingAgreement | Gets my billing agreement.
[**getMyPolls**](PollsApi.md#getMyPolls) | **POST** /v1/Polls/GetMyPolls | Get the all Polls of the user. It will requires hearder kyes userId and token
[**getPaymentPlans**](PollsApi.md#getPaymentPlans) | **POST** /v1/Polls/GetPaymentPlans | Gets the Payment Plans details.
[**getPollById**](PollsApi.md#getPollById) | **POST** /v1/Polls/GetPollById | Gets the poll by identifier.
[**getPollCounters**](PollsApi.md#getPollCounters) | **POST** /v1/Polls/GetPollCounters | Gets the poll counters of the user.
[**getPollResult**](PollsApi.md#getPollResult) | **POST** /v1/Polls/GetPollResult | Get the data from our service. It will requires hearder kyes userId and token and key pollId
[**getPollStatus**](PollsApi.md#getPollStatus) | **POST** /v1/Polls/GetPollStatus | Gets the poll status.
[**getPublicPollByQuery**](PollsApi.md#getPublicPollByQuery) | **POST** /v1/Polls/GetPublicPollByQuery | Get Public Polls for the request parameter
[**getQuestions**](PollsApi.md#getQuestions) | **POST** /v1/Polls/GetQuestions | Gets the questions.
[**getUserFullContact**](PollsApi.md#getUserFullContact) | **POST** /v1/Polls/GetUserFullContact | Gets the user full contact.
[**getWorkerDetails**](PollsApi.md#getWorkerDetails) | **POST** /v1/Polls/GetWorkerDetails | Gets the worker details.
[**listPartnerPoll**](PollsApi.md#listPartnerPoll) | **POST** /v1/Polls/ListPartnerPoll | Lists the partner poll.
[**logOut**](PollsApi.md#logOut) | **POST** /v1/Polls/LogOut | Logout User if successful remove any existing token, Need Header userId and token
[**login**](PollsApi.md#login) | **POST** /v1/Polls/Login | No header required with \&quot;version\&quot; value for current version &#x3D; \&quot;1.0\&quot; now using /api/v1/ for version             Accepts a login json object and returns the LoginResponse object if successful.
[**makeCall**](PollsApi.md#makeCall) | **POST** /v1/Polls/MakeCall | Makes the call.
[**metaTags**](PollsApi.md#metaTags) | **POST** /v1/Polls/MetaTags | Save Meta tags for the poll
[**postAuth**](PollsApi.md#postAuth) | **POST** /v1/Polls/PostAuth | Posts the authentication.
[**reSendVerifyEmail**](PollsApi.md#reSendVerifyEmail) | **POST** /v1/Polls/ReSendVerifyEmail | Res the send verify email.
[**refreshSecretAccessKey**](PollsApi.md#refreshSecretAccessKey) | **POST** /v1/Polls/RefreshSecretAccessKey | Gets the secret access key.             To Update SecretAccessKey request the method with extra header             To Refresh the SecretAccessKey request addition header values  \&quot;accessTokenKey\&quot; &#x3D; Existing Value             And refreshTokenKey &#x3D; \&quot;true\&quot;
[**registerNotifications**](PollsApi.md#registerNotifications) | **POST** /v1/Polls/RegisterNotifications | Register Notifications Preference
[**registration**](PollsApi.md#registration) | **POST** /v1/Polls/Registration | Register new user.
[**resendVerifyCode**](PollsApi.md#resendVerifyCode) | **POST** /v1/Polls/ResendVerifyCode | Resend the verify code.
[**resetPassword**](PollsApi.md#resetPassword) | **POST** /v1/Polls/ResetPassword | Resets the password.
[**resetPasswordRequest**](PollsApi.md#resetPasswordRequest) | **POST** /v1/Polls/ResetPasswordRequest | Reset Password Request
[**saveBillingAgreement**](PollsApi.md#saveBillingAgreement) | **POST** /v1/Polls/SaveBillingAgreement | Saves the billing agreement.
[**saveRemoteImage**](PollsApi.md#saveRemoteImage) | **POST** /v1/Polls/SaveRemoteImage | Saves the remote image.
[**statesByCountry**](PollsApi.md#statesByCountry) | **POST** /v1/Polls/StatesByCountry | States by country identifier or Name.
[**thumbsUpDown**](PollsApi.md#thumbsUpDown) | **POST** /v1/Polls/ThumbsUpDown | Thumbs up down Selection [ThumbsUp&#x3D;1, otherwize ThumbsDown&#x3D;0].
[**updateProfile**](PollsApi.md#updateProfile) | **POST** /v1/Polls/UpdateProfile | Update User Profile
[**updatePublishedPoll**](PollsApi.md#updatePublishedPoll) | **POST** /v1/Polls/UpdatePublishedPoll | Update Published Poll Keywords and Description.
[**uploadFile**](PollsApi.md#uploadFile) | **POST** /v1/Polls/UploadFile | Post File to server will return filename saved, for Profile image send header isProfilePic&#x3D;true
[**verifyEmail**](PollsApi.md#verifyEmail) | **POST** /v1/Polls/VerifyEmail | Verify Email Addresss for the account
[**verifyPhone**](PollsApi.md#verifyPhone) | **POST** /v1/Polls/VerifyPhone | Verifies Phone with the code received over phone to use your account.
[**viewNotificationsPreference**](PollsApi.md#viewNotificationsPreference) | **POST** /v1/Polls/ViewNotificationsPreference | Register Notifications Preference
[**viewProfile**](PollsApi.md#viewProfile) | **POST** /v1/Polls/ViewProfile | View User Profile details for the unique identifier, if the response exists  pictureId info, and for retrieve filename, you need to remove all (-) and add file extention to .png all files on server are saved into png format.
[**viewPublicProfile**](PollsApi.md#viewPublicProfile) | **POST** /v1/Polls/ViewPublicProfile | View Public Profile details for the unique identifier, rest follow the ViewProfile.


<a name="bestImagePoll"></a>
# **bestImagePoll**
> StatusResponse bestImagePoll(bestImage)

Create Best Image poll. Here you can compare two Images for any purpose.

Create Best Image poll. Here you can compare two Images for any purpose.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
BestImage bestImage = new BestImage(); // BestImage | 
try {
    StatusResponse result = apiInstance.bestImagePoll(bestImage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#bestImagePoll");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **bestImage** | [**BestImage**](BestImage.md)|  |

### Return type

[**StatusResponse**](StatusResponse.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="cancelBillingAgreement"></a>
# **cancelBillingAgreement**
> StatusResponse cancelBillingAgreement()

Cancels the billing agreement.

Cancels the billing agreement.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
try {
    StatusResponse result = apiInstance.cancelBillingAgreement();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#cancelBillingAgreement");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**StatusResponse**](StatusResponse.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="changePassword"></a>
# **changePassword**
> StatusResponse changePassword(changePasswordModel)

Change Current Password

Change Current Password

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
ChangePasswordModel changePasswordModel = new ChangePasswordModel(); // ChangePasswordModel | 
try {
    StatusResponse result = apiInstance.changePassword(changePasswordModel);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#changePassword");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **changePasswordModel** | [**ChangePasswordModel**](ChangePasswordModel.md)|  |

### Return type

[**StatusResponse**](StatusResponse.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="citiesByState"></a>
# **citiesByState**
> List&lt;CitiesModel&gt; citiesByState(requestNameModel)

Cities the by state identifier or Name.

Cities the by state identifier or Name.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
RequestNameModel requestNameModel = new RequestNameModel(); // RequestNameModel | 
try {
    List<CitiesModel> result = apiInstance.citiesByState(requestNameModel);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#citiesByState");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestNameModel** | [**RequestNameModel**](RequestNameModel.md)|  |

### Return type

[**List&lt;CitiesModel&gt;**](CitiesModel.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="countries"></a>
# **countries**
> CountriesModelList countries()

Countries list

Countries list

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
try {
    CountriesModelList result = apiInstance.countries();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#countries");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**CountriesModelList**](CountriesModelList.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="createBestTextPoll"></a>
# **createBestTextPoll**
> StatusResponse createBestTextPoll(bestText)

Creates the best text poll.

Creates the best text poll.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
BestText bestText = new BestText(); // BestText | 
try {
    StatusResponse result = apiInstance.createBestTextPoll(bestText);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#createBestTextPoll");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **bestText** | [**BestText**](BestText.md)|  |

### Return type

[**StatusResponse**](StatusResponse.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="createSingleImagePoll"></a>
# **createSingleImagePoll**
> StatusResponse createSingleImagePoll(singleImage)

Creates the Single Image Poll.

Creates the Single Image Poll.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
SingleImage singleImage = new SingleImage(); // SingleImage | 
try {
    StatusResponse result = apiInstance.createSingleImagePoll(singleImage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#createSingleImagePoll");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **singleImage** | [**SingleImage**](SingleImage.md)|  |

### Return type

[**StatusResponse**](StatusResponse.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="createTextPoll"></a>
# **createTextPoll**
> StatusResponse createTextPoll(singleText)

Create the text poll. Here you can get views e.g. Book Titles, movie title, punch line and many more.

Create the text poll. Here you can get views e.g. Book Titles, movie title, punch line and many more.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
SingleText singleText = new SingleText(); // SingleText | 
try {
    StatusResponse result = apiInstance.createTextPoll(singleText);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#createTextPoll");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **singleText** | [**SingleText**](SingleText.md)|  |

### Return type

[**StatusResponse**](StatusResponse.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deleteDraftPoll"></a>
# **deleteDraftPoll**
> StatusResponse deleteDraftPoll(requestPollId)

Deletes the draft poll required PollId integer value

Deletes the draft poll required PollId integer value

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
RequestPollId requestPollId = new RequestPollId(); // RequestPollId | 
try {
    StatusResponse result = apiInstance.deleteDraftPoll(requestPollId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#deleteDraftPoll");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestPollId** | [**RequestPollId**](RequestPollId.md)|  |

### Return type

[**StatusResponse**](StatusResponse.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deletePoll"></a>
# **deletePoll**
> StatusResponse deletePoll(requestPollId)

Deletes the poll from everywhere.

Deletes the poll from everywhere.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
RequestPollId requestPollId = new RequestPollId(); // RequestPollId | 
try {
    StatusResponse result = apiInstance.deletePoll(requestPollId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#deletePoll");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestPollId** | [**RequestPollId**](RequestPollId.md)|  |

### Return type

[**StatusResponse**](StatusResponse.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deleteProfilePic"></a>
# **deleteProfilePic**
> StatusResponse deleteProfilePic(fileUniqueId)

Views the profile pics.

Views the profile pics.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
UUID fileUniqueId = new UUID(); // UUID | file Unique Id
try {
    StatusResponse result = apiInstance.deleteProfilePic(fileUniqueId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#deleteProfilePic");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fileUniqueId** | **UUID**| file Unique Id |

### Return type

[**StatusResponse**](StatusResponse.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deleteUser"></a>
# **deleteUser**
> StatusResponse deleteUser()

Deletes the user.

Deletes the user.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
try {
    StatusResponse result = apiInstance.deleteUser();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#deleteUser");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**StatusResponse**](StatusResponse.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="expirePoll"></a>
# **expirePoll**
> PollResponse expirePoll(requestPollId)

Expires the poll Required PollId integer value.

Expires the poll Required PollId integer value.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
RequestPollId requestPollId = new RequestPollId(); // RequestPollId | 
try {
    PollResponse result = apiInstance.expirePoll(requestPollId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#expirePoll");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestPollId** | [**RequestPollId**](RequestPollId.md)|  |

### Return type

[**PollResponse**](PollResponse.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="extendExpireTime"></a>
# **extendExpireTime**
> StatusResponse extendExpireTime(extendPollRequest)

Extends the expire time.              Rules of extend.             1. Can only extend if not complete             2. Can only extend if last response is within 120 days.

Extends the expire time.              Rules of extend.             1. Can only extend if not complete             2. Can only extend if last response is within 120 days.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
ExtendPollRequest extendPollRequest = new ExtendPollRequest(); // ExtendPollRequest | 
try {
    StatusResponse result = apiInstance.extendExpireTime(extendPollRequest);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#extendExpireTime");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **extendPollRequest** | [**ExtendPollRequest**](ExtendPollRequest.md)|  |

### Return type

[**StatusResponse**](StatusResponse.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="filterViewByPollId"></a>
# **filterViewByPollId**
> FilterData filterViewByPollId(requestPollId)

Gets Filtered View By PollId

Gets Filtered View By PollId

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
RequestPollId requestPollId = new RequestPollId(); // RequestPollId | 
try {
    FilterData result = apiInstance.filterViewByPollId(requestPollId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#filterViewByPollId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestPollId** | [**RequestPollId**](RequestPollId.md)|  |

### Return type

[**FilterData**](FilterData.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="fullFilterViewByPollId"></a>
# **fullFilterViewByPollId**
> FilterData fullFilterViewByPollId(requestPollId)

Gets Full Filter View By PollId

Gets Full Filter View By PollId

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
RequestPollId requestPollId = new RequestPollId(); // RequestPollId | 
try {
    FilterData result = apiInstance.fullFilterViewByPollId(requestPollId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#fullFilterViewByPollId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestPollId** | [**RequestPollId**](RequestPollId.md)|  |

### Return type

[**FilterData**](FilterData.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getAllDepartment"></a>
# **getAllDepartment**
> DepartmentModel getAllDepartment()

Gets All Department.

Gets All Department.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
try {
    DepartmentModel result = apiInstance.getAllDepartment();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#getAllDepartment");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**DepartmentModel**](DepartmentModel.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getAllFilterCategories"></a>
# **getAllFilterCategories**
> FilterCategoryModel getAllFilterCategories()

Gets all  Filter Categories.

Gets all  Filter Categories.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
try {
    FilterCategoryModel result = apiInstance.getAllFilterCategories();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#getAllFilterCategories");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**FilterCategoryModel**](FilterCategoryModel.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getAllFilters"></a>
# **getAllFilters**
> FilterData getAllFilters(filterRequest)

Gets all filters.

Gets all filters.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
FilterRequest filterRequest = new FilterRequest(); // FilterRequest | 
try {
    FilterData result = apiInstance.getAllFilters(filterRequest);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#getAllFilters");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filterRequest** | [**FilterRequest**](FilterRequest.md)|  |

### Return type

[**FilterData**](FilterData.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getBalance"></a>
# **getBalance**
> UserCreditModel getBalance()

Get Credit Balance to create Poll.

Get Credit Balance to create Poll.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
try {
    UserCreditModel result = apiInstance.getBalance();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#getBalance");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**UserCreditModel**](UserCreditModel.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getCategories"></a>
# **getCategories**
> List&lt;CategoryList&gt; getCategories()

Get all categories.

Get all categories.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
try {
    List<CategoryList> result = apiInstance.getCategories();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#getCategories");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List&lt;CategoryList&gt;**](CategoryList.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getDemographicStats"></a>
# **getDemographicStats**
> DemographicStats getDemographicStats(requestPollId)

Gets the demographic stats and status.

Gets the demographic stats and status.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
RequestPollId requestPollId = new RequestPollId(); // RequestPollId | 
try {
    DemographicStats result = apiInstance.getDemographicStats(requestPollId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#getDemographicStats");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestPollId** | [**RequestPollId**](RequestPollId.md)|  |

### Return type

[**DemographicStats**](DemographicStats.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getEducations"></a>
# **getEducations**
> EducationList getEducations()

Get the educations.

Get the educations.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
try {
    EducationList result = apiInstance.getEducations();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#getEducations");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**EducationList**](EducationList.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getEmployeeFilterCount"></a>
# **getEmployeeFilterCount**
> StatusResponse getEmployeeFilterCount(filterRequestObject)

Gets the employee filter count.

Gets the employee filter count.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
FilterRequestObject filterRequestObject = new FilterRequestObject(); // FilterRequestObject | 
try {
    StatusResponse result = apiInstance.getEmployeeFilterCount(filterRequestObject);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#getEmployeeFilterCount");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filterRequestObject** | [**FilterRequestObject**](FilterRequestObject.md)|  |

### Return type

[**StatusResponse**](StatusResponse.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getFileDetails"></a>
# **getFileDetails**
> FileObject getFileDetails(fileRequest)

Gets the file details.

Gets the file details.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
FileRequest fileRequest = new FileRequest(); // FileRequest | 
try {
    FileObject result = apiInstance.getFileDetails(fileRequest);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#getFileDetails");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fileRequest** | [**FileRequest**](FileRequest.md)|  |

### Return type

[**FileObject**](FileObject.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getFilterDataCount"></a>
# **getFilterDataCount**
> FilterDataCount getFilterDataCount(filterCountModel)

Gets the filter data count.

Gets the filter data count.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
FilterCountModel filterCountModel = new FilterCountModel(); // FilterCountModel | 
try {
    FilterDataCount result = apiInstance.getFilterDataCount(filterCountModel);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#getFilterDataCount");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filterCountModel** | [**FilterCountModel**](FilterCountModel.md)|  |

### Return type

[**FilterDataCount**](FilterDataCount.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getFilterOptions"></a>
# **getFilterOptions**
> List&lt;FilterDataModel&gt; getFilterOptions(filterRequest)

Gets all filters.

Gets all filters.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
FilterRequest filterRequest = new FilterRequest(); // FilterRequest | 
try {
    List<FilterDataModel> result = apiInstance.getFilterOptions(filterRequest);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#getFilterOptions");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filterRequest** | [**FilterRequest**](FilterRequest.md)|  |

### Return type

[**List&lt;FilterDataModel&gt;**](FilterDataModel.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getFilterOptionsList"></a>
# **getFilterOptionsList**
> List&lt;FilterDataModel&gt; getFilterOptionsList(filterRequest)

Gets the filter options list.

Gets the filter options list.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
FilterRequest filterRequest = new FilterRequest(); // FilterRequest | 
try {
    List<FilterDataModel> result = apiInstance.getFilterOptionsList(filterRequest);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#getFilterOptionsList");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filterRequest** | [**FilterRequest**](FilterRequest.md)|  |

### Return type

[**List&lt;FilterDataModel&gt;**](FilterDataModel.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getListPublicProfile"></a>
# **getListPublicProfile**
> ContactList getListPublicProfile()

Gets the list public profile.

Gets the list public profile.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
try {
    ContactList result = apiInstance.getListPublicProfile();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#getListPublicProfile");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ContactList**](ContactList.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getLoginHistory"></a>
# **getLoginHistory**
> List&lt;DeviceInfoModel&gt; getLoginHistory()

Gets the login history.

Gets the login history.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
try {
    List<DeviceInfoModel> result = apiInstance.getLoginHistory();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#getLoginHistory");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List&lt;DeviceInfoModel&gt;**](DeviceInfoModel.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getMetaTags"></a>
# **getMetaTags**
> MetaTagViewModel getMetaTags(pollId)

Gets the meta tags.

Gets the meta tags.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
Long pollId = 789L; // Long | The poll identifier.
try {
    MetaTagViewModel result = apiInstance.getMetaTags(pollId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#getMetaTags");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pollId** | **Long**| The poll identifier. |

### Return type

[**MetaTagViewModel**](MetaTagViewModel.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getMyAllPolls"></a>
# **getMyAllPolls**
> ListPollModel getMyAllPolls(pollRequest)

Gets my all polls.

Gets my all polls.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
PollRequest pollRequest = new PollRequest(); // PollRequest | 
try {
    ListPollModel result = apiInstance.getMyAllPolls(pollRequest);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#getMyAllPolls");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pollRequest** | [**PollRequest**](PollRequest.md)|  |

### Return type

[**ListPollModel**](ListPollModel.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getMyBillingAgreement"></a>
# **getMyBillingAgreement**
> BillingAgreementViewModel getMyBillingAgreement()

Gets my billing agreement.

Gets my billing agreement.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
try {
    BillingAgreementViewModel result = apiInstance.getMyBillingAgreement();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#getMyBillingAgreement");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**BillingAgreementViewModel**](BillingAgreementViewModel.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getMyPolls"></a>
# **getMyPolls**
> List&lt;ListPollModel&gt; getMyPolls(pollRequest)

Get the all Polls of the user. It will requires hearder kyes userId and token

Get the all Polls of the user. It will requires hearder kyes userId and token

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
PollRequest pollRequest = new PollRequest(); // PollRequest | 
try {
    List<ListPollModel> result = apiInstance.getMyPolls(pollRequest);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#getMyPolls");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pollRequest** | [**PollRequest**](PollRequest.md)|  |

### Return type

[**List&lt;ListPollModel&gt;**](ListPollModel.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getPaymentPlans"></a>
# **getPaymentPlans**
> PaymentPlansList getPaymentPlans()

Gets the Payment Plans details.

Gets the Payment Plans details.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
try {
    PaymentPlansList result = apiInstance.getPaymentPlans();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#getPaymentPlans");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**PaymentPlansList**](PaymentPlansList.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getPollById"></a>
# **getPollById**
> ListPollModel getPollById(requestPollId)

Gets the poll by identifier.

Gets the poll by identifier.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
RequestPollId requestPollId = new RequestPollId(); // RequestPollId | 
try {
    ListPollModel result = apiInstance.getPollById(requestPollId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#getPollById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestPollId** | [**RequestPollId**](RequestPollId.md)|  |

### Return type

[**ListPollModel**](ListPollModel.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getPollCounters"></a>
# **getPollCounters**
> PollsStatsCounts getPollCounters()

Gets the poll counters of the user.

Gets the poll counters of the user.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
try {
    PollsStatsCounts result = apiInstance.getPollCounters();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#getPollCounters");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**PollsStatsCounts**](PollsStatsCounts.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getPollResult"></a>
# **getPollResult**
> PollResult getPollResult(pollRequest)

Get the data from our service. It will requires hearder kyes userId and token and key pollId

Get the data from our service. It will requires hearder kyes userId and token and key pollId

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
PollRequest pollRequest = new PollRequest(); // PollRequest | 
try {
    PollResult result = apiInstance.getPollResult(pollRequest);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#getPollResult");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pollRequest** | [**PollRequest**](PollRequest.md)|  |

### Return type

[**PollResult**](PollResult.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getPollStatus"></a>
# **getPollStatus**
> StatusResponse getPollStatus(requestPollId)

Gets the poll status.

Gets the poll status.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
RequestPollId requestPollId = new RequestPollId(); // RequestPollId | 
try {
    StatusResponse result = apiInstance.getPollStatus(requestPollId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#getPollStatus");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestPollId** | [**RequestPollId**](RequestPollId.md)|  |

### Return type

[**StatusResponse**](StatusResponse.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getPublicPollByQuery"></a>
# **getPublicPollByQuery**
> List&lt;ListPollModel&gt; getPublicPollByQuery(searchOptions)

Get Public Polls for the request parameter

Get Public Polls for the request parameter

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
SearchOptions searchOptions = new SearchOptions(); // SearchOptions | 
try {
    List<ListPollModel> result = apiInstance.getPublicPollByQuery(searchOptions);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#getPublicPollByQuery");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **searchOptions** | [**SearchOptions**](SearchOptions.md)|  |

### Return type

[**List&lt;ListPollModel&gt;**](ListPollModel.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getQuestions"></a>
# **getQuestions**
> List&lt;SuggestQuestionViewModel&gt; getQuestions()

Gets the questions.

Gets the questions.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
try {
    List<SuggestQuestionViewModel> result = apiInstance.getQuestions();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#getQuestions");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List&lt;SuggestQuestionViewModel&gt;**](SuggestQuestionViewModel.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getUserFullContact"></a>
# **getUserFullContact**
> FullContact getUserFullContact(fullContactModel)

Gets the user full contact.

Gets the user full contact.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
FullContactModel fullContactModel = new FullContactModel(); // FullContactModel | 
try {
    FullContact result = apiInstance.getUserFullContact(fullContactModel);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#getUserFullContact");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fullContactModel** | [**FullContactModel**](FullContactModel.md)|  |

### Return type

[**FullContact**](FullContact.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getWorkerDetails"></a>
# **getWorkerDetails**
> WorkerApiModel getWorkerDetails(workerRequest)

Gets the worker details.

Gets the worker details.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
WorkerRequest workerRequest = new WorkerRequest(); // WorkerRequest | 
try {
    WorkerApiModel result = apiInstance.getWorkerDetails(workerRequest);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#getWorkerDetails");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workerRequest** | [**WorkerRequest**](WorkerRequest.md)|  |

### Return type

[**WorkerApiModel**](WorkerApiModel.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="listPartnerPoll"></a>
# **listPartnerPoll**
> ListPollView listPartnerPoll(pollRequest)

Lists the partner poll.

Lists the partner poll.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
PollRequest pollRequest = new PollRequest(); // PollRequest | 
try {
    ListPollView result = apiInstance.listPartnerPoll(pollRequest);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#listPartnerPoll");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pollRequest** | [**PollRequest**](PollRequest.md)|  |

### Return type

[**ListPollView**](ListPollView.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="logOut"></a>
# **logOut**
> LogoutResponse logOut()

Logout User if successful remove any existing token, Need Header userId and token

Logout User if successful remove any existing token, Need Header userId and token

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
try {
    LogoutResponse result = apiInstance.logOut();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#logOut");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**LogoutResponse**](LogoutResponse.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="login"></a>
# **login**
> LoginResponse login(apiLoginModel)

No header required with \&quot;version\&quot; value for current version &#x3D; \&quot;1.0\&quot; now using /api/v1/ for version             Accepts a login json object and returns the LoginResponse object if successful.

No header required with \&quot;version\&quot; value for current version &#x3D; \&quot;1.0\&quot; now using /api/v1/ for version             Accepts a login json object and returns the LoginResponse object if successful.

### Example
```java
// Import classes:
//import polls.client.ApiException;
//import polls.client.api.PollsApi;


PollsApi apiInstance = new PollsApi();
ApiLoginModel apiLoginModel = new ApiLoginModel(); // ApiLoginModel | 
try {
    LoginResponse result = apiInstance.login(apiLoginModel);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#login");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **apiLoginModel** | [**ApiLoginModel**](ApiLoginModel.md)|  |

### Return type

[**LoginResponse**](LoginResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="makeCall"></a>
# **makeCall**
> StatusResponse makeCall(verifyViewModel)

Makes the call.

Makes the call.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
VerifyViewModel verifyViewModel = new VerifyViewModel(); // VerifyViewModel | 
try {
    StatusResponse result = apiInstance.makeCall(verifyViewModel);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#makeCall");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **verifyViewModel** | [**VerifyViewModel**](VerifyViewModel.md)|  |

### Return type

[**StatusResponse**](StatusResponse.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="metaTags"></a>
# **metaTags**
> MetaTagViewModel metaTags(metaTagViewModel)

Save Meta tags for the poll

Save Meta tags for the poll

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
MetaTagViewModel metaTagViewModel = new MetaTagViewModel(); // MetaTagViewModel | 
try {
    MetaTagViewModel result = apiInstance.metaTags(metaTagViewModel);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#metaTags");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **metaTagViewModel** | [**MetaTagViewModel**](MetaTagViewModel.md)|  |

### Return type

[**MetaTagViewModel**](MetaTagViewModel.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postAuth"></a>
# **postAuth**
> LoginResponse postAuth(externalProviderRequest)

Posts the authentication.

Posts the authentication.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
ExternalProviderRequest externalProviderRequest = new ExternalProviderRequest(); // ExternalProviderRequest | 
try {
    LoginResponse result = apiInstance.postAuth(externalProviderRequest);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#postAuth");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **externalProviderRequest** | [**ExternalProviderRequest**](ExternalProviderRequest.md)|  |

### Return type

[**LoginResponse**](LoginResponse.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="reSendVerifyEmail"></a>
# **reSendVerifyEmail**
> StatusResponse reSendVerifyEmail(emailViewModel)

Res the send verify email.

Res the send verify email.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
EmailViewModel emailViewModel = new EmailViewModel(); // EmailViewModel | 
try {
    StatusResponse result = apiInstance.reSendVerifyEmail(emailViewModel);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#reSendVerifyEmail");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **emailViewModel** | [**EmailViewModel**](EmailViewModel.md)|  |

### Return type

[**StatusResponse**](StatusResponse.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="refreshSecretAccessKey"></a>
# **refreshSecretAccessKey**
> StatusResponse refreshSecretAccessKey()

Gets the secret access key.             To Update SecretAccessKey request the method with extra header             To Refresh the SecretAccessKey request addition header values  \&quot;accessTokenKey\&quot; &#x3D; Existing Value             And refreshTokenKey &#x3D; \&quot;true\&quot;

Gets the secret access key.             To Update SecretAccessKey request the method with extra header             To Refresh the SecretAccessKey request addition header values  \&quot;accessTokenKey\&quot; &#x3D; Existing Value             And refreshTokenKey &#x3D; \&quot;true\&quot;

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
try {
    StatusResponse result = apiInstance.refreshSecretAccessKey();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#refreshSecretAccessKey");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**StatusResponse**](StatusResponse.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="registerNotifications"></a>
# **registerNotifications**
> NotificationViewModel registerNotifications(notificationViewModel)

Register Notifications Preference

Register Notifications Preference

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
NotificationViewModel notificationViewModel = new NotificationViewModel(); // NotificationViewModel | 
try {
    NotificationViewModel result = apiInstance.registerNotifications(notificationViewModel);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#registerNotifications");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **notificationViewModel** | [**NotificationViewModel**](NotificationViewModel.md)|  |

### Return type

[**NotificationViewModel**](NotificationViewModel.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="registration"></a>
# **registration**
> RegisterResponse registration(registerViewModel)

Register new user.

Register new user.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
RegisterViewModel registerViewModel = new RegisterViewModel(); // RegisterViewModel | 
try {
    RegisterResponse result = apiInstance.registration(registerViewModel);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#registration");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **registerViewModel** | [**RegisterViewModel**](RegisterViewModel.md)|  |

### Return type

[**RegisterResponse**](RegisterResponse.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="resendVerifyCode"></a>
# **resendVerifyCode**
> StatusResponse resendVerifyCode(verifyViewModel)

Resend the verify code.

Resend the verify code.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
VerifyViewModel verifyViewModel = new VerifyViewModel(); // VerifyViewModel | 
try {
    StatusResponse result = apiInstance.resendVerifyCode(verifyViewModel);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#resendVerifyCode");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **verifyViewModel** | [**VerifyViewModel**](VerifyViewModel.md)|  |

### Return type

[**StatusResponse**](StatusResponse.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="resetPassword"></a>
# **resetPassword**
> PwdResetResponse resetPassword(resetPassword)

Resets the password.

Resets the password.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
ResetPassword resetPassword = new ResetPassword(); // ResetPassword | 
try {
    PwdResetResponse result = apiInstance.resetPassword(resetPassword);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#resetPassword");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **resetPassword** | [**ResetPassword**](ResetPassword.md)|  |

### Return type

[**PwdResetResponse**](PwdResetResponse.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="resetPasswordRequest"></a>
# **resetPasswordRequest**
> PwdResetResponse resetPasswordRequest(resetPasswordRequest)

Reset Password Request

Reset Password Request

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
ResetPasswordRequest resetPasswordRequest = new ResetPasswordRequest(); // ResetPasswordRequest | 
try {
    PwdResetResponse result = apiInstance.resetPasswordRequest(resetPasswordRequest);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#resetPasswordRequest");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **resetPasswordRequest** | [**ResetPasswordRequest**](ResetPasswordRequest.md)|  |

### Return type

[**PwdResetResponse**](PwdResetResponse.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="saveBillingAgreement"></a>
# **saveBillingAgreement**
> StatusResponse saveBillingAgreement(agreementViewModel)

Saves the billing agreement.

Saves the billing agreement.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
AgreementViewModel agreementViewModel = new AgreementViewModel(); // AgreementViewModel | 
try {
    StatusResponse result = apiInstance.saveBillingAgreement(agreementViewModel);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#saveBillingAgreement");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **agreementViewModel** | [**AgreementViewModel**](AgreementViewModel.md)|  |

### Return type

[**StatusResponse**](StatusResponse.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="saveRemoteImage"></a>
# **saveRemoteImage**
> FileObject saveRemoteImage(remoteImageViewModel)

Saves the remote image.

Saves the remote image.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
RemoteImageViewModel remoteImageViewModel = new RemoteImageViewModel(); // RemoteImageViewModel | 
try {
    FileObject result = apiInstance.saveRemoteImage(remoteImageViewModel);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#saveRemoteImage");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **remoteImageViewModel** | [**RemoteImageViewModel**](RemoteImageViewModel.md)|  |

### Return type

[**FileObject**](FileObject.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="statesByCountry"></a>
# **statesByCountry**
> List&lt;StatesModel&gt; statesByCountry(requestNameModel)

States by country identifier or Name.

States by country identifier or Name.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
RequestNameModel requestNameModel = new RequestNameModel(); // RequestNameModel | 
try {
    List<StatesModel> result = apiInstance.statesByCountry(requestNameModel);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#statesByCountry");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestNameModel** | [**RequestNameModel**](RequestNameModel.md)|  |

### Return type

[**List&lt;StatesModel&gt;**](StatesModel.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="thumbsUpDown"></a>
# **thumbsUpDown**
> ThumbsResponse thumbsUpDown(thumbsRequest)

Thumbs up down Selection [ThumbsUp&#x3D;1, otherwize ThumbsDown&#x3D;0].

Thumbs up down Selection [ThumbsUp&#x3D;1, otherwize ThumbsDown&#x3D;0].

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
ThumbsRequest thumbsRequest = new ThumbsRequest(); // ThumbsRequest | 
try {
    ThumbsResponse result = apiInstance.thumbsUpDown(thumbsRequest);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#thumbsUpDown");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **thumbsRequest** | [**ThumbsRequest**](ThumbsRequest.md)|  |

### Return type

[**ThumbsResponse**](ThumbsResponse.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateProfile"></a>
# **updateProfile**
> User updateProfile(profileViewModel)

Update User Profile

Update User Profile

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
ProfileViewModel profileViewModel = new ProfileViewModel(); // ProfileViewModel | 
try {
    User result = apiInstance.updateProfile(profileViewModel);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#updateProfile");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **profileViewModel** | [**ProfileViewModel**](ProfileViewModel.md)|  |

### Return type

[**User**](User.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updatePublishedPoll"></a>
# **updatePublishedPoll**
> PollResponse updatePublishedPoll(updatePollRequest)

Update Published Poll Keywords and Description.

Update Published Poll Keywords and Description.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
UpdatePollRequest updatePollRequest = new UpdatePollRequest(); // UpdatePollRequest | 
try {
    PollResponse result = apiInstance.updatePublishedPoll(updatePollRequest);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#updatePublishedPoll");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **updatePollRequest** | [**UpdatePollRequest**](UpdatePollRequest.md)|  |

### Return type

[**PollResponse**](PollResponse.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="uploadFile"></a>
# **uploadFile**
> FileObject uploadFile(isProfilePic, accessKey)

Post File to server will return filename saved, for Profile image send header isProfilePic&#x3D;true

Post File to server will return filename saved, for Profile image send header isProfilePic&#x3D;true

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
Boolean isProfilePic = true; // Boolean | true or false
String accessKey = "accessKey_example"; // String | The access key.
try {
    FileObject result = apiInstance.uploadFile(isProfilePic, accessKey);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#uploadFile");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **isProfilePic** | **Boolean**| true or false |
 **accessKey** | **String**| The access key. | [optional]

### Return type

[**FileObject**](FileObject.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="verifyEmail"></a>
# **verifyEmail**
> StatusResponse verifyEmail(requestId)

Verify Email Addresss for the account

Verify Email Addresss for the account

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
RequestId requestId = new RequestId(); // RequestId | 
try {
    StatusResponse result = apiInstance.verifyEmail(requestId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#verifyEmail");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestId** | [**RequestId**](RequestId.md)|  |

### Return type

[**StatusResponse**](StatusResponse.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="verifyPhone"></a>
# **verifyPhone**
> StatusResponse verifyPhone(verifyCodeViewModel)

Verifies Phone with the code received over phone to use your account.

Verifies Phone with the code received over phone to use your account.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
VerifyCodeViewModel verifyCodeViewModel = new VerifyCodeViewModel(); // VerifyCodeViewModel | 
try {
    StatusResponse result = apiInstance.verifyPhone(verifyCodeViewModel);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#verifyPhone");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **verifyCodeViewModel** | [**VerifyCodeViewModel**](VerifyCodeViewModel.md)|  |

### Return type

[**StatusResponse**](StatusResponse.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="viewNotificationsPreference"></a>
# **viewNotificationsPreference**
> NotificationViewModel viewNotificationsPreference()

Register Notifications Preference

Register Notifications Preference

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
try {
    NotificationViewModel result = apiInstance.viewNotificationsPreference();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#viewNotificationsPreference");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**NotificationViewModel**](NotificationViewModel.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="viewProfile"></a>
# **viewProfile**
> User viewProfile()

View User Profile details for the unique identifier, if the response exists  pictureId info, and for retrieve filename, you need to remove all (-) and add file extention to .png all files on server are saved into png format.

View User Profile details for the unique identifier, if the response exists  pictureId info, and for retrieve filename, you need to remove all (-) and add file extention to .png all files on server are saved into png format.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
try {
    User result = apiInstance.viewProfile();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#viewProfile");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**User**](User.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="viewPublicProfile"></a>
# **viewPublicProfile**
> Contact viewPublicProfile(userIdModel)

View Public Profile details for the unique identifier, rest follow the ViewProfile.

View Public Profile details for the unique identifier, rest follow the ViewProfile.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
UserIdModel userIdModel = new UserIdModel(); // UserIdModel | 
try {
    Contact result = apiInstance.viewPublicProfile(userIdModel);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#viewPublicProfile");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userIdModel** | [**UserIdModel**](UserIdModel.md)|  |

### Return type

[**Contact**](Contact.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

