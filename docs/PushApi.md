# PushApi

All URIs are relative to *http://demo.honeyshyam.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**activeNotifications**](PushApi.md#activeNotifications) | **POST** /v1/Push/ActiveNotifications | Gets the active custom notifications.
[**listNotificationAlerts**](PushApi.md#listNotificationAlerts) | **POST** /v1/Push/ListNotificationAlerts | Lists the notification alerts.
[**registerSnsDevice**](PushApi.md#registerSnsDevice) | **POST** /v1/Push/RegisterSnsDevice | Registers the SNS/Push Notification device.
[**sendWebPushNotificationsFCMTest**](PushApi.md#sendWebPushNotificationsFCMTest) | **POST** /v1/Push/SendWebPushNotificationsFCMTest | Sends the web push notifications FCM test.
[**sendWebPushNotificationsTest**](PushApi.md#sendWebPushNotificationsTest) | **POST** /v1/Push/SendWebPushNotificationsTest | Sends the web push notifications test.
[**updateNotification**](PushApi.md#updateNotification) | **POST** /v1/Push/UpdateNotification | Updates the notification.


<a name="activeNotifications"></a>
# **activeNotifications**
> ListCustomNotifications activeNotifications(requestId)

Gets the active custom notifications.

Gets the active custom notifications.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PushApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PushApi apiInstance = new PushApi();
RequestId requestId = new RequestId(); // RequestId | 
try {
    ListCustomNotifications result = apiInstance.activeNotifications(requestId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PushApi#activeNotifications");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestId** | [**RequestId**](RequestId.md)|  |

### Return type

[**ListCustomNotifications**](ListCustomNotifications.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="listNotificationAlerts"></a>
# **listNotificationAlerts**
> List&lt;NotificationTrackingModel&gt; listNotificationAlerts(requestId)

Lists the notification alerts.

Lists the notification alerts.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PushApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PushApi apiInstance = new PushApi();
RequestId requestId = new RequestId(); // RequestId | 
try {
    List<NotificationTrackingModel> result = apiInstance.listNotificationAlerts(requestId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PushApi#listNotificationAlerts");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestId** | [**RequestId**](RequestId.md)|  |

### Return type

[**List&lt;NotificationTrackingModel&gt;**](NotificationTrackingModel.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="registerSnsDevice"></a>
# **registerSnsDevice**
> String registerSnsDevice(registerDeviceSNS)

Registers the SNS/Push Notification device.

Registers the SNS/Push Notification device.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PushApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PushApi apiInstance = new PushApi();
RegisterDeviceSNS registerDeviceSNS = new RegisterDeviceSNS(); // RegisterDeviceSNS | 
try {
    String result = apiInstance.registerSnsDevice(registerDeviceSNS);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PushApi#registerSnsDevice");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **registerDeviceSNS** | [**RegisterDeviceSNS**](RegisterDeviceSNS.md)|  |

### Return type

**String**

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="sendWebPushNotificationsFCMTest"></a>
# **sendWebPushNotificationsFCMTest**
> String sendWebPushNotificationsFCMTest(gcMViewModel)

Sends the web push notifications FCM test.

Sends the web push notifications FCM test.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PushApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PushApi apiInstance = new PushApi();
GCMViewModel gcMViewModel = new GCMViewModel(); // GCMViewModel | 
try {
    String result = apiInstance.sendWebPushNotificationsFCMTest(gcMViewModel);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PushApi#sendWebPushNotificationsFCMTest");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **gcMViewModel** | [**GCMViewModel**](GCMViewModel.md)|  |

### Return type

**String**

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="sendWebPushNotificationsTest"></a>
# **sendWebPushNotificationsTest**
> String sendWebPushNotificationsTest(sendMessageViewModel)

Sends the web push notifications test.

Sends the web push notifications test.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PushApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PushApi apiInstance = new PushApi();
SendMessageViewModel sendMessageViewModel = new SendMessageViewModel(); // SendMessageViewModel | 
try {
    String result = apiInstance.sendWebPushNotificationsTest(sendMessageViewModel);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PushApi#sendWebPushNotificationsTest");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sendMessageViewModel** | [**SendMessageViewModel**](SendMessageViewModel.md)|  |

### Return type

**String**

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateNotification"></a>
# **updateNotification**
> StatusResponse updateNotification(customNotification)

Updates the notification.

Updates the notification.

### Example
```java
// Import classes:
//import polls.client.ApiClient;
//import polls.client.ApiException;
//import polls.client.Configuration;
//import polls.client.auth.*;
//import polls.client.api.PushApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiToken
ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
apiToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiToken.setApiKeyPrefix("Token");

// Configure API key authorization: apiUserId
ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
apiUserId.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiUserId.setApiKeyPrefix("Token");

PushApi apiInstance = new PushApi();
CustomNotification customNotification = new CustomNotification(); // CustomNotification | 
try {
    StatusResponse result = apiInstance.updateNotification(customNotification);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PushApi#updateNotification");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customNotification** | [**CustomNotification**](CustomNotification.md)|  |

### Return type

[**StatusResponse**](StatusResponse.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

