
# Demographics

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**locationDeduced** | [**LocationDeduced**](LocationDeduced.md) |  |  [optional]
**gender** | **String** |  |  [optional]
**locationGeneral** | **String** |  |  [optional]



