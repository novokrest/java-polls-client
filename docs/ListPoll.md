
# ListPoll

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pollTypeId** | **Long** |  |  [optional]
**firstOption** | **String** |  |  [optional]
**secondOption** | **String** |  |  [optional]
**firstImagePath** | **String** |  |  [optional]
**secondImagePath** | **String** |  |  [optional]
**choiceFirstCounter** | **Long** |  |  [optional]
**choiceSecondCounter** | **Long** |  |  [optional]
**resultCounter** | **Long** |  |  [optional]
**filtersJson** | **String** |  |  [optional]
**ownerId** | [**UUID**](UUID.md) |  |  [optional]
**totalCount** | **Long** |  |  [optional]
**completedOn** | [**DateTime**](DateTime.md) |  |  [optional]
**demographicCompleted** | **Long** |  |  [optional]
**pollId** | **Long** |  |  [optional]
**hitId** | **String** |  |  [optional]
**pollStatus** | **String** |  |  [optional]
**pollTitle** | **String** |  | 
**question** | **String** |  | 
**pollOverview** | **String** |  | 
**assignmentDurationInSeconds** | **Long** |  | 
**keywords** | **String** |  | 
**expirationDate** | [**DateTime**](DateTime.md) |  |  [optional]
**filterNameValue** | [**List&lt;FilterPairs&gt;**](FilterPairs.md) |  |  [optional]
**filterMainCategory** | **String** |  |  [optional]
**pUserName** | **String** |  |  [optional]
**isEnablePublic** | **Boolean** |  |  [optional]
**catName** | **String** |  |  [optional]
**userName** | **String** |  |  [optional]
**maxAssignments** | **Long** |  |  [optional]
**tags** | **String** |  | 
**lifeTimeInSeconds** | **Long** |  | 
**isEnabledComments** | **Boolean** |  |  [optional]
**createDate** | [**DateTime**](DateTime.md) |  |  [optional]
**isFilterOption** | **Boolean** |  |  [optional]
**catId** | **Long** |  |  [optional]
**isCompanyPoll** | **Boolean** |  |  [optional]
**alllowedAnonymously** | **Integer** |  |  [optional]
**isAdult** | **Boolean** |  |  [optional]
**isPublished** | **Boolean** |  |  [optional]



