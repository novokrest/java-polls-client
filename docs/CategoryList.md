
# CategoryList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**catId** | **Long** | the cat identifier. |  [optional]
**catName** | **String** | the name of the cat. |  [optional]



