
# CheckinLocationModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  |  [optional]
**latitude** | **Double** |  |  [optional]
**longitude** | **Double** |  |  [optional]
**dateCreated** | [**DateTime**](DateTime.md) |  |  [optional]
**ipAddress** | **String** |  |  [optional]
**timeStamp** | [**DateTime**](DateTime.md) |  |  [optional]
**userId** | [**UUID**](UUID.md) |  |  [optional]
**locationName** | **String** |  |  [optional]



