
# DeviceInfoModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**createDate** | [**DateTime**](DateTime.md) | Gets or sets the create date. |  [optional]
**updateDate** | [**DateTime**](DateTime.md) | Gets or sets the updated date. |  [optional]
**snsDeviceId** | **String** |  | 
**channelId** | **String** |  |  [optional]
**deviceName** | **String** |  |  [optional]
**devicePlatform** | **String** |  |  [optional]
**deviceVersion** | **String** |  |  [optional]
**browserName** | **String** |  |  [optional]
**browserVersion** | **String** |  |  [optional]
**browserUserAgent** | **String** |  |  [optional]
**status** | **String** |  |  [optional]
**snsType** | **String** |  |  [optional]



