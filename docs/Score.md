
# Score

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**provider** | **String** |  |  [optional]
**type** | **String** |  |  [optional]
**value** | **Long** |  |  [optional]



