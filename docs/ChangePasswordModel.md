
# ChangePasswordModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**oldPassword** | **String** | the old password. | 
**newPassword** | **String** | the new password. | 



