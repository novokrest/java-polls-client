
# SocialProfile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **String** |  |  [optional]
**typeId** | **String** |  |  [optional]
**typeName** | **String** |  |  [optional]
**url** | **String** |  |  [optional]
**id** | **String** |  |  [optional]
**followers** | **Long** |  |  [optional]
**username** | **String** |  |  [optional]
**bio** | **String** |  |  [optional]
**following** | **Long** |  |  [optional]



