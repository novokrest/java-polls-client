
# FilterDataModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  |  [optional]
**filterName** | **String** |  |  [optional]
**showGraph** | **Boolean** |  |  [optional]
**showFilter** | **Boolean** |  |  [optional]



