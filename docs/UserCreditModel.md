
# UserCreditModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**userId** | [**UUID**](UUID.md) |  |  [optional]
**dateCreated** | [**DateTime**](DateTime.md) |  |  [optional]
**balanceAmount** | **Double** |  |  [optional]
**modifiedDate** | [**DateTime**](DateTime.md) |  |  [optional]
**creditBalance** | **Long** |  |  [optional]
**pollsBalance** | **Long** |  |  [optional]
**nextBillingDay** | **Long** |  |  [optional]
**lastPaymentDate** | [**DateTime**](DateTime.md) |  |  [optional]
**memberSince** | [**DateTime**](DateTime.md) |  |  [optional]



