
# ThumbsRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**detailId** | **Long** |  |  [optional]
**rateChoice** | **Boolean** |  |  [optional]
**workerId** | **String** |  |  [optional]



