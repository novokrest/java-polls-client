
# SuggestQuestionViewModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**questionId** | **Long** | Primary key Question Id |  [optional]
**catId** | **Long** | Gets or sets the cat identifier. |  [optional]
**catName** | **String** | Gets or sets the name of the cat. |  [optional]
**question** | **String** | Gets or sets the suggestion question. |  [optional]



