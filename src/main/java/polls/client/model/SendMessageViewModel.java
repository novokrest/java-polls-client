/*
 * Polls Web API
 * Poll Service Developers Web API
 *
 * OpenAPI spec version: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package polls.client.model;

import java.util.Objects;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.util.UUID;

/**
 * Send Message ViewModel
 */
@ApiModel(description = "Send Message ViewModel")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2017-12-15T09:23:34.748+03:00")
public class SendMessageViewModel {
  @SerializedName("channelId")
  private String channelId = null;

  @SerializedName("snsType")
  private String snsType = null;

  @SerializedName("messageToSend")
  private String messageToSend = null;

  @SerializedName("userId")
  private UUID userId = null;

  @SerializedName("pollId")
  private Long pollId = null;

  public SendMessageViewModel channelId(String channelId) {
    this.channelId = channelId;
    return this;
  }

   /**
   * Gets or sets the channel identifier.
   * @return channelId
  **/
  @ApiModelProperty(required = true, value = "Gets or sets the channel identifier.")
  public String getChannelId() {
    return channelId;
  }

  public void setChannelId(String channelId) {
    this.channelId = channelId;
  }

  public SendMessageViewModel snsType(String snsType) {
    this.snsType = snsType;
    return this;
  }

   /**
   * Gets or sets the type of the SNS WEB, APN, GCM.
   * @return snsType
  **/
  @ApiModelProperty(required = true, value = "Gets or sets the type of the SNS WEB, APN, GCM.")
  public String getSnsType() {
    return snsType;
  }

  public void setSnsType(String snsType) {
    this.snsType = snsType;
  }

  public SendMessageViewModel messageToSend(String messageToSend) {
    this.messageToSend = messageToSend;
    return this;
  }

   /**
   * Gets or sets the message to send.
   * @return messageToSend
  **/
  @ApiModelProperty(required = true, value = "Gets or sets the message to send.")
  public String getMessageToSend() {
    return messageToSend;
  }

  public void setMessageToSend(String messageToSend) {
    this.messageToSend = messageToSend;
  }

  public SendMessageViewModel userId(UUID userId) {
    this.userId = userId;
    return this;
  }

   /**
   * Gets or sets the user identifier.
   * @return userId
  **/
  @ApiModelProperty(value = "Gets or sets the user identifier.")
  public UUID getUserId() {
    return userId;
  }

  public void setUserId(UUID userId) {
    this.userId = userId;
  }

  public SendMessageViewModel pollId(Long pollId) {
    this.pollId = pollId;
    return this;
  }

   /**
   * Gets or sets the poll identifier.
   * @return pollId
  **/
  @ApiModelProperty(value = "Gets or sets the poll identifier.")
  public Long getPollId() {
    return pollId;
  }

  public void setPollId(Long pollId) {
    this.pollId = pollId;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SendMessageViewModel sendMessageViewModel = (SendMessageViewModel) o;
    return Objects.equals(this.channelId, sendMessageViewModel.channelId) &&
        Objects.equals(this.snsType, sendMessageViewModel.snsType) &&
        Objects.equals(this.messageToSend, sendMessageViewModel.messageToSend) &&
        Objects.equals(this.userId, sendMessageViewModel.userId) &&
        Objects.equals(this.pollId, sendMessageViewModel.pollId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(channelId, snsType, messageToSend, userId, pollId);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SendMessageViewModel {\n");
    
    sb.append("    channelId: ").append(toIndentedString(channelId)).append("\n");
    sb.append("    snsType: ").append(toIndentedString(snsType)).append("\n");
    sb.append("    messageToSend: ").append(toIndentedString(messageToSend)).append("\n");
    sb.append("    userId: ").append(toIndentedString(userId)).append("\n");
    sb.append("    pollId: ").append(toIndentedString(pollId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

