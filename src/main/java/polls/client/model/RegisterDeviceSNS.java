/*
 * Polls Web API
 * Poll Service Developers Web API
 *
 * OpenAPI spec version: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package polls.client.model;

import java.util.Objects;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.util.UUID;

/**
 * 
 */
@ApiModel(description = "")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2017-12-15T09:23:34.748+03:00")
public class RegisterDeviceSNS {
  @SerializedName("userId")
  private UUID userId = null;

  @SerializedName("snsType")
  private String snsType = null;

  @SerializedName("snsAppId")
  private String snsAppId = null;

  @SerializedName("snsDeviceId")
  private String snsDeviceId = null;

  @SerializedName("snsDeviceId2")
  private String snsDeviceId2 = null;

  @SerializedName("channelId")
  private String channelId = null;

  @SerializedName("deviceId")
  private UUID deviceId = null;

  public RegisterDeviceSNS userId(UUID userId) {
    this.userId = userId;
    return this;
  }

   /**
   * 
   * @return userId
  **/
  @ApiModelProperty(value = "")
  public UUID getUserId() {
    return userId;
  }

  public void setUserId(UUID userId) {
    this.userId = userId;
  }

  public RegisterDeviceSNS snsType(String snsType) {
    this.snsType = snsType;
    return this;
  }

   /**
   * 
   * @return snsType
  **/
  @ApiModelProperty(value = "")
  public String getSnsType() {
    return snsType;
  }

  public void setSnsType(String snsType) {
    this.snsType = snsType;
  }

  public RegisterDeviceSNS snsAppId(String snsAppId) {
    this.snsAppId = snsAppId;
    return this;
  }

   /**
   * 
   * @return snsAppId
  **/
  @ApiModelProperty(value = "")
  public String getSnsAppId() {
    return snsAppId;
  }

  public void setSnsAppId(String snsAppId) {
    this.snsAppId = snsAppId;
  }

  public RegisterDeviceSNS snsDeviceId(String snsDeviceId) {
    this.snsDeviceId = snsDeviceId;
    return this;
  }

   /**
   * 
   * @return snsDeviceId
  **/
  @ApiModelProperty(value = "")
  public String getSnsDeviceId() {
    return snsDeviceId;
  }

  public void setSnsDeviceId(String snsDeviceId) {
    this.snsDeviceId = snsDeviceId;
  }

  public RegisterDeviceSNS snsDeviceId2(String snsDeviceId2) {
    this.snsDeviceId2 = snsDeviceId2;
    return this;
  }

   /**
   * 
   * @return snsDeviceId2
  **/
  @ApiModelProperty(value = "")
  public String getSnsDeviceId2() {
    return snsDeviceId2;
  }

  public void setSnsDeviceId2(String snsDeviceId2) {
    this.snsDeviceId2 = snsDeviceId2;
  }

  public RegisterDeviceSNS channelId(String channelId) {
    this.channelId = channelId;
    return this;
  }

   /**
   * 
   * @return channelId
  **/
  @ApiModelProperty(value = "")
  public String getChannelId() {
    return channelId;
  }

  public void setChannelId(String channelId) {
    this.channelId = channelId;
  }

  public RegisterDeviceSNS deviceId(UUID deviceId) {
    this.deviceId = deviceId;
    return this;
  }

   /**
   * 
   * @return deviceId
  **/
  @ApiModelProperty(value = "")
  public UUID getDeviceId() {
    return deviceId;
  }

  public void setDeviceId(UUID deviceId) {
    this.deviceId = deviceId;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    RegisterDeviceSNS registerDeviceSNS = (RegisterDeviceSNS) o;
    return Objects.equals(this.userId, registerDeviceSNS.userId) &&
        Objects.equals(this.snsType, registerDeviceSNS.snsType) &&
        Objects.equals(this.snsAppId, registerDeviceSNS.snsAppId) &&
        Objects.equals(this.snsDeviceId, registerDeviceSNS.snsDeviceId) &&
        Objects.equals(this.snsDeviceId2, registerDeviceSNS.snsDeviceId2) &&
        Objects.equals(this.channelId, registerDeviceSNS.channelId) &&
        Objects.equals(this.deviceId, registerDeviceSNS.deviceId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(userId, snsType, snsAppId, snsDeviceId, snsDeviceId2, channelId, deviceId);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class RegisterDeviceSNS {\n");
    
    sb.append("    userId: ").append(toIndentedString(userId)).append("\n");
    sb.append("    snsType: ").append(toIndentedString(snsType)).append("\n");
    sb.append("    snsAppId: ").append(toIndentedString(snsAppId)).append("\n");
    sb.append("    snsDeviceId: ").append(toIndentedString(snsDeviceId)).append("\n");
    sb.append("    snsDeviceId2: ").append(toIndentedString(snsDeviceId2)).append("\n");
    sb.append("    channelId: ").append(toIndentedString(channelId)).append("\n");
    sb.append("    deviceId: ").append(toIndentedString(deviceId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

