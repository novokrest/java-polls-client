/*
 * Polls Web API
 * Poll Service Developers Web API
 *
 * OpenAPI spec version: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package polls.client.model;

import java.util.Objects;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * Suggetion Question ViewModel
 */
@ApiModel(description = "Suggetion Question ViewModel")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2017-12-15T09:23:34.748+03:00")
public class SuggestQuestionViewModel {
  @SerializedName("QuestionId")
  private Long questionId = null;

  @SerializedName("CatId")
  private Long catId = null;

  @SerializedName("CatName")
  private String catName = null;

  @SerializedName("Question")
  private String question = null;

  public SuggestQuestionViewModel questionId(Long questionId) {
    this.questionId = questionId;
    return this;
  }

   /**
   * Primary key Question Id
   * @return questionId
  **/
  @ApiModelProperty(value = "Primary key Question Id")
  public Long getQuestionId() {
    return questionId;
  }

  public void setQuestionId(Long questionId) {
    this.questionId = questionId;
  }

  public SuggestQuestionViewModel catId(Long catId) {
    this.catId = catId;
    return this;
  }

   /**
   * Gets or sets the cat identifier.
   * @return catId
  **/
  @ApiModelProperty(value = "Gets or sets the cat identifier.")
  public Long getCatId() {
    return catId;
  }

  public void setCatId(Long catId) {
    this.catId = catId;
  }

  public SuggestQuestionViewModel catName(String catName) {
    this.catName = catName;
    return this;
  }

   /**
   * Gets or sets the name of the cat.
   * @return catName
  **/
  @ApiModelProperty(value = "Gets or sets the name of the cat.")
  public String getCatName() {
    return catName;
  }

  public void setCatName(String catName) {
    this.catName = catName;
  }

  public SuggestQuestionViewModel question(String question) {
    this.question = question;
    return this;
  }

   /**
   * Gets or sets the suggestion question.
   * @return question
  **/
  @ApiModelProperty(value = "Gets or sets the suggestion question.")
  public String getQuestion() {
    return question;
  }

  public void setQuestion(String question) {
    this.question = question;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SuggestQuestionViewModel suggestQuestionViewModel = (SuggestQuestionViewModel) o;
    return Objects.equals(this.questionId, suggestQuestionViewModel.questionId) &&
        Objects.equals(this.catId, suggestQuestionViewModel.catId) &&
        Objects.equals(this.catName, suggestQuestionViewModel.catName) &&
        Objects.equals(this.question, suggestQuestionViewModel.question);
  }

  @Override
  public int hashCode() {
    return Objects.hash(questionId, catId, catName, question);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SuggestQuestionViewModel {\n");
    
    sb.append("    questionId: ").append(toIndentedString(questionId)).append("\n");
    sb.append("    catId: ").append(toIndentedString(catId)).append("\n");
    sb.append("    catName: ").append(toIndentedString(catName)).append("\n");
    sb.append("    question: ").append(toIndentedString(question)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

