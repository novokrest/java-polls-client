/*
 * Polls Web API
 * Poll Service Developers Web API
 *
 * OpenAPI spec version: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package polls.client.model;

import java.util.Objects;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import org.joda.time.DateTime;

/**
 * 
 */
@ApiModel(description = "")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2017-12-15T09:23:34.748+03:00")
public class BillingInvoiceModel {
  @SerializedName("id")
  private Long id = null;

  @SerializedName("transactionId")
  private String transactionId = null;

  @SerializedName("paymentDate")
  private DateTime paymentDate = null;

  @SerializedName("grossAmount")
  private Double grossAmount = null;

  @SerializedName("credits")
  private Long credits = null;

  @SerializedName("paymentMethod")
  private String paymentMethod = null;

  @SerializedName("payerEmail")
  private String payerEmail = null;

  @SerializedName("receiverEmail")
  private String receiverEmail = null;

  @SerializedName("paymentStatus")
  private String paymentStatus = null;

  @SerializedName("customerFirstName")
  private String customerFirstName = null;

  @SerializedName("customerLastName")
  private String customerLastName = null;

  @SerializedName("billingCompanyName")
  private String billingCompanyName = null;

  @SerializedName("billingAddressLine1")
  private String billingAddressLine1 = null;

  @SerializedName("billingAddressLine2")
  private String billingAddressLine2 = null;

  @SerializedName("billingState")
  private String billingState = null;

  @SerializedName("zipCode")
  private String zipCode = null;

  @SerializedName("product")
  private String product = null;

  @SerializedName("isPremium")
  private Boolean isPremium = null;

  @SerializedName("billingPhone")
  private String billingPhone = null;

  @SerializedName("customerCompanyName")
  private String customerCompanyName = null;

  @SerializedName("countryCode")
  private Long countryCode = null;

  @SerializedName("customerPhoneNumber")
  private String customerPhoneNumber = null;

  @SerializedName("customerPostalAddress")
  private String customerPostalAddress = null;

  @SerializedName("orgLogoUrl")
  private String orgLogoUrl = null;

  public BillingInvoiceModel id(Long id) {
    this.id = id;
    return this;
  }

   /**
   * 
   * @return id
  **/
  @ApiModelProperty(value = "")
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public BillingInvoiceModel transactionId(String transactionId) {
    this.transactionId = transactionId;
    return this;
  }

   /**
   * 
   * @return transactionId
  **/
  @ApiModelProperty(value = "")
  public String getTransactionId() {
    return transactionId;
  }

  public void setTransactionId(String transactionId) {
    this.transactionId = transactionId;
  }

  public BillingInvoiceModel paymentDate(DateTime paymentDate) {
    this.paymentDate = paymentDate;
    return this;
  }

   /**
   * 
   * @return paymentDate
  **/
  @ApiModelProperty(value = "")
  public DateTime getPaymentDate() {
    return paymentDate;
  }

  public void setPaymentDate(DateTime paymentDate) {
    this.paymentDate = paymentDate;
  }

  public BillingInvoiceModel grossAmount(Double grossAmount) {
    this.grossAmount = grossAmount;
    return this;
  }

   /**
   * 
   * @return grossAmount
  **/
  @ApiModelProperty(value = "")
  public Double getGrossAmount() {
    return grossAmount;
  }

  public void setGrossAmount(Double grossAmount) {
    this.grossAmount = grossAmount;
  }

  public BillingInvoiceModel credits(Long credits) {
    this.credits = credits;
    return this;
  }

   /**
   * 
   * @return credits
  **/
  @ApiModelProperty(value = "")
  public Long getCredits() {
    return credits;
  }

  public void setCredits(Long credits) {
    this.credits = credits;
  }

  public BillingInvoiceModel paymentMethod(String paymentMethod) {
    this.paymentMethod = paymentMethod;
    return this;
  }

   /**
   * 
   * @return paymentMethod
  **/
  @ApiModelProperty(value = "")
  public String getPaymentMethod() {
    return paymentMethod;
  }

  public void setPaymentMethod(String paymentMethod) {
    this.paymentMethod = paymentMethod;
  }

  public BillingInvoiceModel payerEmail(String payerEmail) {
    this.payerEmail = payerEmail;
    return this;
  }

   /**
   * 
   * @return payerEmail
  **/
  @ApiModelProperty(value = "")
  public String getPayerEmail() {
    return payerEmail;
  }

  public void setPayerEmail(String payerEmail) {
    this.payerEmail = payerEmail;
  }

  public BillingInvoiceModel receiverEmail(String receiverEmail) {
    this.receiverEmail = receiverEmail;
    return this;
  }

   /**
   * 
   * @return receiverEmail
  **/
  @ApiModelProperty(value = "")
  public String getReceiverEmail() {
    return receiverEmail;
  }

  public void setReceiverEmail(String receiverEmail) {
    this.receiverEmail = receiverEmail;
  }

  public BillingInvoiceModel paymentStatus(String paymentStatus) {
    this.paymentStatus = paymentStatus;
    return this;
  }

   /**
   * 
   * @return paymentStatus
  **/
  @ApiModelProperty(value = "")
  public String getPaymentStatus() {
    return paymentStatus;
  }

  public void setPaymentStatus(String paymentStatus) {
    this.paymentStatus = paymentStatus;
  }

  public BillingInvoiceModel customerFirstName(String customerFirstName) {
    this.customerFirstName = customerFirstName;
    return this;
  }

   /**
   * 
   * @return customerFirstName
  **/
  @ApiModelProperty(value = "")
  public String getCustomerFirstName() {
    return customerFirstName;
  }

  public void setCustomerFirstName(String customerFirstName) {
    this.customerFirstName = customerFirstName;
  }

  public BillingInvoiceModel customerLastName(String customerLastName) {
    this.customerLastName = customerLastName;
    return this;
  }

   /**
   * 
   * @return customerLastName
  **/
  @ApiModelProperty(value = "")
  public String getCustomerLastName() {
    return customerLastName;
  }

  public void setCustomerLastName(String customerLastName) {
    this.customerLastName = customerLastName;
  }

  public BillingInvoiceModel billingCompanyName(String billingCompanyName) {
    this.billingCompanyName = billingCompanyName;
    return this;
  }

   /**
   * 
   * @return billingCompanyName
  **/
  @ApiModelProperty(value = "")
  public String getBillingCompanyName() {
    return billingCompanyName;
  }

  public void setBillingCompanyName(String billingCompanyName) {
    this.billingCompanyName = billingCompanyName;
  }

  public BillingInvoiceModel billingAddressLine1(String billingAddressLine1) {
    this.billingAddressLine1 = billingAddressLine1;
    return this;
  }

   /**
   * 
   * @return billingAddressLine1
  **/
  @ApiModelProperty(value = "")
  public String getBillingAddressLine1() {
    return billingAddressLine1;
  }

  public void setBillingAddressLine1(String billingAddressLine1) {
    this.billingAddressLine1 = billingAddressLine1;
  }

  public BillingInvoiceModel billingAddressLine2(String billingAddressLine2) {
    this.billingAddressLine2 = billingAddressLine2;
    return this;
  }

   /**
   * 
   * @return billingAddressLine2
  **/
  @ApiModelProperty(value = "")
  public String getBillingAddressLine2() {
    return billingAddressLine2;
  }

  public void setBillingAddressLine2(String billingAddressLine2) {
    this.billingAddressLine2 = billingAddressLine2;
  }

  public BillingInvoiceModel billingState(String billingState) {
    this.billingState = billingState;
    return this;
  }

   /**
   * 
   * @return billingState
  **/
  @ApiModelProperty(value = "")
  public String getBillingState() {
    return billingState;
  }

  public void setBillingState(String billingState) {
    this.billingState = billingState;
  }

  public BillingInvoiceModel zipCode(String zipCode) {
    this.zipCode = zipCode;
    return this;
  }

   /**
   * 
   * @return zipCode
  **/
  @ApiModelProperty(value = "")
  public String getZipCode() {
    return zipCode;
  }

  public void setZipCode(String zipCode) {
    this.zipCode = zipCode;
  }

  public BillingInvoiceModel product(String product) {
    this.product = product;
    return this;
  }

   /**
   * 
   * @return product
  **/
  @ApiModelProperty(value = "")
  public String getProduct() {
    return product;
  }

  public void setProduct(String product) {
    this.product = product;
  }

  public BillingInvoiceModel isPremium(Boolean isPremium) {
    this.isPremium = isPremium;
    return this;
  }

   /**
   * 
   * @return isPremium
  **/
  @ApiModelProperty(value = "")
  public Boolean getIsPremium() {
    return isPremium;
  }

  public void setIsPremium(Boolean isPremium) {
    this.isPremium = isPremium;
  }

  public BillingInvoiceModel billingPhone(String billingPhone) {
    this.billingPhone = billingPhone;
    return this;
  }

   /**
   * 
   * @return billingPhone
  **/
  @ApiModelProperty(value = "")
  public String getBillingPhone() {
    return billingPhone;
  }

  public void setBillingPhone(String billingPhone) {
    this.billingPhone = billingPhone;
  }

  public BillingInvoiceModel customerCompanyName(String customerCompanyName) {
    this.customerCompanyName = customerCompanyName;
    return this;
  }

   /**
   * 
   * @return customerCompanyName
  **/
  @ApiModelProperty(value = "")
  public String getCustomerCompanyName() {
    return customerCompanyName;
  }

  public void setCustomerCompanyName(String customerCompanyName) {
    this.customerCompanyName = customerCompanyName;
  }

  public BillingInvoiceModel countryCode(Long countryCode) {
    this.countryCode = countryCode;
    return this;
  }

   /**
   * 
   * @return countryCode
  **/
  @ApiModelProperty(value = "")
  public Long getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(Long countryCode) {
    this.countryCode = countryCode;
  }

  public BillingInvoiceModel customerPhoneNumber(String customerPhoneNumber) {
    this.customerPhoneNumber = customerPhoneNumber;
    return this;
  }

   /**
   * 
   * @return customerPhoneNumber
  **/
  @ApiModelProperty(value = "")
  public String getCustomerPhoneNumber() {
    return customerPhoneNumber;
  }

  public void setCustomerPhoneNumber(String customerPhoneNumber) {
    this.customerPhoneNumber = customerPhoneNumber;
  }

  public BillingInvoiceModel customerPostalAddress(String customerPostalAddress) {
    this.customerPostalAddress = customerPostalAddress;
    return this;
  }

   /**
   * 
   * @return customerPostalAddress
  **/
  @ApiModelProperty(value = "")
  public String getCustomerPostalAddress() {
    return customerPostalAddress;
  }

  public void setCustomerPostalAddress(String customerPostalAddress) {
    this.customerPostalAddress = customerPostalAddress;
  }

  public BillingInvoiceModel orgLogoUrl(String orgLogoUrl) {
    this.orgLogoUrl = orgLogoUrl;
    return this;
  }

   /**
   * 
   * @return orgLogoUrl
  **/
  @ApiModelProperty(value = "")
  public String getOrgLogoUrl() {
    return orgLogoUrl;
  }

  public void setOrgLogoUrl(String orgLogoUrl) {
    this.orgLogoUrl = orgLogoUrl;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    BillingInvoiceModel billingInvoiceModel = (BillingInvoiceModel) o;
    return Objects.equals(this.id, billingInvoiceModel.id) &&
        Objects.equals(this.transactionId, billingInvoiceModel.transactionId) &&
        Objects.equals(this.paymentDate, billingInvoiceModel.paymentDate) &&
        Objects.equals(this.grossAmount, billingInvoiceModel.grossAmount) &&
        Objects.equals(this.credits, billingInvoiceModel.credits) &&
        Objects.equals(this.paymentMethod, billingInvoiceModel.paymentMethod) &&
        Objects.equals(this.payerEmail, billingInvoiceModel.payerEmail) &&
        Objects.equals(this.receiverEmail, billingInvoiceModel.receiverEmail) &&
        Objects.equals(this.paymentStatus, billingInvoiceModel.paymentStatus) &&
        Objects.equals(this.customerFirstName, billingInvoiceModel.customerFirstName) &&
        Objects.equals(this.customerLastName, billingInvoiceModel.customerLastName) &&
        Objects.equals(this.billingCompanyName, billingInvoiceModel.billingCompanyName) &&
        Objects.equals(this.billingAddressLine1, billingInvoiceModel.billingAddressLine1) &&
        Objects.equals(this.billingAddressLine2, billingInvoiceModel.billingAddressLine2) &&
        Objects.equals(this.billingState, billingInvoiceModel.billingState) &&
        Objects.equals(this.zipCode, billingInvoiceModel.zipCode) &&
        Objects.equals(this.product, billingInvoiceModel.product) &&
        Objects.equals(this.isPremium, billingInvoiceModel.isPremium) &&
        Objects.equals(this.billingPhone, billingInvoiceModel.billingPhone) &&
        Objects.equals(this.customerCompanyName, billingInvoiceModel.customerCompanyName) &&
        Objects.equals(this.countryCode, billingInvoiceModel.countryCode) &&
        Objects.equals(this.customerPhoneNumber, billingInvoiceModel.customerPhoneNumber) &&
        Objects.equals(this.customerPostalAddress, billingInvoiceModel.customerPostalAddress) &&
        Objects.equals(this.orgLogoUrl, billingInvoiceModel.orgLogoUrl);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, transactionId, paymentDate, grossAmount, credits, paymentMethod, payerEmail, receiverEmail, paymentStatus, customerFirstName, customerLastName, billingCompanyName, billingAddressLine1, billingAddressLine2, billingState, zipCode, product, isPremium, billingPhone, customerCompanyName, countryCode, customerPhoneNumber, customerPostalAddress, orgLogoUrl);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class BillingInvoiceModel {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    transactionId: ").append(toIndentedString(transactionId)).append("\n");
    sb.append("    paymentDate: ").append(toIndentedString(paymentDate)).append("\n");
    sb.append("    grossAmount: ").append(toIndentedString(grossAmount)).append("\n");
    sb.append("    credits: ").append(toIndentedString(credits)).append("\n");
    sb.append("    paymentMethod: ").append(toIndentedString(paymentMethod)).append("\n");
    sb.append("    payerEmail: ").append(toIndentedString(payerEmail)).append("\n");
    sb.append("    receiverEmail: ").append(toIndentedString(receiverEmail)).append("\n");
    sb.append("    paymentStatus: ").append(toIndentedString(paymentStatus)).append("\n");
    sb.append("    customerFirstName: ").append(toIndentedString(customerFirstName)).append("\n");
    sb.append("    customerLastName: ").append(toIndentedString(customerLastName)).append("\n");
    sb.append("    billingCompanyName: ").append(toIndentedString(billingCompanyName)).append("\n");
    sb.append("    billingAddressLine1: ").append(toIndentedString(billingAddressLine1)).append("\n");
    sb.append("    billingAddressLine2: ").append(toIndentedString(billingAddressLine2)).append("\n");
    sb.append("    billingState: ").append(toIndentedString(billingState)).append("\n");
    sb.append("    zipCode: ").append(toIndentedString(zipCode)).append("\n");
    sb.append("    product: ").append(toIndentedString(product)).append("\n");
    sb.append("    isPremium: ").append(toIndentedString(isPremium)).append("\n");
    sb.append("    billingPhone: ").append(toIndentedString(billingPhone)).append("\n");
    sb.append("    customerCompanyName: ").append(toIndentedString(customerCompanyName)).append("\n");
    sb.append("    countryCode: ").append(toIndentedString(countryCode)).append("\n");
    sb.append("    customerPhoneNumber: ").append(toIndentedString(customerPhoneNumber)).append("\n");
    sb.append("    customerPostalAddress: ").append(toIndentedString(customerPostalAddress)).append("\n");
    sb.append("    orgLogoUrl: ").append(toIndentedString(orgLogoUrl)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

