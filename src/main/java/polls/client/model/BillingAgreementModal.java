/*
 * Polls Web API
 * Poll Service Developers Web API
 *
 * OpenAPI spec version: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package polls.client.model;

import java.util.Objects;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.util.UUID;
import org.joda.time.DateTime;

/**
 * 
 */
@ApiModel(description = "")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2017-09-02T00:44:51.695+03:00")
public class BillingAgreementModal {
  @SerializedName("BillingId")
  private Long billingId = null;

  @SerializedName("UserId")
  private UUID userId = null;

  @SerializedName("PlanId")
  private UUID planId = null;

  @SerializedName("AgreementId")
  private String agreementId = null;

  @SerializedName("AgreementStatus")
  private String agreementStatus = null;

  @SerializedName("PayerEmail")
  private String payerEmail = null;

  @SerializedName("FirstName")
  private String firstName = null;

  @SerializedName("LastName")
  private String lastName = null;

  @SerializedName("TimeStamp")
  private String timeStamp = null;

  @SerializedName("DateCreated")
  private DateTime dateCreated = null;

  @SerializedName("TransactionType")
  private String transactionType = null;

  @SerializedName("CloseDate")
  private DateTime closeDate = null;

  @SerializedName("IsActive")
  private Boolean isActive = null;

  @SerializedName("NextBillingDay")
  private Long nextBillingDay = null;

  @SerializedName("PayerId")
  private String payerId = null;

  @SerializedName("PaymentGross")
  private Double paymentGross = null;

  @SerializedName("MaxAmount")
  private Double maxAmount = null;

  @SerializedName("PaymentStatus")
  private String paymentStatus = null;

  @SerializedName("mp_status")
  private Long mpStatus = null;

  @SerializedName("ReasonCode")
  private String reasonCode = null;

  @SerializedName("MpDesc")
  private String mpDesc = null;

  @SerializedName("PaymentMethod")
  private String paymentMethod = null;

  @SerializedName("CCBrand")
  private String ccBrand = null;

  @SerializedName("CCLAst4")
  private Long ccLAst4 = null;

  @SerializedName("CCExpMonth")
  private Long ccExpMonth = null;

  @SerializedName("CCExpYear")
  private Long ccExpYear = null;

  @SerializedName("CardId")
  private String cardId = null;

  public BillingAgreementModal billingId(Long billingId) {
    this.billingId = billingId;
    return this;
  }

   /**
   * 
   * @return billingId
  **/
  @ApiModelProperty(value = "")
  public Long getBillingId() {
    return billingId;
  }

  public void setBillingId(Long billingId) {
    this.billingId = billingId;
  }

  public BillingAgreementModal userId(UUID userId) {
    this.userId = userId;
    return this;
  }

   /**
   * 
   * @return userId
  **/
  @ApiModelProperty(value = "")
  public UUID getUserId() {
    return userId;
  }

  public void setUserId(UUID userId) {
    this.userId = userId;
  }

  public BillingAgreementModal planId(UUID planId) {
    this.planId = planId;
    return this;
  }

   /**
   * 
   * @return planId
  **/
  @ApiModelProperty(value = "")
  public UUID getPlanId() {
    return planId;
  }

  public void setPlanId(UUID planId) {
    this.planId = planId;
  }

  public BillingAgreementModal agreementId(String agreementId) {
    this.agreementId = agreementId;
    return this;
  }

   /**
   * 
   * @return agreementId
  **/
  @ApiModelProperty(value = "")
  public String getAgreementId() {
    return agreementId;
  }

  public void setAgreementId(String agreementId) {
    this.agreementId = agreementId;
  }

  public BillingAgreementModal agreementStatus(String agreementStatus) {
    this.agreementStatus = agreementStatus;
    return this;
  }

   /**
   * 
   * @return agreementStatus
  **/
  @ApiModelProperty(value = "")
  public String getAgreementStatus() {
    return agreementStatus;
  }

  public void setAgreementStatus(String agreementStatus) {
    this.agreementStatus = agreementStatus;
  }

  public BillingAgreementModal payerEmail(String payerEmail) {
    this.payerEmail = payerEmail;
    return this;
  }

   /**
   * 
   * @return payerEmail
  **/
  @ApiModelProperty(value = "")
  public String getPayerEmail() {
    return payerEmail;
  }

  public void setPayerEmail(String payerEmail) {
    this.payerEmail = payerEmail;
  }

  public BillingAgreementModal firstName(String firstName) {
    this.firstName = firstName;
    return this;
  }

   /**
   * 
   * @return firstName
  **/
  @ApiModelProperty(value = "")
  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public BillingAgreementModal lastName(String lastName) {
    this.lastName = lastName;
    return this;
  }

   /**
   * 
   * @return lastName
  **/
  @ApiModelProperty(value = "")
  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public BillingAgreementModal timeStamp(String timeStamp) {
    this.timeStamp = timeStamp;
    return this;
  }

   /**
   * 
   * @return timeStamp
  **/
  @ApiModelProperty(value = "")
  public String getTimeStamp() {
    return timeStamp;
  }

  public void setTimeStamp(String timeStamp) {
    this.timeStamp = timeStamp;
  }

  public BillingAgreementModal dateCreated(DateTime dateCreated) {
    this.dateCreated = dateCreated;
    return this;
  }

   /**
   * 
   * @return dateCreated
  **/
  @ApiModelProperty(value = "")
  public DateTime getDateCreated() {
    return dateCreated;
  }

  public void setDateCreated(DateTime dateCreated) {
    this.dateCreated = dateCreated;
  }

  public BillingAgreementModal transactionType(String transactionType) {
    this.transactionType = transactionType;
    return this;
  }

   /**
   * 
   * @return transactionType
  **/
  @ApiModelProperty(value = "")
  public String getTransactionType() {
    return transactionType;
  }

  public void setTransactionType(String transactionType) {
    this.transactionType = transactionType;
  }

  public BillingAgreementModal closeDate(DateTime closeDate) {
    this.closeDate = closeDate;
    return this;
  }

   /**
   * 
   * @return closeDate
  **/
  @ApiModelProperty(value = "")
  public DateTime getCloseDate() {
    return closeDate;
  }

  public void setCloseDate(DateTime closeDate) {
    this.closeDate = closeDate;
  }

  public BillingAgreementModal isActive(Boolean isActive) {
    this.isActive = isActive;
    return this;
  }

   /**
   * 
   * @return isActive
  **/
  @ApiModelProperty(value = "")
  public Boolean getIsActive() {
    return isActive;
  }

  public void setIsActive(Boolean isActive) {
    this.isActive = isActive;
  }

  public BillingAgreementModal nextBillingDay(Long nextBillingDay) {
    this.nextBillingDay = nextBillingDay;
    return this;
  }

   /**
   * 
   * @return nextBillingDay
  **/
  @ApiModelProperty(value = "")
  public Long getNextBillingDay() {
    return nextBillingDay;
  }

  public void setNextBillingDay(Long nextBillingDay) {
    this.nextBillingDay = nextBillingDay;
  }

  public BillingAgreementModal payerId(String payerId) {
    this.payerId = payerId;
    return this;
  }

   /**
   * 
   * @return payerId
  **/
  @ApiModelProperty(value = "")
  public String getPayerId() {
    return payerId;
  }

  public void setPayerId(String payerId) {
    this.payerId = payerId;
  }

  public BillingAgreementModal paymentGross(Double paymentGross) {
    this.paymentGross = paymentGross;
    return this;
  }

   /**
   * 
   * @return paymentGross
  **/
  @ApiModelProperty(value = "")
  public Double getPaymentGross() {
    return paymentGross;
  }

  public void setPaymentGross(Double paymentGross) {
    this.paymentGross = paymentGross;
  }

  public BillingAgreementModal maxAmount(Double maxAmount) {
    this.maxAmount = maxAmount;
    return this;
  }

   /**
   * 
   * @return maxAmount
  **/
  @ApiModelProperty(value = "")
  public Double getMaxAmount() {
    return maxAmount;
  }

  public void setMaxAmount(Double maxAmount) {
    this.maxAmount = maxAmount;
  }

  public BillingAgreementModal paymentStatus(String paymentStatus) {
    this.paymentStatus = paymentStatus;
    return this;
  }

   /**
   * 
   * @return paymentStatus
  **/
  @ApiModelProperty(value = "")
  public String getPaymentStatus() {
    return paymentStatus;
  }

  public void setPaymentStatus(String paymentStatus) {
    this.paymentStatus = paymentStatus;
  }

  public BillingAgreementModal mpStatus(Long mpStatus) {
    this.mpStatus = mpStatus;
    return this;
  }

   /**
   * 
   * @return mpStatus
  **/
  @ApiModelProperty(value = "")
  public Long getMpStatus() {
    return mpStatus;
  }

  public void setMpStatus(Long mpStatus) {
    this.mpStatus = mpStatus;
  }

  public BillingAgreementModal reasonCode(String reasonCode) {
    this.reasonCode = reasonCode;
    return this;
  }

   /**
   * 
   * @return reasonCode
  **/
  @ApiModelProperty(value = "")
  public String getReasonCode() {
    return reasonCode;
  }

  public void setReasonCode(String reasonCode) {
    this.reasonCode = reasonCode;
  }

  public BillingAgreementModal mpDesc(String mpDesc) {
    this.mpDesc = mpDesc;
    return this;
  }

   /**
   * 
   * @return mpDesc
  **/
  @ApiModelProperty(value = "")
  public String getMpDesc() {
    return mpDesc;
  }

  public void setMpDesc(String mpDesc) {
    this.mpDesc = mpDesc;
  }

  public BillingAgreementModal paymentMethod(String paymentMethod) {
    this.paymentMethod = paymentMethod;
    return this;
  }

   /**
   * 
   * @return paymentMethod
  **/
  @ApiModelProperty(value = "")
  public String getPaymentMethod() {
    return paymentMethod;
  }

  public void setPaymentMethod(String paymentMethod) {
    this.paymentMethod = paymentMethod;
  }

  public BillingAgreementModal ccBrand(String ccBrand) {
    this.ccBrand = ccBrand;
    return this;
  }

   /**
   * 
   * @return ccBrand
  **/
  @ApiModelProperty(value = "")
  public String getCcBrand() {
    return ccBrand;
  }

  public void setCcBrand(String ccBrand) {
    this.ccBrand = ccBrand;
  }

  public BillingAgreementModal ccLAst4(Long ccLAst4) {
    this.ccLAst4 = ccLAst4;
    return this;
  }

   /**
   * 
   * @return ccLAst4
  **/
  @ApiModelProperty(value = "")
  public Long getCcLAst4() {
    return ccLAst4;
  }

  public void setCcLAst4(Long ccLAst4) {
    this.ccLAst4 = ccLAst4;
  }

  public BillingAgreementModal ccExpMonth(Long ccExpMonth) {
    this.ccExpMonth = ccExpMonth;
    return this;
  }

   /**
   * 
   * @return ccExpMonth
  **/
  @ApiModelProperty(value = "")
  public Long getCcExpMonth() {
    return ccExpMonth;
  }

  public void setCcExpMonth(Long ccExpMonth) {
    this.ccExpMonth = ccExpMonth;
  }

  public BillingAgreementModal ccExpYear(Long ccExpYear) {
    this.ccExpYear = ccExpYear;
    return this;
  }

   /**
   * 
   * @return ccExpYear
  **/
  @ApiModelProperty(value = "")
  public Long getCcExpYear() {
    return ccExpYear;
  }

  public void setCcExpYear(Long ccExpYear) {
    this.ccExpYear = ccExpYear;
  }

  public BillingAgreementModal cardId(String cardId) {
    this.cardId = cardId;
    return this;
  }

   /**
   * 
   * @return cardId
  **/
  @ApiModelProperty(value = "")
  public String getCardId() {
    return cardId;
  }

  public void setCardId(String cardId) {
    this.cardId = cardId;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    BillingAgreementModal billingAgreementModal = (BillingAgreementModal) o;
    return Objects.equals(this.billingId, billingAgreementModal.billingId) &&
        Objects.equals(this.userId, billingAgreementModal.userId) &&
        Objects.equals(this.planId, billingAgreementModal.planId) &&
        Objects.equals(this.agreementId, billingAgreementModal.agreementId) &&
        Objects.equals(this.agreementStatus, billingAgreementModal.agreementStatus) &&
        Objects.equals(this.payerEmail, billingAgreementModal.payerEmail) &&
        Objects.equals(this.firstName, billingAgreementModal.firstName) &&
        Objects.equals(this.lastName, billingAgreementModal.lastName) &&
        Objects.equals(this.timeStamp, billingAgreementModal.timeStamp) &&
        Objects.equals(this.dateCreated, billingAgreementModal.dateCreated) &&
        Objects.equals(this.transactionType, billingAgreementModal.transactionType) &&
        Objects.equals(this.closeDate, billingAgreementModal.closeDate) &&
        Objects.equals(this.isActive, billingAgreementModal.isActive) &&
        Objects.equals(this.nextBillingDay, billingAgreementModal.nextBillingDay) &&
        Objects.equals(this.payerId, billingAgreementModal.payerId) &&
        Objects.equals(this.paymentGross, billingAgreementModal.paymentGross) &&
        Objects.equals(this.maxAmount, billingAgreementModal.maxAmount) &&
        Objects.equals(this.paymentStatus, billingAgreementModal.paymentStatus) &&
        Objects.equals(this.mpStatus, billingAgreementModal.mpStatus) &&
        Objects.equals(this.reasonCode, billingAgreementModal.reasonCode) &&
        Objects.equals(this.mpDesc, billingAgreementModal.mpDesc) &&
        Objects.equals(this.paymentMethod, billingAgreementModal.paymentMethod) &&
        Objects.equals(this.ccBrand, billingAgreementModal.ccBrand) &&
        Objects.equals(this.ccLAst4, billingAgreementModal.ccLAst4) &&
        Objects.equals(this.ccExpMonth, billingAgreementModal.ccExpMonth) &&
        Objects.equals(this.ccExpYear, billingAgreementModal.ccExpYear) &&
        Objects.equals(this.cardId, billingAgreementModal.cardId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(billingId, userId, planId, agreementId, agreementStatus, payerEmail, firstName, lastName, timeStamp, dateCreated, transactionType, closeDate, isActive, nextBillingDay, payerId, paymentGross, maxAmount, paymentStatus, mpStatus, reasonCode, mpDesc, paymentMethod, ccBrand, ccLAst4, ccExpMonth, ccExpYear, cardId);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class BillingAgreementModal {\n");
    
    sb.append("    billingId: ").append(toIndentedString(billingId)).append("\n");
    sb.append("    userId: ").append(toIndentedString(userId)).append("\n");
    sb.append("    planId: ").append(toIndentedString(planId)).append("\n");
    sb.append("    agreementId: ").append(toIndentedString(agreementId)).append("\n");
    sb.append("    agreementStatus: ").append(toIndentedString(agreementStatus)).append("\n");
    sb.append("    payerEmail: ").append(toIndentedString(payerEmail)).append("\n");
    sb.append("    firstName: ").append(toIndentedString(firstName)).append("\n");
    sb.append("    lastName: ").append(toIndentedString(lastName)).append("\n");
    sb.append("    timeStamp: ").append(toIndentedString(timeStamp)).append("\n");
    sb.append("    dateCreated: ").append(toIndentedString(dateCreated)).append("\n");
    sb.append("    transactionType: ").append(toIndentedString(transactionType)).append("\n");
    sb.append("    closeDate: ").append(toIndentedString(closeDate)).append("\n");
    sb.append("    isActive: ").append(toIndentedString(isActive)).append("\n");
    sb.append("    nextBillingDay: ").append(toIndentedString(nextBillingDay)).append("\n");
    sb.append("    payerId: ").append(toIndentedString(payerId)).append("\n");
    sb.append("    paymentGross: ").append(toIndentedString(paymentGross)).append("\n");
    sb.append("    maxAmount: ").append(toIndentedString(maxAmount)).append("\n");
    sb.append("    paymentStatus: ").append(toIndentedString(paymentStatus)).append("\n");
    sb.append("    mpStatus: ").append(toIndentedString(mpStatus)).append("\n");
    sb.append("    reasonCode: ").append(toIndentedString(reasonCode)).append("\n");
    sb.append("    mpDesc: ").append(toIndentedString(mpDesc)).append("\n");
    sb.append("    paymentMethod: ").append(toIndentedString(paymentMethod)).append("\n");
    sb.append("    ccBrand: ").append(toIndentedString(ccBrand)).append("\n");
    sb.append("    ccLAst4: ").append(toIndentedString(ccLAst4)).append("\n");
    sb.append("    ccExpMonth: ").append(toIndentedString(ccExpMonth)).append("\n");
    sb.append("    ccExpYear: ").append(toIndentedString(ccExpYear)).append("\n");
    sb.append("    cardId: ").append(toIndentedString(cardId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

