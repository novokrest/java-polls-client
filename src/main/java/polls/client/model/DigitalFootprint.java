/*
 * Polls Web API
 * Poll Service Developers Web API
 *
 * OpenAPI spec version: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package polls.client.model;

import java.util.Objects;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import polls.client.model.Score;
import polls.client.model.Topic;

/**
 * 
 */
@ApiModel(description = "")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2017-09-02T00:44:51.695+03:00")
public class DigitalFootprint {
  @SerializedName("scores")
  private List<Score> scores = null;

  @SerializedName("topics")
  private List<Topic> topics = null;

  public DigitalFootprint scores(List<Score> scores) {
    this.scores = scores;
    return this;
  }

  public DigitalFootprint addScoresItem(Score scoresItem) {
    if (this.scores == null) {
      this.scores = new ArrayList<Score>();
    }
    this.scores.add(scoresItem);
    return this;
  }

   /**
   * 
   * @return scores
  **/
  @ApiModelProperty(value = "")
  public List<Score> getScores() {
    return scores;
  }

  public void setScores(List<Score> scores) {
    this.scores = scores;
  }

  public DigitalFootprint topics(List<Topic> topics) {
    this.topics = topics;
    return this;
  }

  public DigitalFootprint addTopicsItem(Topic topicsItem) {
    if (this.topics == null) {
      this.topics = new ArrayList<Topic>();
    }
    this.topics.add(topicsItem);
    return this;
  }

   /**
   * 
   * @return topics
  **/
  @ApiModelProperty(value = "")
  public List<Topic> getTopics() {
    return topics;
  }

  public void setTopics(List<Topic> topics) {
    this.topics = topics;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DigitalFootprint digitalFootprint = (DigitalFootprint) o;
    return Objects.equals(this.scores, digitalFootprint.scores) &&
        Objects.equals(this.topics, digitalFootprint.topics);
  }

  @Override
  public int hashCode() {
    return Objects.hash(scores, topics);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DigitalFootprint {\n");
    
    sb.append("    scores: ").append(toIndentedString(scores)).append("\n");
    sb.append("    topics: ").append(toIndentedString(topics)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

