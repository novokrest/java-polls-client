/*
 * Polls Web API
 * Poll Service Developers Web API
 *
 * OpenAPI spec version: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package polls.client.model;

import java.util.Objects;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 */
@ApiModel(description = "")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2017-12-15T09:23:34.748+03:00")
public class Photo {
  @SerializedName("height")
  private Long height = null;

  @SerializedName("html_attributions")
  private List<String> htmlAttributions = null;

  @SerializedName("photo_reference")
  private String photoReference = null;

  @SerializedName("width")
  private Long width = null;

  public Photo height(Long height) {
    this.height = height;
    return this;
  }

   /**
   * Gets or sets the height.
   * @return height
  **/
  @ApiModelProperty(value = "Gets or sets the height.")
  public Long getHeight() {
    return height;
  }

  public void setHeight(Long height) {
    this.height = height;
  }

  public Photo htmlAttributions(List<String> htmlAttributions) {
    this.htmlAttributions = htmlAttributions;
    return this;
  }

  public Photo addHtmlAttributionsItem(String htmlAttributionsItem) {
    if (this.htmlAttributions == null) {
      this.htmlAttributions = new ArrayList<String>();
    }
    this.htmlAttributions.add(htmlAttributionsItem);
    return this;
  }

   /**
   * Gets or sets the HTML attributions.
   * @return htmlAttributions
  **/
  @ApiModelProperty(value = "Gets or sets the HTML attributions.")
  public List<String> getHtmlAttributions() {
    return htmlAttributions;
  }

  public void setHtmlAttributions(List<String> htmlAttributions) {
    this.htmlAttributions = htmlAttributions;
  }

  public Photo photoReference(String photoReference) {
    this.photoReference = photoReference;
    return this;
  }

   /**
   * Gets or sets the photo reference.
   * @return photoReference
  **/
  @ApiModelProperty(value = "Gets or sets the photo reference.")
  public String getPhotoReference() {
    return photoReference;
  }

  public void setPhotoReference(String photoReference) {
    this.photoReference = photoReference;
  }

  public Photo width(Long width) {
    this.width = width;
    return this;
  }

   /**
   * Gets or sets the width.
   * @return width
  **/
  @ApiModelProperty(value = "Gets or sets the width.")
  public Long getWidth() {
    return width;
  }

  public void setWidth(Long width) {
    this.width = width;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Photo photo = (Photo) o;
    return Objects.equals(this.height, photo.height) &&
        Objects.equals(this.htmlAttributions, photo.htmlAttributions) &&
        Objects.equals(this.photoReference, photo.photoReference) &&
        Objects.equals(this.width, photo.width);
  }

  @Override
  public int hashCode() {
    return Objects.hash(height, htmlAttributions, photoReference, width);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Photo {\n");
    
    sb.append("    height: ").append(toIndentedString(height)).append("\n");
    sb.append("    htmlAttributions: ").append(toIndentedString(htmlAttributions)).append("\n");
    sb.append("    photoReference: ").append(toIndentedString(photoReference)).append("\n");
    sb.append("    width: ").append(toIndentedString(width)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

