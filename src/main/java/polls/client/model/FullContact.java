/*
 * Polls Web API
 * Poll Service Developers Web API
 *
 * OpenAPI spec version: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package polls.client.model;

import java.util.Objects;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * 
 */
@ApiModel(description = "")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2017-12-15T09:23:34.748+03:00")
public class FullContact {
  @SerializedName("status")
  private Long status = null;

  @SerializedName("requestId")
  private String requestId = null;

  @SerializedName("likelihood")
  private Double likelihood = null;

  @SerializedName("photos")
  private String photos = null;

  public FullContact status(Long status) {
    this.status = status;
    return this;
  }

   /**
   * 
   * @return status
  **/
  @ApiModelProperty(value = "")
  public Long getStatus() {
    return status;
  }

  public void setStatus(Long status) {
    this.status = status;
  }

  public FullContact requestId(String requestId) {
    this.requestId = requestId;
    return this;
  }

   /**
   * 
   * @return requestId
  **/
  @ApiModelProperty(value = "")
  public String getRequestId() {
    return requestId;
  }

  public void setRequestId(String requestId) {
    this.requestId = requestId;
  }

  public FullContact likelihood(Double likelihood) {
    this.likelihood = likelihood;
    return this;
  }

   /**
   * 
   * @return likelihood
  **/
  @ApiModelProperty(value = "")
  public Double getLikelihood() {
    return likelihood;
  }

  public void setLikelihood(Double likelihood) {
    this.likelihood = likelihood;
  }

  public FullContact photos(String photos) {
    this.photos = photos;
    return this;
  }

   /**
   * 
   * @return photos
  **/
  @ApiModelProperty(value = "")
  public String getPhotos() {
    return photos;
  }

  public void setPhotos(String photos) {
    this.photos = photos;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    FullContact fullContact = (FullContact) o;
    return Objects.equals(this.status, fullContact.status) &&
        Objects.equals(this.requestId, fullContact.requestId) &&
        Objects.equals(this.likelihood, fullContact.likelihood) &&
        Objects.equals(this.photos, fullContact.photos);
  }

  @Override
  public int hashCode() {
    return Objects.hash(status, requestId, likelihood, photos);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class FullContact {\n");
    
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    requestId: ").append(toIndentedString(requestId)).append("\n");
    sb.append("    likelihood: ").append(toIndentedString(likelihood)).append("\n");
    sb.append("    photos: ").append(toIndentedString(photos)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

