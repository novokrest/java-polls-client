package polls.client.api.push;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.SendMessageViewModel;
import polls.client.model.HttpResponseMessage;
import polls.client.api.utils.WordGenerator;
import java.util.UUID;
import java.util.List;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class SendWebPushNotificationsTestTest extends PushApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws ApiException {
        // given
        SendMessageViewModel sendMessageViewModel = new SendMessageViewModel();

        sendMessageViewModel.setChannelId(WordGenerator.generateWord());
        sendMessageViewModel.setSnsType(WordGenerator.generateWord());
        sendMessageViewModel.setMessageToSend(WordGenerator.generateWord());

        // when
        HttpResponseMessage response = makeRequest(sendMessageViewModel);

        // then
        assertNotNull(response);
        assertNotNull(response.getVersion());
        assertNotNull(response.getContent());
        assertNotNull(response.getStatusCode());
        assertNotNull(response.getReasonPhrase());
        assertNotNull(response.getHeaders());
        assertNotNull(response.getRequestMessage());
        assertNotNull(response.getIsSuccessStatusCode());
    }

    private HttpResponseMessage makeRequest(SendMessageViewModel sendMessageViewModel) throws ApiException {
        return authRequestExecutor.execute(() -> {
            return api.sendWebPushNotificationsTest(sendMessageViewModel); 
        });
    }
}