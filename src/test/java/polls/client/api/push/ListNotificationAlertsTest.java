package polls.client.api.push;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.RequestId;
import polls.client.model.NotificationTrackingModel;
import polls.client.api.utils.UuidGenerator;
import java.util.UUID;
import java.util.List;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class ListNotificationAlertsTest extends PushApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws ApiException {
        // given
        RequestId requestId = new RequestId();

        requestId.setId(UuidGenerator.generateUUID());

        // when
        List<NotificationTrackingModel> response = makeRequest(requestId);

        // then
        assertNotNull(response);
    }

    private List<NotificationTrackingModel> makeRequest(RequestId requestId) throws ApiException {
        return authRequestExecutor.execute(() -> {
            return api.listNotificationAlerts(requestId); 
        });
    }
}