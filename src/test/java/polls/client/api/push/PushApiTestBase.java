package polls.client.api.push;

import polls.client.api.PushApi;
import polls.client.Configuration;

import org.junit.Before;
import org.junit.Ignore;

@Ignore
public class PushApiTestBase {

    private static final int TIMEOUT = 0;

    protected final PushApi api = new PushApi();

    @Before
    public void before() {
        Configuration.getDefaultApiClient().setConnectTimeout(TIMEOUT);
    }
}