package polls.client.api.push;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.RegisterDeviceSNS;
import java.util.UUID;
import java.util.List;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class RegisterSnsDeviceTest extends PushApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws ApiException {
        // given
        RegisterDeviceSNS registerDeviceSNS = new RegisterDeviceSNS();


        // when
        String response = makeRequest(registerDeviceSNS);

        // then
        assertNotNull(response);
    }

    private String makeRequest(RegisterDeviceSNS registerDeviceSNS) throws ApiException {
        return authRequestExecutor.execute(() -> {
            return api.registerSnsDevice(registerDeviceSNS); 
        });
    }
}