package polls.client.api.business;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.PollsStatsCounts;
import polls.client.api.utils.UuidGenerator;
import java.util.UUID;
import java.util.List;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class GetPollCountersTest extends BusinessApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws ApiException {
        // given
        UUID userId = UuidGenerator.generateUUID();


        // when
        PollsStatsCounts response = makeRequest(userId);

        // then
        assertNotNull(response);
        assertNotNull(response.getCompleted());
        assertNotNull(response.getDraft());
        assertNotNull(response.getExpired());
        assertNotNull(response.getInProgress());
        assertNotNull(response.getPending());
        assertNotNull(response.getTotalPolls());
    }

    private PollsStatsCounts makeRequest(UUID userId) throws ApiException {
        return authRequestExecutor.execute(() -> {
            return api.getPollCounters(userId); 
        });
    }
}