package polls.client.api.business;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.CompanyUsers;
import polls.client.model.StatusResponse;
import polls.client.api.utils.WordGenerator;
import polls.client.api.utils.EmailGenerator;
import polls.client.api.utils.DeviceModelGenerator;
import polls.client.api.utils.ContactGenerator;
import java.util.UUID;
import java.util.List;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class AddNewEmployeeTest extends BusinessApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws ApiException {
        // given
        CompanyUsers companyUsers = new CompanyUsers();

        companyUsers.setUserName(WordGenerator.generateName(10));
        companyUsers.setEmailAddress(EmailGenerator.generateEmail());
        companyUsers.setPassword(WordGenerator.generatePassword());
        companyUsers.setNewDevice(DeviceModelGenerator.generateDeviceModel());
        companyUsers.setNewProfileContact(ContactGenerator.generateContact());

        // when
        StatusResponse response = makeRequest(companyUsers);

        // then
        assertNotNull(response);
        assertNotNull(response.getStatus());
        assertNotNull(response.getStatusMessage());
    }

    private StatusResponse makeRequest(CompanyUsers companyUsers) throws ApiException {
        return authRequestExecutor.execute(() -> {
            return api.addNewEmployee(companyUsers); 
        });
    }
}