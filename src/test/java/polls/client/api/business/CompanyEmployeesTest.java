package polls.client.api.business;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.UserIdModel;
import polls.client.model.Contact;
import java.util.UUID;
import java.util.List;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class CompanyEmployeesTest extends BusinessApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws ApiException {
        // given
        UserIdModel userIdModel = new UserIdModel();


        // when
        List<Contact> response = makeRequest(userIdModel);

        // then
        assertNotNull(response);
    }

    private List<Contact> makeRequest(UserIdModel userIdModel) throws ApiException {
        return authRequestExecutor.execute(() -> {
            return api.companyEmployees(userIdModel); 
        });
    }
}