package polls.client.api.business;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.AnswerModel;
import polls.client.model.StatusResponse;
import polls.client.api.utils.Random;
import polls.client.api.utils.UuidGenerator;
import java.util.UUID;
import java.util.List;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class PostPollAnswerTest extends BusinessApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws ApiException {
        // given
        AnswerModel answerModel = new AnswerModel();

        answerModel.setPollId(Random.nextLong(1L, 10L));
        answerModel.setAnswerChoice(1);
        answerModel.setWorkerUserId(UuidGenerator.generateUUID());

        // when
        StatusResponse response = makeRequest(answerModel);

        // then
        assertNotNull(response);
        assertNotNull(response.getStatus());
        assertNotNull(response.getStatusMessage());
    }

    private StatusResponse makeRequest(AnswerModel answerModel) throws ApiException {
        return authRequestExecutor.execute(() -> {
            return api.postPollAnswer(answerModel); 
        });
    }
}