package polls.client.api.business;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.UserInvitation;
import polls.client.model.StatusResponse;
import java.util.UUID;
import java.util.List;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class ResponseInvitationTest extends BusinessApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws ApiException {
        // given
        UserInvitation userInvitation = new UserInvitation();


        // when
        StatusResponse response = makeRequest(userInvitation);

        // then
        assertNotNull(response);
        assertNotNull(response.getStatus());
        assertNotNull(response.getStatusMessage());
    }

    private StatusResponse makeRequest(UserInvitation userInvitation) throws ApiException {
        return authRequestExecutor.execute(() -> {
            return api.responseInvitation(userInvitation); 
        });
    }
}