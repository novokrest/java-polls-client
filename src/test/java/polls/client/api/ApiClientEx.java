package polls.client.api;

import polls.client.ApiClient;
import polls.client.api.auth.Authorization;
import polls.client.auth.ApiKeyAuth;


public final class ApiClientEx {

    private ApiClientEx() { }

    public static void setAuth(ApiClient api, Authorization authorization) {
        ApiKeyAuth userAuth = (ApiKeyAuth) api.getAuthentication("apiUserId");
        userAuth.setApiKey(authorization.getUserId().toString());

        ApiKeyAuth tokenAuth = (ApiKeyAuth) api.getAuthentication("apiToken");
        tokenAuth.setApiKey(authorization.getToken().toString());
    }
}



