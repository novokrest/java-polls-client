package polls.client.api.publicPoll;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.PollRequest;
import polls.client.model.PollResult;
import java.util.UUID;
import java.util.List;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class GetPollResultTest extends PublicPollApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws ApiException {
        // given
        PollRequest pollRequest = new PollRequest();


        // when
        PollResult response = makeRequest(pollRequest);

        // then
        assertNotNull(response);
        assertNotNull(response.getPollId());
        assertNotNull(response.getOwnerId());
        assertNotNull(response.getPollDetailId());
        assertNotNull(response.getWorkerId());
        assertNotNull(response.getAcceptTime());
        assertNotNull(response.getChoice());
        assertNotNull(response.getAnswerText());
        assertNotNull(response.getThumbsUp());
        assertNotNull(response.getThumbsDown());
        assertNotNull(response.getEnablePublic());
        assertNotNull(response.getIdCat());
        assertNotNull(response.getGender());
        assertNotNull(response.getAge());
        assertNotNull(response.getIncome());
        assertNotNull(response.getEthnicity());
        assertNotNull(response.getEducation());
        assertNotNull(response.getRelationshipStatus());
        assertNotNull(response.getNumberofChildren());
        assertNotNull(response.getSmoking());
        assertNotNull(response.getDietType());
        assertNotNull(response.getHomeType());
        assertNotNull(response.getCommunity());
        assertNotNull(response.getPolitics());
        assertNotNull(response.getMobileDevice());
        assertNotNull(response.getLiteraryPreference());
        assertNotNull(response.getBooksReadPerMonth());
        assertNotNull(response.getEmploymentStatus());
        assertNotNull(response.getThumbsStatus());
    }

    private PollResult makeRequest(PollRequest pollRequest) throws ApiException {
        return authRequestExecutor.execute(() -> {
            return api.getPollResult(pollRequest); 
        });
    }
}