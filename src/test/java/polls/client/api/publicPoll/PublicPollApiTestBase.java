package polls.client.api.publicPoll;

import polls.client.api.PublicPollApi;
import polls.client.Configuration;

import org.junit.Before;
import org.junit.Ignore;

@Ignore
public class PublicPollApiTestBase {

    private static final int TIMEOUT = 0;

    protected final PublicPollApi api = new PublicPollApi();

    @Before
    public void before() {
        Configuration.getDefaultApiClient().setConnectTimeout(TIMEOUT);
    }
}