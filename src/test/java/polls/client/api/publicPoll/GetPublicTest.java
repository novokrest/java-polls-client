package polls.client.api.publicPoll;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.SearchOptions;
import polls.client.model.PublicPollViewModel;
import java.util.UUID;
import java.util.List;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class GetPublicTest extends PublicPollApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws ApiException {
        // given
        SearchOptions searchOptions = new SearchOptions();


        // when
        List<PublicPollViewModel> response = makeRequest(searchOptions);

        // then
        assertNotNull(response);
    }

    private List<PublicPollViewModel> makeRequest(SearchOptions searchOptions) throws ApiException {
        return authRequestExecutor.execute(() -> {
            return api.getPublic(searchOptions); 
        });
    }
}