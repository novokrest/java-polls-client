package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.FilterCountModel;
import polls.client.model.FilterDataCount;
import java.util.UUID;
import java.util.List;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class GetFilterDataCountTest extends PollsApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws ApiException {
        // given
        FilterCountModel filterCountModel = new FilterCountModel();


        // when
        FilterDataCount response = makeRequest(filterCountModel);

        // then
        assertNotNull(response);
        assertNotNull(response.getFilterCount());
    }

    private FilterDataCount makeRequest(FilterCountModel filterCountModel) throws ApiException {
        return authRequestExecutor.execute(() -> {
            return api.getFilterDataCount(filterCountModel); 
        });
    }
}