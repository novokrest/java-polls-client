package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.SuggestQuestionViewModel;
import java.util.UUID;
import java.util.List;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class GetQuestionsTest extends PollsApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws ApiException {
        // given

        // when
        List<SuggestQuestionViewModel> response = makeRequest();

        // then
        assertNotNull(response);
    }

    private List<SuggestQuestionViewModel> makeRequest() throws ApiException {
        return authRequestExecutor.execute(() -> {
            return api.getQuestions(); 
        });
    }
}