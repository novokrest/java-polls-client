package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.FilterCategoryModel;
import java.util.UUID;
import java.util.List;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class GetAllFilterCategoriesTest extends PollsApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws ApiException {
        // given

        // when
        FilterCategoryModel response = makeRequest();

        // then
        assertNotNull(response);
        assertNotNull(response.getCategoryName());
        assertNotNull(response.getFilter1());
        assertNotNull(response.getFilter2());
        assertNotNull(response.getFilter3());
        assertNotNull(response.getFilter4());
    }

    private FilterCategoryModel makeRequest() throws ApiException {
        return authRequestExecutor.execute(() -> {
            return api.getAllFilterCategories(); 
        });
    }
}