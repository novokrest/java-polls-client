package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.FilterRequest;
import polls.client.model.FilterData;
import java.util.UUID;
import java.util.List;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class GetAllFiltersTest extends PollsApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws ApiException {
        // given
        FilterRequest filterRequest = new FilterRequest();


        // when
        FilterData response = makeRequest(filterRequest);

        // then
        assertNotNull(response);
        assertNotNull(response.getFilters());
    }

    private FilterData makeRequest(FilterRequest filterRequest) throws ApiException {
        return authRequestExecutor.execute(() -> {
            return api.getAllFilters(filterRequest); 
        });
    }
}