package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.WorkerRequest;
import polls.client.model.WorkerApiModel;
import polls.client.api.utils.WordGenerator;
import java.util.UUID;
import java.util.List;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class GetWorkerDetailsTest extends PollsApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws ApiException {
        // given
        WorkerRequest workerRequest = new WorkerRequest();

        workerRequest.setWorkerId(WordGenerator.generateWord());

        // when
        WorkerApiModel response = makeRequest(workerRequest);

        // then
        assertNotNull(response);
        assertNotNull(response.getWorkerId());
        assertNotNull(response.getGender());
        assertNotNull(response.getEducationId());
        assertNotNull(response.getEducation());
        assertNotNull(response.getAge());
        assertNotNull(response.getIncome());
        assertNotNull(response.getThumbsUp());
        assertNotNull(response.getThumbsDown());
    }

    private WorkerApiModel makeRequest(WorkerRequest workerRequest) throws ApiException {
        return authRequestExecutor.execute(() -> {
            return api.getWorkerDetails(workerRequest); 
        });
    }
}