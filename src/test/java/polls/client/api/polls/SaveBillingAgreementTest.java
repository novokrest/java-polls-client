package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.AgreementViewModel;
import polls.client.model.StatusResponse;
import polls.client.api.utils.UuidGenerator;
import polls.client.api.utils.WordGenerator;
import polls.client.api.utils.EmailGenerator;
import polls.client.api.utils.Random;
import java.util.UUID;
import java.util.List;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class SaveBillingAgreementTest extends PollsApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws ApiException {
        // given
        AgreementViewModel agreementViewModel = new AgreementViewModel();

        agreementViewModel.setPlanId(UuidGenerator.generateUUID());
        agreementViewModel.setAgreementId(WordGenerator.generateWord());
        agreementViewModel.setPayerEmail(EmailGenerator.generateEmail());
        agreementViewModel.setFirstName(WordGenerator.generateName(10));
        agreementViewModel.setLastName(WordGenerator.generateName(10));
        agreementViewModel.setNextBillingDay(Random.nextLong(1L, 10L));
        agreementViewModel.setPayerId(WordGenerator.generateWord());
        agreementViewModel.setPaymentGross(Random.nextDouble(1.00D, 100.00D));
        agreementViewModel.setPaymentMethod(WordGenerator.generateWord());

        // when
        StatusResponse response = makeRequest(agreementViewModel);

        // then
        assertNotNull(response);
        assertNotNull(response.getStatus());
        assertNotNull(response.getStatusMessage());
    }

    private StatusResponse makeRequest(AgreementViewModel agreementViewModel) throws ApiException {
        return authRequestExecutor.execute(() -> {
            return api.saveBillingAgreement(agreementViewModel); 
        });
    }
}