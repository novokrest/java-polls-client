package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.ExtendPollRequest;
import polls.client.model.StatusResponse;
import polls.client.api.utils.Random;
import java.util.UUID;
import java.util.List;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class ExtendExpireTimeTest extends PollsApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws ApiException {
        // given
        ExtendPollRequest extendPollRequest = new ExtendPollRequest();

        extendPollRequest.setPollId(Random.nextLong(1L, 10L));
        extendPollRequest.setExpirationIncreamentInSeconds(Random.nextLong(1L, 10L));

        // when
        StatusResponse response = makeRequest(extendPollRequest);

        // then
        assertNotNull(response);
        assertNotNull(response.getStatus());
        assertNotNull(response.getStatusMessage());
    }

    private StatusResponse makeRequest(ExtendPollRequest extendPollRequest) throws ApiException {
        return authRequestExecutor.execute(() -> {
            return api.extendExpireTime(extendPollRequest); 
        });
    }
}