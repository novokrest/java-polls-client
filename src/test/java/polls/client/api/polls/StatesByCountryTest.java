package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.RequestNameModel;
import polls.client.model.StatesModel;
import java.util.UUID;
import java.util.List;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class StatesByCountryTest extends PollsApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws ApiException {
        // given
        RequestNameModel requestNameModel = new RequestNameModel();


        // when
        List<StatesModel> response = makeRequest(requestNameModel);

        // then
        assertNotNull(response);
    }

    private List<StatesModel> makeRequest(RequestNameModel requestNameModel) throws ApiException {
        return authRequestExecutor.execute(() -> {
            return api.statesByCountry(requestNameModel); 
        });
    }
}