package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.FilterRequest;
import polls.client.model.FilterDataModel;
import java.util.UUID;
import java.util.List;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class GetFilterOptionsListTest extends PollsApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws ApiException {
        // given
        FilterRequest filterRequest = new FilterRequest();


        // when
        List<FilterDataModel> response = makeRequest(filterRequest);

        // then
        assertNotNull(response);
    }

    private List<FilterDataModel> makeRequest(FilterRequest filterRequest) throws ApiException {
        return authRequestExecutor.execute(() -> {
            return api.getFilterOptionsList(filterRequest); 
        });
    }
}