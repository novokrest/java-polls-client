package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.RegisterViewModel;
import polls.client.model.RegisterResponse;
import polls.client.api.utils.WordGenerator;
import polls.client.api.utils.EmailGenerator;
import polls.client.api.utils.DeviceModelGenerator;
import polls.client.api.utils.ContactGenerator;
import java.util.UUID;
import java.util.List;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class RegistrationTest extends PollsApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws ApiException {
        // given
        RegisterViewModel registerViewModel = new RegisterViewModel();

        registerViewModel.setUserName(WordGenerator.generateName(10));
        registerViewModel.setEmailAddress(EmailGenerator.generateEmail());
        registerViewModel.setPassword(WordGenerator.generatePassword());
        registerViewModel.setNewDevice(DeviceModelGenerator.generateDeviceModel());
        registerViewModel.setNewProfileContact(ContactGenerator.generateContact());

        // when
        RegisterResponse response = makeRequest(registerViewModel);

        // then
        assertNotNull(response);
        assertNotNull(response.getUserId());
        assertNotNull(response.getToken());
        assertNotNull(response.getDisplayName());
        assertNotNull(response.getAccessLevel());
        assertNotNull(response.getMessage());
        assertNotNull(response.getIsEmailVerified());
        assertNotNull(response.getIsPhoneVerified());
        assertNotNull(response.getIsCallVerified());
    }

    private RegisterResponse makeRequest(RegisterViewModel registerViewModel) throws ApiException {
        return authRequestExecutor.execute(() -> {
            return api.registration(registerViewModel); 
        });
    }
}