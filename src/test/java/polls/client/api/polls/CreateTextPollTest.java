package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.SingleText;
import polls.client.model.StatusResponse;
import polls.client.api.utils.WordGenerator;
import polls.client.api.utils.Random;
import java.util.UUID;
import java.util.List;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class CreateTextPollTest extends PollsApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws ApiException {
        // given
        SingleText singleText = new SingleText();

        singleText.setPollTitle(WordGenerator.generateWord());
        singleText.setQuestion(WordGenerator.generateWord());
        singleText.setPollOverview(WordGenerator.generateWord());
        singleText.setAssignmentDurationInSeconds(Random.nextLong(1L, 10L));
        singleText.setKeywords(WordGenerator.generateWord());
        singleText.setTags(WordGenerator.generateWord());
        singleText.setLifeTimeInSeconds(Random.nextLong(1L, 10L));

        // when
        StatusResponse response = makeRequest(singleText);

        // then
        assertNotNull(response);
        assertNotNull(response.getStatus());
        assertNotNull(response.getStatusMessage());
    }

    private StatusResponse makeRequest(SingleText singleText) throws ApiException {
        return authRequestExecutor.execute(() -> {
            return api.createTextPoll(singleText); 
        });
    }
}