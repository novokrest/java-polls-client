package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.DeviceInfoModel;
import java.util.UUID;
import java.util.List;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class GetLoginHistoryTest extends PollsApiTestBase {

    @Test
    public void testValidRequest() throws ApiException {
        // given

        // when
        List<DeviceInfoModel> response = makeRequest();

        // then
        assertNotNull(response);
    }

    private List<DeviceInfoModel> makeRequest() throws ApiException {
        return api.getLoginHistory(); 
    }
}