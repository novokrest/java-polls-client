package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.ChangePasswordModel;
import polls.client.model.StatusResponse;
import polls.client.api.utils.WordGenerator;
import java.util.UUID;
import java.util.List;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class ChangePasswordTest extends PollsApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws ApiException {
        // given
        ChangePasswordModel changePasswordModel = new ChangePasswordModel();

        changePasswordModel.setOldPassword(WordGenerator.generatePassword());
        changePasswordModel.setNewPassword(WordGenerator.generatePassword());

        // when
        StatusResponse response = makeRequest(changePasswordModel);

        // then
        assertNotNull(response);
        assertNotNull(response.getStatus());
        assertNotNull(response.getStatusMessage());
    }

    private StatusResponse makeRequest(ChangePasswordModel changePasswordModel) throws ApiException {
        return authRequestExecutor.execute(() -> {
            return api.changePassword(changePasswordModel); 
        });
    }
}