package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.ApiLoginModel;
import polls.client.model.LoginResponse;
import polls.client.api.utils.WordGenerator;
import polls.client.api.utils.DeviceModelGenerator;
import java.util.UUID;
import java.util.List;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class LoginTest extends PollsApiTestBase {

    @Test
    public void testValidRequest() throws ApiException {
        // given
        ApiLoginModel apiLoginModel = new ApiLoginModel();

        apiLoginModel.setUserName("samer");
        apiLoginModel.setPassword("123456");
        apiLoginModel.setNewDevice(DeviceModelGenerator.generateDeviceModel());

        // when
        LoginResponse response = makeRequest(apiLoginModel);

        // then
        assertNotNull(response);
        assertNotNull(response.getUserName());
        assertNotNull(response.getDeviceId());
        assertNotNull(response.getIsPartner());
        assertNotNull(response.getAccessLevel());
        assertNotNull(response.getUserId());
        assertNotNull(response.getToken());
        assertNotNull(response.getDisplayName());
        assertNotNull(response.getIsEmailVerified());
        assertNotNull(response.getIsPhoneVerified());
        assertNotNull(response.getIsCallVerified());
        assertNotNull(response.getUserType());
        assertNotNull(response.getSecretAccessKey());
    }

    private LoginResponse makeRequest(ApiLoginModel apiLoginModel) throws ApiException {
        return api.login(apiLoginModel); 
    }
}