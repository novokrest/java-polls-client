package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.UpdatePollRequest;
import polls.client.model.PollResponse;
import polls.client.api.utils.Random;
import polls.client.api.utils.WordGenerator;
import java.util.UUID;
import java.util.List;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class UpdatePublishedPollTest extends PollsApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws ApiException {
        // given
        UpdatePollRequest updatePollRequest = new UpdatePollRequest();

        updatePollRequest.setPollId(Random.nextLong(1L, 10L));
        updatePollRequest.setKeywords(WordGenerator.generateWord());
        updatePollRequest.setPollOverview(WordGenerator.generateWord());

        // when
        PollResponse response = makeRequest(updatePollRequest);

        // then
        assertNotNull(response);
        assertNotNull(response.getStatus());
    }

    private PollResponse makeRequest(UpdatePollRequest updatePollRequest) throws ApiException {
        return authRequestExecutor.execute(() -> {
            return api.updatePublishedPoll(updatePollRequest); 
        });
    }
}