package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.PaymentPlanResponse;
import java.util.UUID;
import java.util.List;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class GetPaymentPlansTest extends PollsApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws ApiException {
        // given

        // when
        PaymentPlanResponse response = makeRequest();

        // then
        assertNotNull(response);
        assertNotNull(response.getLevelName());
        assertNotNull(response.getLevelDescription());
        assertNotNull(response.getNumberResponses());
        assertNotNull(response.getCredits());
        assertNotNull(response.getCreditPrice());
        assertNotNull(response.getPlanId());
        assertNotNull(response.getProductId());
        assertNotNull(response.getIsPremium());
        assertNotNull(response.getIsSubsribed());
    }

    private PaymentPlanResponse makeRequest() throws ApiException {
        return authRequestExecutor.execute(() -> {
            return api.getPaymentPlans(); 
        });
    }
}