package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.PollRequest;
import polls.client.model.ListPollModel;
import java.util.UUID;
import java.util.List;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class GetMyAllPollsTest extends PollsApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws ApiException {
        // given
        PollRequest pollRequest = new PollRequest();


        // when
        ListPollModel response = makeRequest(pollRequest);

        // then
        assertNotNull(response);
        assertNotNull(response.getPollId());
        assertNotNull(response.getHitId());
        assertNotNull(response.getPollTypeId());
        assertNotNull(response.getCreateDate());
        assertNotNull(response.getPollStatus());
        assertNotNull(response.getPollTitle());
        assertNotNull(response.getQuestion());
        assertNotNull(response.getKeywords());
        assertNotNull(response.getExpirationDate());
        assertNotNull(response.getPollOverview());
        assertNotNull(response.getFirstOption());
        assertNotNull(response.getSecondOption());
        assertNotNull(response.getFirstImagePath());
        assertNotNull(response.getSecondImagePath());
        assertNotNull(response.getAssignmentDurationInSeconds());
        assertNotNull(response.getMaxAssignments());
        assertNotNull(response.getIsPublished());
        assertNotNull(response.getLifeTimeInSeconds());
        assertNotNull(response.getChoiceFirstCounter());
        assertNotNull(response.getChoiceSecondCounter());
        assertNotNull(response.getResultCounter());
        assertNotNull(response.getIsFilterOption());
        assertNotNull(response.getFilterMainCategory());
        assertNotNull(response.getFiltersJson());
        assertNotNull(response.getIsEnablePublic());
        assertNotNull(response.getCatId());
        assertNotNull(response.getCatName());
        assertNotNull(response.getUserName());
        assertNotNull(response.getOwnerId());
        assertNotNull(response.getTotalCount());
        assertNotNull(response.getIsAdult());
        assertNotNull(response.getCompletedOn());
        assertNotNull(response.getTags());
        assertNotNull(response.getAlllowedAnonymously());
        assertNotNull(response.getIsEnabledComments());
        assertNotNull(response.getDemographicCompleted());
    }

    private ListPollModel makeRequest(PollRequest pollRequest) throws ApiException {
        return authRequestExecutor.execute(() -> {
            return api.getMyAllPolls(pollRequest); 
        });
    }
}