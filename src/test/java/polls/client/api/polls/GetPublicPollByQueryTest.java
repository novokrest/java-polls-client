package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.SearchOptions;
import polls.client.model.ListPollModel;
import java.util.UUID;
import java.util.List;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class GetPublicPollByQueryTest extends PollsApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws ApiException {
        // given
        SearchOptions searchOptions = new SearchOptions();


        // when
        List<ListPollModel> response = makeRequest(searchOptions);

        // then
        assertNotNull(response);
    }

    private List<ListPollModel> makeRequest(SearchOptions searchOptions) throws ApiException {
        return authRequestExecutor.execute(() -> {
            return api.getPublicPollByQuery(searchOptions); 
        });
    }
}