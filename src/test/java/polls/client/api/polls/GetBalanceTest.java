package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.UserCreditModel;
import java.util.UUID;
import java.util.List;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class GetBalanceTest extends PollsApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws ApiException {
        // given

        // when
        UserCreditModel response = makeRequest();

        // then
        assertNotNull(response);
        assertNotNull(response.getUserId());
        assertNotNull(response.getDateCreated());
        assertNotNull(response.getBalanceAmount());
        assertNotNull(response.getModifiedDate());
        assertNotNull(response.getCreditBalance());
        assertNotNull(response.getPollsBalance());
        assertNotNull(response.getNextBillingDay());
        assertNotNull(response.getLastPaymentDate());
        assertNotNull(response.getMemberSince());
    }

    private UserCreditModel makeRequest() throws ApiException {
        return authRequestExecutor.execute(() -> {
            return api.getBalance(); 
        });
    }
}