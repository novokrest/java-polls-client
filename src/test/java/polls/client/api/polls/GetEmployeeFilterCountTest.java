package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.FilterRequestObject;
import polls.client.model.StatusResponse;
import java.util.UUID;
import java.util.List;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class GetEmployeeFilterCountTest extends PollsApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws ApiException {
        // given
        FilterRequestObject filterRequestObject = new FilterRequestObject();


        // when
        StatusResponse response = makeRequest(filterRequestObject);

        // then
        assertNotNull(response);
        assertNotNull(response.getStatus());
        assertNotNull(response.getStatusMessage());
    }

    private StatusResponse makeRequest(FilterRequestObject filterRequestObject) throws ApiException {
        return authRequestExecutor.execute(() -> {
            return api.getEmployeeFilterCount(filterRequestObject); 
        });
    }
}