package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.ThumbsRequest;
import polls.client.model.ThumbsResponse;
import java.util.UUID;
import java.util.List;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class ThumbsUpDownTest extends PollsApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws ApiException {
        // given
        ThumbsRequest thumbsRequest = new ThumbsRequest();


        // when
        ThumbsResponse response = makeRequest(thumbsRequest);

        // then
        assertNotNull(response);
        assertNotNull(response.getStatus());
        assertNotNull(response.getRatingChoice());
    }

    private ThumbsResponse makeRequest(ThumbsRequest thumbsRequest) throws ApiException {
        return authRequestExecutor.execute(() -> {
            return api.thumbsUpDown(thumbsRequest); 
        });
    }
}