package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.ResetPasswordRequest;
import polls.client.model.PwdResetResponse;
import polls.client.api.utils.EmailGenerator;
import java.util.UUID;
import java.util.List;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class ResetPasswordRequestTest extends PollsApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws ApiException {
        // given
        ResetPasswordRequest resetPasswordRequest = new ResetPasswordRequest();

        resetPasswordRequest.setEmailAddress(EmailGenerator.generateEmail());

        // when
        PwdResetResponse response = makeRequest(resetPasswordRequest);

        // then
        assertNotNull(response);
        assertNotNull(response.getStatusMessage());
    }

    private PwdResetResponse makeRequest(ResetPasswordRequest resetPasswordRequest) throws ApiException {
        return authRequestExecutor.execute(() -> {
            return api.resetPasswordRequest(resetPasswordRequest); 
        });
    }
}