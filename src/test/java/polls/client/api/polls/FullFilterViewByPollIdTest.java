package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.RequestPollId;
import polls.client.model.FilterData;
import polls.client.api.utils.Random;
import java.util.UUID;
import java.util.List;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class FullFilterViewByPollIdTest extends PollsApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws ApiException {
        // given
        RequestPollId requestPollId = new RequestPollId();

        requestPollId.setPollId(Random.nextLong(1L, 10L));

        // when
        FilterData response = makeRequest(requestPollId);

        // then
        assertNotNull(response);
        assertNotNull(response.getFilters());
    }

    private FilterData makeRequest(RequestPollId requestPollId) throws ApiException {
        return authRequestExecutor.execute(() -> {
            return api.fullFilterViewByPollId(requestPollId); 
        });
    }
}