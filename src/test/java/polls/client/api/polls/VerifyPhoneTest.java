package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.VerifyCodeViewModel;
import polls.client.model.StatusResponse;
import polls.client.api.utils.WordGenerator;
import java.util.UUID;
import java.util.List;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class VerifyPhoneTest extends PollsApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws ApiException {
        // given
        VerifyCodeViewModel verifyCodeViewModel = new VerifyCodeViewModel();

        verifyCodeViewModel.setVerificationCode(WordGenerator.generateWord());
        verifyCodeViewModel.setUserName(WordGenerator.generateName(10));

        // when
        StatusResponse response = makeRequest(verifyCodeViewModel);

        // then
        assertNotNull(response);
        assertNotNull(response.getStatus());
        assertNotNull(response.getStatusMessage());
    }

    private StatusResponse makeRequest(VerifyCodeViewModel verifyCodeViewModel) throws ApiException {
        return authRequestExecutor.execute(() -> {
            return api.verifyPhone(verifyCodeViewModel); 
        });
    }
}