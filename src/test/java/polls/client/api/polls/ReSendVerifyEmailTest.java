package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.EmailViewModel;
import polls.client.model.StatusResponse;
import polls.client.api.utils.EmailGenerator;
import java.util.UUID;
import java.util.List;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class ReSendVerifyEmailTest extends PollsApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws ApiException {
        // given
        EmailViewModel emailViewModel = new EmailViewModel();

        emailViewModel.setEmailAddress(EmailGenerator.generateEmail());

        // when
        StatusResponse response = makeRequest(emailViewModel);

        // then
        assertNotNull(response);
        assertNotNull(response.getStatus());
        assertNotNull(response.getStatusMessage());
    }

    private StatusResponse makeRequest(EmailViewModel emailViewModel) throws ApiException {
        return authRequestExecutor.execute(() -> {
            return api.reSendVerifyEmail(emailViewModel); 
        });
    }
}