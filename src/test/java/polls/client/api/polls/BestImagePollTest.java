package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.BestImage;
import polls.client.model.StatusResponse;
import polls.client.api.utils.WordGenerator;
import polls.client.api.utils.Random;
import java.util.UUID;
import java.util.List;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class BestImagePollTest extends PollsApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws ApiException {
        // given
        BestImage bestImage = new BestImage();

        bestImage.setFirstImagePath(WordGenerator.generateWord());
        bestImage.setSecondImagePath(WordGenerator.generateWord());
        bestImage.setPollTitle(WordGenerator.generateWord());
        bestImage.setQuestion(WordGenerator.generateWord());
        bestImage.setPollOverview(WordGenerator.generateWord());
        bestImage.setAssignmentDurationInSeconds(Random.nextLong(1L, 10L));
        bestImage.setKeywords(WordGenerator.generateWord());
        bestImage.setTags(WordGenerator.generateWord());
        bestImage.setLifeTimeInSeconds(Random.nextLong(1L, 10L));

        // when
        StatusResponse response = makeRequest(bestImage);

        // then
        assertNotNull(response);
        assertNotNull(response.getStatus());
        assertNotNull(response.getStatusMessage());
    }

    private StatusResponse makeRequest(BestImage bestImage) throws ApiException {
        return authRequestExecutor.execute(() -> {
            return api.bestImagePoll(bestImage); 
        });
    }
}