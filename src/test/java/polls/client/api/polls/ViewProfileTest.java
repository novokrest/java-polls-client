package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.User;
import java.util.UUID;
import java.util.List;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class ViewProfileTest extends PollsApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws ApiException {
        // given

        // when
        User response = makeRequest();

        // then
        assertNotNull(response);
        assertNotNull(response.getEmailAddress());
        assertNotNull(response.getUserId());
        assertNotNull(response.getIsActive());
        assertNotNull(response.getUserRole());
        assertNotNull(response.getIsCanRunFreePoll());
        assertNotNull(response.getIsBuyCredits());
        assertNotNull(response.getIsPartner());
        assertNotNull(response.getDepartmentId());
        assertNotNull(response.getLastLogin());
        assertNotNull(response.getCreateDate());
        assertNotNull(response.getAccessLevel());
        assertNotNull(response.getId());
        assertNotNull(response.getUserName());
        assertNotNull(response.getProfileId());
        assertNotNull(response.getStatus());
        assertNotNull(response.getIsEmailVerified());
        assertNotNull(response.getToken());
        assertNotNull(response.getIsPhoneVerified());
        assertNotNull(response.getIsCallVerified());
        assertNotNull(response.getSource());
        assertNotNull(response.getDisplayName());
        assertNotNull(response.getUserType());
        assertNotNull(response.getBuyCompanyPolls());
        assertNotNull(response.getBuyTurkPolls());
        assertNotNull(response.getIsRequiedApproval());
        assertNotNull(response.getOrg());
        assertNotNull(response.getEmailInvites());
        assertNotNull(response.getNewProfileContact());
        assertNotNull(response.getNewDevice());
    }

    private User makeRequest() throws ApiException {
        return authRequestExecutor.execute(() -> {
            return api.viewProfile(); 
        });
    }
}