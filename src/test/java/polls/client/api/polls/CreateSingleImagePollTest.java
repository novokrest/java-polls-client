package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.SingleImage;
import polls.client.model.StatusResponse;
import polls.client.api.utils.WordGenerator;
import polls.client.api.utils.Random;
import java.util.UUID;
import java.util.List;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class CreateSingleImagePollTest extends PollsApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws ApiException {
        // given
        SingleImage singleImage = new SingleImage();

        singleImage.setFirstImagePath(WordGenerator.generateWord());
        singleImage.setPollTitle(WordGenerator.generateWord());
        singleImage.setQuestion(WordGenerator.generateWord());
        singleImage.setPollOverview(WordGenerator.generateWord());
        singleImage.setAssignmentDurationInSeconds(Random.nextLong(1L, 10L));
        singleImage.setKeywords(WordGenerator.generateWord());
        singleImage.setTags(WordGenerator.generateWord());
        singleImage.setLifeTimeInSeconds(Random.nextLong(1L, 10L));

        // when
        StatusResponse response = makeRequest(singleImage);

        // then
        assertNotNull(response);
        assertNotNull(response.getStatus());
        assertNotNull(response.getStatusMessage());
    }

    private StatusResponse makeRequest(SingleImage singleImage) throws ApiException {
        return authRequestExecutor.execute(() -> {
            return api.createSingleImagePoll(singleImage); 
        });
    }
}