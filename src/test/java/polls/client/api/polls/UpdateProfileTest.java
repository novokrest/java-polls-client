package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.ProfileViewModel;
import polls.client.model.User;
import polls.client.api.utils.WordGenerator;
import polls.client.api.utils.Random;
import polls.client.api.utils.PhoneGenerator;
import polls.client.api.utils.GenderGenerator;
import polls.client.api.utils.DateTimeGenerator;
import polls.client.api.utils.EmailGenerator;
import java.util.UUID;
import java.util.List;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class UpdateProfileTest extends PollsApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws ApiException {
        // given
        ProfileViewModel profileViewModel = new ProfileViewModel();

        profileViewModel.setCompanyName(WordGenerator.generateName(10));
        profileViewModel.setJobTitle(WordGenerator.generateWord());
        profileViewModel.setCountryCode(Random.nextLong(1L, 10L));
        profileViewModel.setPhoneNumber(PhoneGenerator.generatePhone());
        profileViewModel.setGender(GenderGenerator.generateGender());
        profileViewModel.setBirthDate(DateTimeGenerator.currentDateTime());
        profileViewModel.setEducationLevel(WordGenerator.generateWord());
        profileViewModel.setAddressLine1(WordGenerator.generateWord());
        profileViewModel.setAddressLine2(WordGenerator.generateWord());
        profileViewModel.setAddressCity(WordGenerator.generateWord());
        profileViewModel.setAddressCountry(WordGenerator.generateWord());
        profileViewModel.setAddressState(WordGenerator.generateWord());
        profileViewModel.setAddressZip(Random.nextLong(1L, 10L));
        profileViewModel.setFirstName(WordGenerator.generateName(10));
        profileViewModel.setLastName(WordGenerator.generateName(10));
        profileViewModel.setPayPalEmail(EmailGenerator.generateEmail());

        // when
        User response = makeRequest(profileViewModel);

        // then
        assertNotNull(response);
        assertNotNull(response.getEmailAddress());
        assertNotNull(response.getUserId());
        assertNotNull(response.getIsActive());
        assertNotNull(response.getUserRole());
        assertNotNull(response.getIsCanRunFreePoll());
        assertNotNull(response.getIsBuyCredits());
        assertNotNull(response.getIsPartner());
        assertNotNull(response.getDepartmentId());
        assertNotNull(response.getLastLogin());
        assertNotNull(response.getCreateDate());
        assertNotNull(response.getAccessLevel());
        assertNotNull(response.getId());
        assertNotNull(response.getUserName());
        assertNotNull(response.getProfileId());
        assertNotNull(response.getStatus());
        assertNotNull(response.getIsEmailVerified());
        assertNotNull(response.getToken());
        assertNotNull(response.getIsPhoneVerified());
        assertNotNull(response.getIsCallVerified());
        assertNotNull(response.getSource());
        assertNotNull(response.getDisplayName());
        assertNotNull(response.getUserType());
        assertNotNull(response.getBuyCompanyPolls());
        assertNotNull(response.getBuyTurkPolls());
        assertNotNull(response.getIsRequiedApproval());
        assertNotNull(response.getOrg());
        assertNotNull(response.getEmailInvites());
        assertNotNull(response.getNewProfileContact());
        assertNotNull(response.getNewDevice());
    }

    private User makeRequest(ProfileViewModel profileViewModel) throws ApiException {
        return authRequestExecutor.execute(() -> {
            return api.updateProfile(profileViewModel); 
        });
    }
}