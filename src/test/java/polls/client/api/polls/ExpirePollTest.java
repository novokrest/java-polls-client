package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.RequestPollId;
import polls.client.model.PollResponse;
import polls.client.api.utils.Random;
import java.util.UUID;
import java.util.List;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class ExpirePollTest extends PollsApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws ApiException {
        // given
        RequestPollId requestPollId = new RequestPollId();

        requestPollId.setPollId(Random.nextLong(1L, 10L));

        // when
        PollResponse response = makeRequest(requestPollId);

        // then
        assertNotNull(response);
        assertNotNull(response.getStatus());
    }

    private PollResponse makeRequest(RequestPollId requestPollId) throws ApiException {
        return authRequestExecutor.execute(() -> {
            return api.expirePoll(requestPollId); 
        });
    }
}