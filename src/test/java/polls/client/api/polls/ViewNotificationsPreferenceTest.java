package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.NotificationViewModel;
import java.util.UUID;
import java.util.List;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class ViewNotificationsPreferenceTest extends PollsApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws ApiException {
        // given

        // when
        NotificationViewModel response = makeRequest();

        // then
        assertNotNull(response);
        assertNotNull(response.getDeviceId());
        assertNotNull(response.getEmail());
        assertNotNull(response.getBrowser());
        assertNotNull(response.getSMS());
        assertNotNull(response.getPhoneCall());
        assertNotNull(response.getPushNotification());
    }

    private NotificationViewModel makeRequest() throws ApiException {
        return authRequestExecutor.execute(() -> {
            return api.viewNotificationsPreference(); 
        });
    }
}