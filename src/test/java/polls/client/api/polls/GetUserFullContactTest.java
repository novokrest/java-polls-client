package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.FullContactModel;
import polls.client.model.FullContact;
import java.util.UUID;
import java.util.List;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class GetUserFullContactTest extends PollsApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws ApiException {
        // given
        FullContactModel fullContactModel = new FullContactModel();


        // when
        FullContact response = makeRequest(fullContactModel);

        // then
        assertNotNull(response);
        assertNotNull(response.getStatus());
        assertNotNull(response.getRequestId());
        assertNotNull(response.getLikelihood());
        assertNotNull(response.getPhotos());
        assertNotNull(response.getContactInfo());
        assertNotNull(response.getOrganizations());
        assertNotNull(response.getDemographics());
        assertNotNull(response.getSocialProfiles());
        assertNotNull(response.getDigitalFootprint());
    }

    private FullContact makeRequest(FullContactModel fullContactModel) throws ApiException {
        return authRequestExecutor.execute(() -> {
            return api.getUserFullContact(fullContactModel); 
        });
    }
}