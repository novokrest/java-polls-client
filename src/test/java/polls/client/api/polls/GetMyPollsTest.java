package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.PollRequest;
import polls.client.model.ListPollModel;
import java.util.UUID;
import java.util.List;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class GetMyPollsTest extends PollsApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws ApiException {
        // given
        PollRequest pollRequest = new PollRequest();


        // when
        List<ListPollModel> response = makeRequest(pollRequest);

        // then
        assertNotNull(response);
    }

    private List<ListPollModel> makeRequest(PollRequest pollRequest) throws ApiException {
        return authRequestExecutor.execute(() -> {
            return api.getMyPolls(pollRequest); 
        });
    }
}