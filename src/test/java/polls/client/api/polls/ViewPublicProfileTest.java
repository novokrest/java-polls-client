package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.UserIdModel;
import polls.client.model.Contact;
import java.util.UUID;
import java.util.List;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class ViewPublicProfileTest extends PollsApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws ApiException {
        // given
        UserIdModel userIdModel = new UserIdModel();


        // when
        Contact response = makeRequest(userIdModel);

        // then
        assertNotNull(response);
        assertNotNull(response.getUserId());
        assertNotNull(response.getCountryCode());
        assertNotNull(response.getPhoneNumber());
        assertNotNull(response.getGender());
        assertNotNull(response.getBirthDate());
        assertNotNull(response.getAddressLine1());
        assertNotNull(response.getAddressLine2());
        assertNotNull(response.getAddressCity());
        assertNotNull(response.getAddressCountry());
        assertNotNull(response.getAddressState());
        assertNotNull(response.getAddressZip());
        assertNotNull(response.getOwnerId());
        assertNotNull(response.getPictureUrl());
        assertNotNull(response.getProfilePictures());
        assertNotNull(response.getIsEmailVerified());
        assertNotNull(response.getIsPhoneVerified());
        assertNotNull(response.getPublicPolls());
        assertNotNull(response.getBusinessPolls());
        assertNotNull(response.getPrivatePolls());
        assertNotNull(response.getTotalPolls());
        assertNotNull(response.getDeletedPictures());
        assertNotNull(response.getLastLogin());
        assertNotNull(response.getMemberSince());
        assertNotNull(response.getFirstName());
        assertNotNull(response.getLastName());
        assertNotNull(response.getPayPalEmail());
        assertNotNull(response.getPhoneVerifiedDateTime());
        assertNotNull(response.getPhoneVerificationMethod());
        assertNotNull(response.getEmailVerifiedDateTime());
        assertNotNull(response.getId());
        assertNotNull(response.getCompanyName());
        assertNotNull(response.getJobTitle());
        assertNotNull(response.getEducationLevel());
        assertNotNull(response.getRelationshipStatus());
        assertNotNull(response.getMaritalStatus());
        assertNotNull(response.getPictureId());
        assertNotNull(response.getIsPublicProfile());
        assertNotNull(response.getIsPhonePublic());
        assertNotNull(response.getIsEmailPublic());
        assertNotNull(response.getViewCounter());
        assertNotNull(response.getPhoneType());
    }

    private Contact makeRequest(UserIdModel userIdModel) throws ApiException {
        return authRequestExecutor.execute(() -> {
            return api.viewPublicProfile(userIdModel); 
        });
    }
}