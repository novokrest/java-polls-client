package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.PayRequestData;
import polls.client.model.PayResponse;
import java.util.UUID;
import java.util.List;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class VerifyPaymentTest extends PollsApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws ApiException {
        // given
        PayRequestData payRequestData = new PayRequestData();


        // when
        PayResponse response = makeRequest(payRequestData);

        // then
        assertNotNull(response);
        assertNotNull(response.getStatus());
        assertNotNull(response.getCreditBalance());
        assertNotNull(response.getPollsBalance());
    }

    private PayResponse makeRequest(PayRequestData payRequestData) throws ApiException {
        return authRequestExecutor.execute(() -> {
            return api.verifyPayment(payRequestData); 
        });
    }
}