package polls.client.api.pay;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.StripeViewModel;
import polls.client.model.StatusResponse;
import polls.client.api.utils.WordGenerator;
import polls.client.api.utils.EmailGenerator;
import polls.client.api.utils.UuidGenerator;
import polls.client.api.utils.Random;
import java.util.UUID;
import java.util.List;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class ChargePaymentTest extends PayApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws ApiException {
        // given
        StripeViewModel stripeViewModel = new StripeViewModel();

        stripeViewModel.setStripeToken(WordGenerator.generateWord());
        stripeViewModel.setStripeEmail(EmailGenerator.generateEmail());
        stripeViewModel.setPlanId(UuidGenerator.generateUUID());
        stripeViewModel.setUserId(UuidGenerator.generateUUID());
        stripeViewModel.setChargeAmount(Random.nextLong(1L, 10L));

        // when
        StatusResponse response = makeRequest(stripeViewModel);

        // then
        assertNotNull(response);
        assertNotNull(response.getStatus());
        assertNotNull(response.getStatusMessage());
    }

    private StatusResponse makeRequest(StripeViewModel stripeViewModel) throws ApiException {
        return authRequestExecutor.execute(() -> {
            return api.chargePayment(stripeViewModel); 
        });
    }
}