package polls.client.api.pay;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.AuthTokenViewModel;
import polls.client.model.AccessTokenViewModel;
import polls.client.api.utils.WordGenerator;
import java.util.UUID;
import java.util.List;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class GetAccessTokenTest extends PayApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws ApiException {
        // given
        AuthTokenViewModel authTokenViewModel = new AuthTokenViewModel();

        authTokenViewModel.setPlatform("Windows");
        authTokenViewModel.setEnvironment(WordGenerator.generateWord());
        authTokenViewModel.setCode(WordGenerator.generateWord());

        // when
        AccessTokenViewModel response = makeRequest(authTokenViewModel);

        // then
        assertNotNull(response);
        assertNotNull(response.getScope());
        assertNotNull(response.getNonce());
        assertNotNull(response.getObligations());
        assertNotNull(response.getAccessToken());
        assertNotNull(response.getTokenType());
        assertNotNull(response.getExpiresIn());
        assertNotNull(response.getRefreshToken());
    }

    private AccessTokenViewModel makeRequest(AuthTokenViewModel authTokenViewModel) throws ApiException {
        return authRequestExecutor.execute(() -> {
            return api.getAccessToken(authTokenViewModel); 
        });
    }
}