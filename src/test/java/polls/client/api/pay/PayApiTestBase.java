package polls.client.api.pay;

import polls.client.api.PayApi;
import polls.client.Configuration;

import org.junit.Before;
import org.junit.Ignore;

@Ignore
public class PayApiTestBase {

    private static final int TIMEOUT = 0;

    protected final PayApi api = new PayApi();

    @Before
    public void before() {
        Configuration.getDefaultApiClient().setConnectTimeout(TIMEOUT);
    }
}