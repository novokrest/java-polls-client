package polls.client.api.pay;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.InvoiceRequest;
import polls.client.model.BillingInvoiceModel;
import polls.client.api.utils.Random;
import polls.client.api.utils.UuidGenerator;
import java.util.UUID;
import java.util.List;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class BillingInvoiceTest extends PayApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws ApiException {
        // given
        InvoiceRequest invoiceRequest = new InvoiceRequest();

        invoiceRequest.setId(Random.nextLong(1L, 10L));
        invoiceRequest.setUserId(UuidGenerator.generateUUID());

        // when
        BillingInvoiceModel response = makeRequest(invoiceRequest);

        // then
        assertNotNull(response);
        assertNotNull(response.getId());
        assertNotNull(response.getTransactionId());
        assertNotNull(response.getPaymentDate());
        assertNotNull(response.getGrossAmount());
        assertNotNull(response.getCredits());
        assertNotNull(response.getPaymentMethod());
        assertNotNull(response.getPayerEmail());
        assertNotNull(response.getReceiverEmail());
        assertNotNull(response.getPaymentStatus());
        assertNotNull(response.getCustomerFirstName());
        assertNotNull(response.getCustomerLastName());
        assertNotNull(response.getBillingCompanyName());
        assertNotNull(response.getBillingAddressLine1());
        assertNotNull(response.getBillingAddressLine2());
        assertNotNull(response.getBillingState());
        assertNotNull(response.getZipCode());
        assertNotNull(response.getProduct());
        assertNotNull(response.getIsPremium());
        assertNotNull(response.getBillingPhone());
        assertNotNull(response.getCustomerCompanyName());
        assertNotNull(response.getCountryCode());
        assertNotNull(response.getCustomerPhoneNumber());
        assertNotNull(response.getCustomerPostalAddress());
        assertNotNull(response.getOrgLogoUrl());
    }

    private BillingInvoiceModel makeRequest(InvoiceRequest invoiceRequest) throws ApiException {
        return authRequestExecutor.execute(() -> {
            return api.billingInvoice(invoiceRequest); 
        });
    }
}