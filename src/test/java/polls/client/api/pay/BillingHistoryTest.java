package polls.client.api.pay;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.RequestId;
import polls.client.model.BillingHistoryModel;
import polls.client.api.utils.UuidGenerator;
import java.util.UUID;
import java.util.List;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class BillingHistoryTest extends PayApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws ApiException {
        // given
        RequestId requestId = new RequestId();

        requestId.setId(UuidGenerator.generateUUID());

        // when
        List<BillingHistoryModel> response = makeRequest(requestId);

        // then
        assertNotNull(response);
    }

    private List<BillingHistoryModel> makeRequest(RequestId requestId) throws ApiException {
        return authRequestExecutor.execute(() -> {
            return api.billingHistory(requestId); 
        });
    }
}