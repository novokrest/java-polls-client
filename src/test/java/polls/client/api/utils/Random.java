package polls.client.api.utils;

import java.util.concurrent.ThreadLocalRandom;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Random {

    public static long nextLong(long from, long to) {
        return ThreadLocalRandom.current().nextLong(from, to);
    }

    public static List<Long> nextLongs(long from, long to) {
        return Collections.unmodifiableList(Arrays.asList(nextLong(from, to)));
    }

    public static int nextInt(int from, int to) {
        return ThreadLocalRandom.current().nextInt(from, to);
    }

    public static List<Integer> nextInts(int from, int to) {
        return Collections.unmodifiableList(Arrays.asList(nextInt(from, to)));
    }

    public static double nextDouble(double from, double to) {
        return ThreadLocalRandom.current().nextDouble(from, to);
    }

    public static List<Double> nextDoubles(double from, double to) {
        return Collections.unmodifiableList(Arrays.asList(nextDouble(from, to)));
    }
}
