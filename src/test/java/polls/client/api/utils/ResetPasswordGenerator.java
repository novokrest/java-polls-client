package polls.client.api.utils;

import polls.client.model.ResetPassword;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ResetPasswordGenerator {

    public static ResetPassword generateResetPassword() {
        ResetPassword resetPassword = new ResetPassword();

        resetPassword.setRequestId(WordGenerator.generateWord());
        resetPassword.setNewPassword(WordGenerator.generatePassword());
        resetPassword.setConfirmPassword(WordGenerator.generatePassword());

        return resetPassword;
    }

    public static List<ResetPassword> generateResetPasswordCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateResetPassword()));
    }
}
