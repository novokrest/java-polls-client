package polls.client.api.utils;

import polls.client.model.ExternalProviderRequest;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ExternalProviderRequestGenerator {

    public static ExternalProviderRequest generateExternalProviderRequest() {
        ExternalProviderRequest externalProviderRequest = new ExternalProviderRequest();

        externalProviderRequest.setProvider(0);
        externalProviderRequest.setId(WordGenerator.generateWord());
        externalProviderRequest.setVerifiedEmail(false);
        externalProviderRequest.setGivenName(WordGenerator.generateName(10));
        externalProviderRequest.setFamilyName(WordGenerator.generateName(10));
        externalProviderRequest.setAccessToken(WordGenerator.generateWord());

        return externalProviderRequest;
    }

    public static List<ExternalProviderRequest> generateExternalProviderRequestCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateExternalProviderRequest()));
    }
}
