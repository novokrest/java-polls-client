package polls.client.api.utils;

import polls.client.model.DigitalFootprint;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class DigitalFootprintGenerator {

    public static DigitalFootprint generateDigitalFootprint() {
        DigitalFootprint digitalFootprint = new DigitalFootprint();


        return digitalFootprint;
    }

    public static List<DigitalFootprint> generateDigitalFootprintCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateDigitalFootprint()));
    }
}
