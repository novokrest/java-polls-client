package polls.client.api.utils;

import polls.client.model.UserRelation;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class UserRelationGenerator {

    public static UserRelation generateUserRelation() {
        UserRelation userRelation = new UserRelation();


        return userRelation;
    }

    public static List<UserRelation> generateUserRelationCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateUserRelation()));
    }
}
