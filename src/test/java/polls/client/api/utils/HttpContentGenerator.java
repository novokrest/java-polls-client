package polls.client.api.utils;

import polls.client.model.HttpContent;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class HttpContentGenerator {

    public static HttpContent generateHttpContent() {
        HttpContent httpContent = new HttpContent();


        return httpContent;
    }

    public static List<HttpContent> generateHttpContentCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateHttpContent()));
    }
}
