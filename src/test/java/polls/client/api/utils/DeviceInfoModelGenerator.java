package polls.client.api.utils;

import polls.client.model.DeviceInfoModel;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class DeviceInfoModelGenerator {

    public static DeviceInfoModel generateDeviceInfoModel() {
        DeviceInfoModel deviceInfoModel = new DeviceInfoModel();

        deviceInfoModel.setSnsDeviceId(WordGenerator.generateWord());

        return deviceInfoModel;
    }

    public static List<DeviceInfoModel> generateDeviceInfoModelCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateDeviceInfoModel()));
    }
}
