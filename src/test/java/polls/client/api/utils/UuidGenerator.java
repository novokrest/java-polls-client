package polls.client.api.utils;

import java.util.UUID;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class UuidGenerator {

    public static UUID generateUUID() {
        return UUID.randomUUID();
    }

    public static List<UUID> generateUUIDs() {
        return Collections.unmodifiableList(Arrays.asList(generateUUID()));
    }
}
