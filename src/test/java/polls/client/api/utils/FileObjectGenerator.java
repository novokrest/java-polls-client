package polls.client.api.utils;

import polls.client.model.FileObject;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class FileObjectGenerator {

    public static FileObject generateFileObject() {
        FileObject fileObject = new FileObject();


        return fileObject;
    }

    public static List<FileObject> generateFileObjectCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateFileObject()));
    }
}
