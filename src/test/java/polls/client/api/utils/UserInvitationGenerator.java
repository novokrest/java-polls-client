package polls.client.api.utils;

import polls.client.model.UserInvitation;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class UserInvitationGenerator {

    public static UserInvitation generateUserInvitation() {
        UserInvitation userInvitation = new UserInvitation();


        return userInvitation;
    }

    public static List<UserInvitation> generateUserInvitationCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateUserInvitation()));
    }
}
