package polls.client.api.utils;

import polls.client.model.AccessTokenViewModel;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class AccessTokenViewModelGenerator {

    public static AccessTokenViewModel generateAccessTokenViewModel() {
        AccessTokenViewModel accessTokenViewModel = new AccessTokenViewModel();


        return accessTokenViewModel;
    }

    public static List<AccessTokenViewModel> generateAccessTokenViewModelCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateAccessTokenViewModel()));
    }
}
