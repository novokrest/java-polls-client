package polls.client.api.utils;

import polls.client.model.CompanyUsers;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class CompanyUsersGenerator {

    public static CompanyUsers generateCompanyUsers() {
        CompanyUsers companyUsers = new CompanyUsers();

        companyUsers.setUserName(WordGenerator.generateName(10));
        companyUsers.setEmailAddress(EmailGenerator.generateEmail());
        companyUsers.setPassword(WordGenerator.generatePassword());
        companyUsers.setNewDevice(DeviceModelGenerator.generateDeviceModel());
        companyUsers.setNewProfileContact(ContactGenerator.generateContact());

        return companyUsers;
    }

    public static List<CompanyUsers> generateCompanyUsersCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateCompanyUsers()));
    }
}
