package polls.client.api.utils;

import polls.client.model.Obligation;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ObligationGenerator {

    public static Obligation generateObligation() {
        Obligation obligation = new Obligation();


        return obligation;
    }

    public static List<Obligation> generateObligationCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateObligation()));
    }
}
