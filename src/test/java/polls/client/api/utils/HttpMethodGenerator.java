package polls.client.api.utils;

import polls.client.model.HttpMethod;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class HttpMethodGenerator {

    public static HttpMethod generateHttpMethod() {
        HttpMethod httpMethod = new HttpMethod();


        return httpMethod;
    }

    public static List<HttpMethod> generateHttpMethodCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateHttpMethod()));
    }
}
