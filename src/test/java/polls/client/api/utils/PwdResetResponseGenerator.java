package polls.client.api.utils;

import polls.client.model.PwdResetResponse;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PwdResetResponseGenerator {

    public static PwdResetResponse generatePwdResetResponse() {
        PwdResetResponse pwdResetResponse = new PwdResetResponse();


        return pwdResetResponse;
    }

    public static List<PwdResetResponse> generatePwdResetResponseCollection() {
        return Collections.unmodifiableList(Arrays.asList(generatePwdResetResponse()));
    }
}
