package polls.client.api.utils;

import polls.client.model.FilterData;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class FilterDataGenerator {

    public static FilterData generateFilterData() {
        FilterData filterData = new FilterData();


        return filterData;
    }

    public static List<FilterData> generateFilterDataCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateFilterData()));
    }
}
