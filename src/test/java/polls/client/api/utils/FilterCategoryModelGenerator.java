package polls.client.api.utils;

import polls.client.model.FilterCategoryModel;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class FilterCategoryModelGenerator {

    public static FilterCategoryModel generateFilterCategoryModel() {
        FilterCategoryModel filterCategoryModel = new FilterCategoryModel();


        return filterCategoryModel;
    }

    public static List<FilterCategoryModel> generateFilterCategoryModelCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateFilterCategoryModel()));
    }
}
