package polls.client.api.utils;

import polls.client.model.InvoiceRequest;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class InvoiceRequestGenerator {

    public static InvoiceRequest generateInvoiceRequest() {
        InvoiceRequest invoiceRequest = new InvoiceRequest();

        invoiceRequest.setId(Random.nextLong(1L, 10L));
        invoiceRequest.setUserId(UuidGenerator.generateUUID());

        return invoiceRequest;
    }

    public static List<InvoiceRequest> generateInvoiceRequestCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateInvoiceRequest()));
    }
}
