package polls.client.api.utils;

import polls.client.model.RequestNameModel;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class RequestNameModelGenerator {

    public static RequestNameModel generateRequestNameModel() {
        RequestNameModel requestNameModel = new RequestNameModel();


        return requestNameModel;
    }

    public static List<RequestNameModel> generateRequestNameModelCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateRequestNameModel()));
    }
}
