package polls.client.api.utils;

import org.joda.time.DateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class DateTimeGenerator {

    public static DateTime currentDateTime() {
        return DateTime.now();
    }

    public static List<DateTime> generateDateTimes() {
        return Collections.unmodifiableList(Arrays.asList(currentDateTime()));
    }
}
