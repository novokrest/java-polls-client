package polls.client.api.utils;

import polls.client.model.ListPollModel;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ListPollModelGenerator {

    public static ListPollModel generateListPollModel() {
        ListPollModel listPollModel = new ListPollModel();


        return listPollModel;
    }

    public static List<ListPollModel> generateListPollModelCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateListPollModel()));
    }
}
