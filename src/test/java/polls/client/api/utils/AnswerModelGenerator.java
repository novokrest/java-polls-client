package polls.client.api.utils;

import polls.client.model.AnswerModel;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class AnswerModelGenerator {

    public static AnswerModel generateAnswerModel() {
        AnswerModel answerModel = new AnswerModel();

        answerModel.setPollId(Random.nextLong(1L, 10L));
        answerModel.setAnswerChoice(1);
        answerModel.setWorkerUserId(UuidGenerator.generateUUID());

        return answerModel;
    }

    public static List<AnswerModel> generateAnswerModelCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateAnswerModel()));
    }
}
