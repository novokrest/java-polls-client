package polls.client.api.utils;

import polls.client.model.AuthTokenViewModel;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class AuthTokenViewModelGenerator {

    public static AuthTokenViewModel generateAuthTokenViewModel() {
        AuthTokenViewModel authTokenViewModel = new AuthTokenViewModel();

        authTokenViewModel.setPlatform("Windows");
        authTokenViewModel.setEnvironment(WordGenerator.generateWord());
        authTokenViewModel.setCode(WordGenerator.generateWord());

        return authTokenViewModel;
    }

    public static List<AuthTokenViewModel> generateAuthTokenViewModelCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateAuthTokenViewModel()));
    }
}
