package polls.client.api.utils;

import polls.client.model.BillingInvoiceModel;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class BillingInvoiceModelGenerator {

    public static BillingInvoiceModel generateBillingInvoiceModel() {
        BillingInvoiceModel billingInvoiceModel = new BillingInvoiceModel();


        return billingInvoiceModel;
    }

    public static List<BillingInvoiceModel> generateBillingInvoiceModelCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateBillingInvoiceModel()));
    }
}
