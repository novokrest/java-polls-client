package polls.client.api.utils;

import polls.client.model.FilterDataModel;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class FilterDataModelGenerator {

    public static FilterDataModel generateFilterDataModel() {
        FilterDataModel filterDataModel = new FilterDataModel();


        return filterDataModel;
    }

    public static List<FilterDataModel> generateFilterDataModelCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateFilterDataModel()));
    }
}
