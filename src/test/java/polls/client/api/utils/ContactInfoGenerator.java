package polls.client.api.utils;

import polls.client.model.ContactInfo;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ContactInfoGenerator {

    public static ContactInfo generateContactInfo() {
        ContactInfo contactInfo = new ContactInfo();


        return contactInfo;
    }

    public static List<ContactInfo> generateContactInfoCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateContactInfo()));
    }
}
