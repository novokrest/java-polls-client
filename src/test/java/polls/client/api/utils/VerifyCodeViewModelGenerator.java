package polls.client.api.utils;

import polls.client.model.VerifyCodeViewModel;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class VerifyCodeViewModelGenerator {

    public static VerifyCodeViewModel generateVerifyCodeViewModel() {
        VerifyCodeViewModel verifyCodeViewModel = new VerifyCodeViewModel();

        verifyCodeViewModel.setVerificationCode(WordGenerator.generateWord());
        verifyCodeViewModel.setUserName(WordGenerator.generateName(10));

        return verifyCodeViewModel;
    }

    public static List<VerifyCodeViewModel> generateVerifyCodeViewModelCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateVerifyCodeViewModel()));
    }
}
