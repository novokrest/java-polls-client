package polls.client.api.utils;

import polls.client.model.FileRequest;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class FileRequestGenerator {

    public static FileRequest generateFileRequest() {
        FileRequest fileRequest = new FileRequest();


        return fileRequest;
    }

    public static List<FileRequest> generateFileRequestCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateFileRequest()));
    }
}
