package polls.client.api.utils;

import polls.client.model.UpdatePollRequest;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class UpdatePollRequestGenerator {

    public static UpdatePollRequest generateUpdatePollRequest() {
        UpdatePollRequest updatePollRequest = new UpdatePollRequest();

        updatePollRequest.setPollId(Random.nextLong(1L, 10L));
        updatePollRequest.setKeywords(WordGenerator.generateWord());
        updatePollRequest.setPollOverview(WordGenerator.generateWord());

        return updatePollRequest;
    }

    public static List<UpdatePollRequest> generateUpdatePollRequestCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateUpdatePollRequest()));
    }
}
