package polls.client.api.utils;

import polls.client.model.FullContactModel;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class FullContactModelGenerator {

    public static FullContactModel generateFullContactModel() {
        FullContactModel fullContactModel = new FullContactModel();


        return fullContactModel;
    }

    public static List<FullContactModel> generateFullContactModelCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateFullContactModel()));
    }
}
