package polls.client.api.utils;

import polls.client.model.Country;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class CountryGenerator {

    public static Country generateCountry() {
        Country country = new Country();


        return country;
    }

    public static List<Country> generateCountryCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateCountry()));
    }
}
