package polls.client.api.utils;

import polls.client.model.ProfileViewModel;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ProfileViewModelGenerator {

    public static ProfileViewModel generateProfileViewModel() {
        ProfileViewModel profileViewModel = new ProfileViewModel();

        profileViewModel.setCompanyName(WordGenerator.generateName(10));
        profileViewModel.setJobTitle(WordGenerator.generateWord());
        profileViewModel.setCountryCode(Random.nextLong(1L, 10L));
        profileViewModel.setPhoneNumber(PhoneGenerator.generatePhone());
        profileViewModel.setGender(GenderGenerator.generateGender());
        profileViewModel.setBirthDate(DateTimeGenerator.currentDateTime());
        profileViewModel.setEducationLevel(WordGenerator.generateWord());
        profileViewModel.setAddressLine1(WordGenerator.generateWord());
        profileViewModel.setAddressLine2(WordGenerator.generateWord());
        profileViewModel.setAddressCity(WordGenerator.generateWord());
        profileViewModel.setAddressCountry(WordGenerator.generateWord());
        profileViewModel.setAddressState(WordGenerator.generateWord());
        profileViewModel.setAddressZip(Random.nextLong(1L, 10L));
        profileViewModel.setFirstName(WordGenerator.generateName(10));
        profileViewModel.setLastName(WordGenerator.generateName(10));
        profileViewModel.setPayPalEmail(EmailGenerator.generateEmail());

        return profileViewModel;
    }

    public static List<ProfileViewModel> generateProfileViewModelCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateProfileViewModel()));
    }
}
