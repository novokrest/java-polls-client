package polls.client.api.utils;

import polls.client.model.CheckinLocationViewModel;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class CheckinLocationViewModelGenerator {

    public static CheckinLocationViewModel generateCheckinLocationViewModel() {
        CheckinLocationViewModel checkinLocationViewModel = new CheckinLocationViewModel();

        checkinLocationViewModel.setLatitude(Random.nextDouble(1.00D, 100.00D));
        checkinLocationViewModel.setLongitude(Random.nextDouble(1.00D, 100.00D));
        checkinLocationViewModel.setUserId(UuidGenerator.generateUUID());

        return checkinLocationViewModel;
    }

    public static List<CheckinLocationViewModel> generateCheckinLocationViewModelCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateCheckinLocationViewModel()));
    }
}
