package polls.client.api.utils;

import polls.client.model.ResetPasswordRequest;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ResetPasswordRequestGenerator {

    public static ResetPasswordRequest generateResetPasswordRequest() {
        ResetPasswordRequest resetPasswordRequest = new ResetPasswordRequest();

        resetPasswordRequest.setEmailAddress(EmailGenerator.generateEmail());

        return resetPasswordRequest;
    }

    public static List<ResetPasswordRequest> generateResetPasswordRequestCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateResetPasswordRequest()));
    }
}
