package polls.client.api.utils;

import polls.client.model.SuggestQuestionViewModel;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class SuggestQuestionViewModelGenerator {

    public static SuggestQuestionViewModel generateSuggestQuestionViewModel() {
        SuggestQuestionViewModel suggestQuestionViewModel = new SuggestQuestionViewModel();


        return suggestQuestionViewModel;
    }

    public static List<SuggestQuestionViewModel> generateSuggestQuestionViewModelCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateSuggestQuestionViewModel()));
    }
}
