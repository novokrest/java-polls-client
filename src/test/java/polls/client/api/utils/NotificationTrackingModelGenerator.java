package polls.client.api.utils;

import polls.client.model.NotificationTrackingModel;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class NotificationTrackingModelGenerator {

    public static NotificationTrackingModel generateNotificationTrackingModel() {
        NotificationTrackingModel notificationTrackingModel = new NotificationTrackingModel();


        return notificationTrackingModel;
    }

    public static List<NotificationTrackingModel> generateNotificationTrackingModelCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateNotificationTrackingModel()));
    }
}
