package polls.client.api.utils;

import polls.client.model.Device;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class DeviceGenerator {

    public static Device generateDevice() {
        Device device = new Device();

        device.setSnsDeviceId(WordGenerator.generateWord());

        return device;
    }

    public static List<Device> generateDeviceCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateDevice()));
    }
}
