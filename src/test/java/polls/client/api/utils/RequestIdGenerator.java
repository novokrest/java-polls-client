package polls.client.api.utils;

import polls.client.model.RequestId;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class RequestIdGenerator {

    public static RequestId generateRequestId() {
        RequestId requestId = new RequestId();

        requestId.setId(UuidGenerator.generateUUID());

        return requestId;
    }

    public static List<RequestId> generateRequestIdCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateRequestId()));
    }
}
