package polls.client.api.utils;

import polls.client.model.VerifyViewModel;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class VerifyViewModelGenerator {

    public static VerifyViewModel generateVerifyViewModel() {
        VerifyViewModel verifyViewModel = new VerifyViewModel();

        verifyViewModel.setUserName(WordGenerator.generateName(10));

        return verifyViewModel;
    }

    public static List<VerifyViewModel> generateVerifyViewModelCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateVerifyViewModel()));
    }
}
