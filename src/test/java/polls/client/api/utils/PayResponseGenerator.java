package polls.client.api.utils;

import polls.client.model.PayResponse;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PayResponseGenerator {

    public static PayResponse generatePayResponse() {
        PayResponse payResponse = new PayResponse();


        return payResponse;
    }

    public static List<PayResponse> generatePayResponseCollection() {
        return Collections.unmodifiableList(Arrays.asList(generatePayResponse()));
    }
}
