package polls.client.api.utils;

import polls.client.model.UserCreditModel;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class UserCreditModelGenerator {

    public static UserCreditModel generateUserCreditModel() {
        UserCreditModel userCreditModel = new UserCreditModel();


        return userCreditModel;
    }

    public static List<UserCreditModel> generateUserCreditModelCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateUserCreditModel()));
    }
}
