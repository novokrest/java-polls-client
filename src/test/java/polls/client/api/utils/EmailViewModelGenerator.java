package polls.client.api.utils;

import polls.client.model.EmailViewModel;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class EmailViewModelGenerator {

    public static EmailViewModel generateEmailViewModel() {
        EmailViewModel emailViewModel = new EmailViewModel();

        emailViewModel.setEmailAddress(EmailGenerator.generateEmail());

        return emailViewModel;
    }

    public static List<EmailViewModel> generateEmailViewModelCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateEmailViewModel()));
    }
}
