package polls.client.api.utils;

import polls.client.model.HttpRequestMessage;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class HttpRequestMessageGenerator {

    public static HttpRequestMessage generateHttpRequestMessage() {
        HttpRequestMessage httpRequestMessage = new HttpRequestMessage();


        return httpRequestMessage;
    }

    public static List<HttpRequestMessage> generateHttpRequestMessageCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateHttpRequestMessage()));
    }
}
