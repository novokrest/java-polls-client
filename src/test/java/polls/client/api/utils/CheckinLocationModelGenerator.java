package polls.client.api.utils;

import polls.client.model.CheckinLocationModel;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class CheckinLocationModelGenerator {

    public static CheckinLocationModel generateCheckinLocationModel() {
        CheckinLocationModel checkinLocationModel = new CheckinLocationModel();


        return checkinLocationModel;
    }

    public static List<CheckinLocationModel> generateCheckinLocationModelCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateCheckinLocationModel()));
    }
}
