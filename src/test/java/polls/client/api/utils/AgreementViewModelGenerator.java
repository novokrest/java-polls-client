package polls.client.api.utils;

import polls.client.model.AgreementViewModel;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class AgreementViewModelGenerator {

    public static AgreementViewModel generateAgreementViewModel() {
        AgreementViewModel agreementViewModel = new AgreementViewModel();

        agreementViewModel.setPlanId(UuidGenerator.generateUUID());
        agreementViewModel.setAgreementId(WordGenerator.generateWord());
        agreementViewModel.setPayerEmail(EmailGenerator.generateEmail());
        agreementViewModel.setFirstName(WordGenerator.generateName(10));
        agreementViewModel.setLastName(WordGenerator.generateName(10));
        agreementViewModel.setNextBillingDay(Random.nextLong(1L, 10L));
        agreementViewModel.setPayerId(WordGenerator.generateWord());
        agreementViewModel.setPaymentGross(Random.nextDouble(1.00D, 100.00D));
        agreementViewModel.setPaymentMethod(WordGenerator.generateWord());

        return agreementViewModel;
    }

    public static List<AgreementViewModel> generateAgreementViewModelCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateAgreementViewModel()));
    }
}
