package polls.client.api.utils;

import polls.client.model.StatesModel;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class StatesModelGenerator {

    public static StatesModel generateStatesModel() {
        StatesModel statesModel = new StatesModel();


        return statesModel;
    }

    public static List<StatesModel> generateStatesModelCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateStatesModel()));
    }
}
