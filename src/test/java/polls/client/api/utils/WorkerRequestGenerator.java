package polls.client.api.utils;

import polls.client.model.WorkerRequest;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class WorkerRequestGenerator {

    public static WorkerRequest generateWorkerRequest() {
        WorkerRequest workerRequest = new WorkerRequest();

        workerRequest.setWorkerId(WordGenerator.generateWord());

        return workerRequest;
    }

    public static List<WorkerRequest> generateWorkerRequestCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateWorkerRequest()));
    }
}
