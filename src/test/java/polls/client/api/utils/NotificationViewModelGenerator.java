package polls.client.api.utils;

import polls.client.model.NotificationViewModel;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class NotificationViewModelGenerator {

    public static NotificationViewModel generateNotificationViewModel() {
        NotificationViewModel notificationViewModel = new NotificationViewModel();

        notificationViewModel.setDeviceId(UuidGenerator.generateUUID());

        return notificationViewModel;
    }

    public static List<NotificationViewModel> generateNotificationViewModelCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateNotificationViewModel()));
    }
}
