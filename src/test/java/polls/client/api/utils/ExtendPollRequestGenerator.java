package polls.client.api.utils;

import polls.client.model.ExtendPollRequest;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ExtendPollRequestGenerator {

    public static ExtendPollRequest generateExtendPollRequest() {
        ExtendPollRequest extendPollRequest = new ExtendPollRequest();

        extendPollRequest.setPollId(Random.nextLong(1L, 10L));
        extendPollRequest.setExpirationIncreamentInSeconds(Random.nextLong(1L, 10L));

        return extendPollRequest;
    }

    public static List<ExtendPollRequest> generateExtendPollRequestCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateExtendPollRequest()));
    }
}
