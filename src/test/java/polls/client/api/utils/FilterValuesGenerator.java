package polls.client.api.utils;

import polls.client.model.FilterValues;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class FilterValuesGenerator {

    public static FilterValues generateFilterValues() {
        FilterValues filterValues = new FilterValues();


        return filterValues;
    }

    public static List<FilterValues> generateFilterValuesCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateFilterValues()));
    }
}
