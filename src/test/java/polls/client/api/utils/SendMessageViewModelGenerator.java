package polls.client.api.utils;

import polls.client.model.SendMessageViewModel;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class SendMessageViewModelGenerator {

    public static SendMessageViewModel generateSendMessageViewModel() {
        SendMessageViewModel sendMessageViewModel = new SendMessageViewModel();

        sendMessageViewModel.setChannelId(WordGenerator.generateWord());
        sendMessageViewModel.setSnsType(WordGenerator.generateWord());
        sendMessageViewModel.setMessageToSend(WordGenerator.generateWord());

        return sendMessageViewModel;
    }

    public static List<SendMessageViewModel> generateSendMessageViewModelCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateSendMessageViewModel()));
    }
}
