package polls.client.api.utils;

import polls.client.model.BestImage;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class BestImageGenerator {

    public static BestImage generateBestImage() {
        BestImage bestImage = new BestImage();

        bestImage.setFirstImagePath(WordGenerator.generateWord());
        bestImage.setSecondImagePath(WordGenerator.generateWord());
        bestImage.setPollTitle(WordGenerator.generateWord());
        bestImage.setQuestion(WordGenerator.generateWord());
        bestImage.setPollOverview(WordGenerator.generateWord());
        bestImage.setAssignmentDurationInSeconds(Random.nextLong(1L, 10L));
        bestImage.setKeywords(WordGenerator.generateWord());
        bestImage.setTags(WordGenerator.generateWord());
        bestImage.setLifeTimeInSeconds(Random.nextLong(1L, 10L));

        return bestImage;
    }

    public static List<BestImage> generateBestImageCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateBestImage()));
    }
}
