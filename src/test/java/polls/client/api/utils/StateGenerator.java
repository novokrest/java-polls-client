package polls.client.api.utils;

import polls.client.model.State;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class StateGenerator {

    public static State generateState() {
        State state = new State();


        return state;
    }

    public static List<State> generateStateCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateState()));
    }
}
