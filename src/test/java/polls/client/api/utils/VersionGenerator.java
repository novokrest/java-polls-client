package polls.client.api.utils;

import polls.client.model.Version;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class VersionGenerator {

    public static Version generateVersion() {
        Version version = new Version();


        return version;
    }

    public static List<Version> generateVersionCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateVersion()));
    }
}
