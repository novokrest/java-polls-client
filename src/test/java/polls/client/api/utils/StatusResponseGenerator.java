package polls.client.api.utils;

import polls.client.model.StatusResponse;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class StatusResponseGenerator {

    public static StatusResponse generateStatusResponse() {
        StatusResponse statusResponse = new StatusResponse();


        return statusResponse;
    }

    public static List<StatusResponse> generateStatusResponseCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateStatusResponse()));
    }
}
