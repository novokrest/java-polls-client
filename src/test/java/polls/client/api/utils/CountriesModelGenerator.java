package polls.client.api.utils;

import polls.client.model.CountriesModel;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class CountriesModelGenerator {

    public static CountriesModel generateCountriesModel() {
        CountriesModel countriesModel = new CountriesModel();


        return countriesModel;
    }

    public static List<CountriesModel> generateCountriesModelCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateCountriesModel()));
    }
}
