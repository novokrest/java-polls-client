package polls.client.api.utils;

import polls.client.model.LocationsList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class LocationsListGenerator {

    public static LocationsList generateLocationsList() {
        LocationsList locationsList = new LocationsList();


        return locationsList;
    }

    public static List<LocationsList> generateLocationsListCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateLocationsList()));
    }
}
