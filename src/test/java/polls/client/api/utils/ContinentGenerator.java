package polls.client.api.utils;

import polls.client.model.Continent;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ContinentGenerator {

    public static Continent generateContinent() {
        Continent continent = new Continent();


        return continent;
    }

    public static List<Continent> generateContinentCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateContinent()));
    }
}
