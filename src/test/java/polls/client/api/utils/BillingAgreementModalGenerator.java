package polls.client.api.utils;

import polls.client.model.BillingAgreementModal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class BillingAgreementModalGenerator {

    public static BillingAgreementModal generateBillingAgreementModal() {
        BillingAgreementModal billingAgreementModal = new BillingAgreementModal();


        return billingAgreementModal;
    }

    public static List<BillingAgreementModal> generateBillingAgreementModalCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateBillingAgreementModal()));
    }
}
