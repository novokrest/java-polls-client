package polls.client.api.utils;

import polls.client.model.RegisterViewModel;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class RegisterViewModelGenerator {

    public static RegisterViewModel generateRegisterViewModel() {
        RegisterViewModel registerViewModel = new RegisterViewModel();

        registerViewModel.setUserName(WordGenerator.generateName(10));
        registerViewModel.setEmailAddress(EmailGenerator.generateEmail());
        registerViewModel.setPassword(WordGenerator.generatePassword());
        registerViewModel.setNewDevice(DeviceModelGenerator.generateDeviceModel());
        registerViewModel.setNewProfileContact(ContactGenerator.generateContact());

        return registerViewModel;
    }

    public static List<RegisterViewModel> generateRegisterViewModelCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateRegisterViewModel()));
    }
}
