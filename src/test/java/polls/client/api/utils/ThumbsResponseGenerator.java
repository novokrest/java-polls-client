package polls.client.api.utils;

import polls.client.model.ThumbsResponse;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ThumbsResponseGenerator {

    public static ThumbsResponse generateThumbsResponse() {
        ThumbsResponse thumbsResponse = new ThumbsResponse();


        return thumbsResponse;
    }

    public static List<ThumbsResponse> generateThumbsResponseCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateThumbsResponse()));
    }
}
