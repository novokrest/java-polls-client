package polls.client.api.utils;

import polls.client.model.Contact;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ContactGenerator {

    public static Contact generateContact() {
        Contact contact = new Contact();

        contact.setCountryCode(Random.nextLong(1L, 10L));
        contact.setPhoneNumber(PhoneGenerator.generatePhone());
        contact.setGender(GenderGenerator.generateGender());
        contact.setBirthDate(WordGenerator.generateWord());
        contact.setAddressLine1(WordGenerator.generateWord());
        contact.setAddressLine2(WordGenerator.generateWord());
        contact.setAddressCity(WordGenerator.generateWord());
        contact.setAddressCountry(WordGenerator.generateWord());
        contact.setAddressState(WordGenerator.generateWord());
        contact.setAddressZip(WordGenerator.generateWord());
        contact.setFirstName(WordGenerator.generateName(10));
        contact.setLastName(WordGenerator.generateName(10));
        contact.setPayPalEmail(EmailGenerator.generateEmail());

        return contact;
    }

    public static List<Contact> generateContactCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateContact()));
    }
}
