package polls.client.api.utils;

import polls.client.model.LoginResponse;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class LoginResponseGenerator {

    public static LoginResponse generateLoginResponse() {
        LoginResponse loginResponse = new LoginResponse();


        return loginResponse;
    }

    public static List<LoginResponse> generateLoginResponseCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateLoginResponse()));
    }
}
