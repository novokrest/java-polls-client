package polls.client.api.utils;

import polls.client.model.ApiLoginModel;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ApiLoginModelGenerator {

    public static ApiLoginModel generateApiLoginModel() {
        ApiLoginModel apiLoginModel = new ApiLoginModel();

        apiLoginModel.setUserName("samer");
        apiLoginModel.setPassword("123456");
        apiLoginModel.setNewDevice(DeviceModelGenerator.generateDeviceModel());

        return apiLoginModel;
    }

    public static List<ApiLoginModel> generateApiLoginModelCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateApiLoginModel()));
    }
}
