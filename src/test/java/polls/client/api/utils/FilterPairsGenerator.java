package polls.client.api.utils;

import polls.client.model.FilterPairs;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class FilterPairsGenerator {

    public static FilterPairs generateFilterPairs() {
        FilterPairs filterPairs = new FilterPairs();


        return filterPairs;
    }

    public static List<FilterPairs> generateFilterPairsCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateFilterPairs()));
    }
}
