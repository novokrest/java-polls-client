package polls.client.api.utils;

import polls.client.model.FilterResp;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class FilterRespGenerator {

    public static FilterResp generateFilterResp() {
        FilterResp filterResp = new FilterResp();


        return filterResp;
    }

    public static List<FilterResp> generateFilterRespCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateFilterResp()));
    }
}
