package polls.client.api.utils;

import polls.client.model.ChangePasswordModel;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ChangePasswordModelGenerator {

    public static ChangePasswordModel generateChangePasswordModel() {
        ChangePasswordModel changePasswordModel = new ChangePasswordModel();

        changePasswordModel.setOldPassword(WordGenerator.generatePassword());
        changePasswordModel.setNewPassword(WordGenerator.generatePassword());

        return changePasswordModel;
    }

    public static List<ChangePasswordModel> generateChangePasswordModelCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateChangePasswordModel()));
    }
}
