package polls.client.api.utils;

import polls.client.model.FilterDataCount;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class FilterDataCountGenerator {

    public static FilterDataCount generateFilterDataCount() {
        FilterDataCount filterDataCount = new FilterDataCount();


        return filterDataCount;
    }

    public static List<FilterDataCount> generateFilterDataCountCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateFilterDataCount()));
    }
}
