package polls.client.api.utils;

import polls.client.model.RegisterDeviceSNS;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class RegisterDeviceSNSGenerator {

    public static RegisterDeviceSNS generateRegisterDeviceSNS() {
        RegisterDeviceSNS registerDeviceSNS = new RegisterDeviceSNS();


        return registerDeviceSNS;
    }

    public static List<RegisterDeviceSNS> generateRegisterDeviceSNSCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateRegisterDeviceSNS()));
    }
}
