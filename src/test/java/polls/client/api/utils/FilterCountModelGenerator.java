package polls.client.api.utils;

import polls.client.model.FilterCountModel;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class FilterCountModelGenerator {

    public static FilterCountModel generateFilterCountModel() {
        FilterCountModel filterCountModel = new FilterCountModel();


        return filterCountModel;
    }

    public static List<FilterCountModel> generateFilterCountModelCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateFilterCountModel()));
    }
}
