package polls.client.api.utils;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class EmailGenerator {

    private static final List<String> SERVER_NAMES = Collections.unmodifiableList(Arrays.asList(
            "gmail", "hotmail", "mail"
    ));

    private static final List<String> DOMAIN_NAMES = Collections.unmodifiableList(Arrays.asList(
            "com", "org", "net"
    ));

    public static String generateEmail() {
        String userName = WordGenerator.generateName(Random.nextInt(8, 20));
        String serverName = SERVER_NAMES.get(Random.nextInt(0, SERVER_NAMES.size()));
        String domainName = DOMAIN_NAMES.get(Random.nextInt(0, DOMAIN_NAMES.size()));
        return String.format("%s@%s.%s", userName, serverName, domainName);
    }
}
