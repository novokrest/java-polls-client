package polls.client.api.utils;

import polls.client.model.PaymentPlanResponse;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PaymentPlanResponseGenerator {

    public static PaymentPlanResponse generatePaymentPlanResponse() {
        PaymentPlanResponse paymentPlanResponse = new PaymentPlanResponse();


        return paymentPlanResponse;
    }

    public static List<PaymentPlanResponse> generatePaymentPlanResponseCollection() {
        return Collections.unmodifiableList(Arrays.asList(generatePaymentPlanResponse()));
    }
}
