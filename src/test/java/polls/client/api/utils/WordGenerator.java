package polls.client.api.utils;

import java.util.stream.IntStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class WordGenerator {

    private static final String LEXICON = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final String DIGITS = "12345674890";

    public static String generateName() {
        int length = randomInt(4, 50);
        return generateName(length);
    }

    public static String generateName(int length) {
        return randomWord(length);
    }

    public static String generatePassword() {
        int length = randomInt(6, 20);
        return randomWord(length);
    }

    public static String generatePhoneNumber() {
        return String.format("(%s)-%s-%s", randomNumber(3), randomNumber(3), randomNumber(4));
    }

    public static String generateWord() {
        return randomWord(10);
    }

    public static List<String> generateWords() {
        return Collections.unmodifiableList(Arrays.asList(generateWord()));
    }

    private static String randomWord(int length) {
        StringBuilder builder = new StringBuilder(length);
        IntStream.range(0, length).forEach(i -> builder.append(randomChar()));
        return builder.toString();
    }

    private static String randomNumber(int length) {
        StringBuilder builder = new StringBuilder(length);
        IntStream.range(0, length).forEach(i -> builder.append(randomDigit()));
        return builder.toString();
    }

    private static char randomChar() {
        int index = randomInt(0, LEXICON.length());
        return LEXICON.charAt(index);
    }

    private static char randomDigit() {
        int index = randomInt(0, DIGITS.length());
        return DIGITS.charAt(index);
    }

    private static int randomInt(int from, int to) {
        return Random.nextInt(from, to);
    }
}
