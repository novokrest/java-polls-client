package polls.client.api.utils;

import polls.client.model.RemoteImageViewModel;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class RemoteImageViewModelGenerator {

    public static RemoteImageViewModel generateRemoteImageViewModel() {
        RemoteImageViewModel remoteImageViewModel = new RemoteImageViewModel();

        remoteImageViewModel.setRemoteImageUrl(WordGenerator.generateWord());
        remoteImageViewModel.setAccessKey(WordGenerator.generateWord());

        return remoteImageViewModel;
    }

    public static List<RemoteImageViewModel> generateRemoteImageViewModelCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateRemoteImageViewModel()));
    }
}
