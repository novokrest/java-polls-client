package polls.client.api.utils;

import polls.client.model.RegisterResponse;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class RegisterResponseGenerator {

    public static RegisterResponse generateRegisterResponse() {
        RegisterResponse registerResponse = new RegisterResponse();

        registerResponse.setUserId(UuidGenerator.generateUUID());
        registerResponse.setToken(UuidGenerator.generateUUID());

        return registerResponse;
    }

    public static List<RegisterResponse> generateRegisterResponseCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateRegisterResponse()));
    }
}
