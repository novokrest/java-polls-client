package polls.client.api.utils;

import polls.client.model.DemographicStats;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class DemographicStatsGenerator {

    public static DemographicStats generateDemographicStats() {
        DemographicStats demographicStats = new DemographicStats();


        return demographicStats;
    }

    public static List<DemographicStats> generateDemographicStatsCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateDemographicStats()));
    }
}
