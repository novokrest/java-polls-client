package polls.client.api.utils;

import polls.client.model.LogoutResponse;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class LogoutResponseGenerator {

    public static LogoutResponse generateLogoutResponse() {
        LogoutResponse logoutResponse = new LogoutResponse();


        return logoutResponse;
    }

    public static List<LogoutResponse> generateLogoutResponseCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateLogoutResponse()));
    }
}
