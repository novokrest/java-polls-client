package polls.client.api.utils;

import polls.client.model.EducationModel;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class EducationModelGenerator {

    public static EducationModel generateEducationModel() {
        EducationModel educationModel = new EducationModel();


        return educationModel;
    }

    public static List<EducationModel> generateEducationModelCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateEducationModel()));
    }
}
