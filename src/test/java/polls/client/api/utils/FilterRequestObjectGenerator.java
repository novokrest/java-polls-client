package polls.client.api.utils;

import polls.client.model.FilterRequestObject;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class FilterRequestObjectGenerator {

    public static FilterRequestObject generateFilterRequestObject() {
        FilterRequestObject filterRequestObject = new FilterRequestObject();


        return filterRequestObject;
    }

    public static List<FilterRequestObject> generateFilterRequestObjectCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateFilterRequestObject()));
    }
}
