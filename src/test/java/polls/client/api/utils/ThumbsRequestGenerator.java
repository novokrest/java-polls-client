package polls.client.api.utils;

import polls.client.model.ThumbsRequest;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ThumbsRequestGenerator {

    public static ThumbsRequest generateThumbsRequest() {
        ThumbsRequest thumbsRequest = new ThumbsRequest();


        return thumbsRequest;
    }

    public static List<ThumbsRequest> generateThumbsRequestCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateThumbsRequest()));
    }
}
