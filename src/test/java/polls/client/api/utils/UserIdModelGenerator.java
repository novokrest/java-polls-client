package polls.client.api.utils;

import polls.client.model.UserIdModel;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class UserIdModelGenerator {

    public static UserIdModel generateUserIdModel() {
        UserIdModel userIdModel = new UserIdModel();


        return userIdModel;
    }

    public static List<UserIdModel> generateUserIdModelCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateUserIdModel()));
    }
}
