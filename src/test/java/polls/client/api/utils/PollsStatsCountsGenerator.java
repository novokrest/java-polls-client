package polls.client.api.utils;

import polls.client.model.PollsStatsCounts;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PollsStatsCountsGenerator {

    public static PollsStatsCounts generatePollsStatsCounts() {
        PollsStatsCounts pollsStatsCounts = new PollsStatsCounts();


        return pollsStatsCounts;
    }

    public static List<PollsStatsCounts> generatePollsStatsCountsCollection() {
        return Collections.unmodifiableList(Arrays.asList(generatePollsStatsCounts()));
    }
}
