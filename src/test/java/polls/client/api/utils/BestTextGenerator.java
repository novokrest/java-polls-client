package polls.client.api.utils;

import polls.client.model.BestText;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class BestTextGenerator {

    public static BestText generateBestText() {
        BestText bestText = new BestText();

        bestText.setFirstOption(WordGenerator.generateWord());
        bestText.setSecondOption(WordGenerator.generateWord());
        bestText.setPollTitle(WordGenerator.generateWord());
        bestText.setQuestion(WordGenerator.generateWord());
        bestText.setPollOverview(WordGenerator.generateWord());
        bestText.setAssignmentDurationInSeconds(Random.nextLong(1L, 10L));
        bestText.setKeywords(WordGenerator.generateWord());
        bestText.setTags(WordGenerator.generateWord());
        bestText.setLifeTimeInSeconds(Random.nextLong(1L, 10L));

        return bestText;
    }

    public static List<BestText> generateBestTextCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateBestText()));
    }
}
