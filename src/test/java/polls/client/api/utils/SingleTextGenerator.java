package polls.client.api.utils;

import polls.client.model.SingleText;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class SingleTextGenerator {

    public static SingleText generateSingleText() {
        SingleText singleText = new SingleText();

        singleText.setPollTitle(WordGenerator.generateWord());
        singleText.setQuestion(WordGenerator.generateWord());
        singleText.setPollOverview(WordGenerator.generateWord());
        singleText.setAssignmentDurationInSeconds(Random.nextLong(1L, 10L));
        singleText.setKeywords(WordGenerator.generateWord());
        singleText.setTags(WordGenerator.generateWord());
        singleText.setLifeTimeInSeconds(Random.nextLong(1L, 10L));

        return singleText;
    }

    public static List<SingleText> generateSingleTextCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateSingleText()));
    }
}
