package polls.client.api.utils;

import polls.client.model.RequestPollId;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class RequestPollIdGenerator {

    public static RequestPollId generateRequestPollId() {
        RequestPollId requestPollId = new RequestPollId();

        requestPollId.setPollId(Random.nextLong(1L, 10L));

        return requestPollId;
    }

    public static List<RequestPollId> generateRequestPollIdCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateRequestPollId()));
    }
}
