package polls.client.api.utils;

import polls.client.model.Demographics;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class DemographicsGenerator {

    public static Demographics generateDemographics() {
        Demographics demographics = new Demographics();


        return demographics;
    }

    public static List<Demographics> generateDemographicsCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateDemographics()));
    }
}
