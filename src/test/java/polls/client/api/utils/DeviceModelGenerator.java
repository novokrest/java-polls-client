package polls.client.api.utils;

import polls.client.model.DeviceModel;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class DeviceModelGenerator {

    public static DeviceModel generateDeviceModel() {
        DeviceModel deviceModel = new DeviceModel();

        deviceModel.setDeviceName(WordGenerator.generateName(10));
        deviceModel.setDevicePlatform("Windows");
        deviceModel.setSnsDeviceId(WordGenerator.generateWord());

        return deviceModel;
    }

    public static List<DeviceModel> generateDeviceModelCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateDeviceModel()));
    }
}
