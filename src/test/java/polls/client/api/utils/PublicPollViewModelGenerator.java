package polls.client.api.utils;

import polls.client.model.PublicPollViewModel;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PublicPollViewModelGenerator {

    public static PublicPollViewModel generatePublicPollViewModel() {
        PublicPollViewModel publicPollViewModel = new PublicPollViewModel();


        return publicPollViewModel;
    }

    public static List<PublicPollViewModel> generatePublicPollViewModelCollection() {
        return Collections.unmodifiableList(Arrays.asList(generatePublicPollViewModel()));
    }
}
