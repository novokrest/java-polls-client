package polls.client.api.utils;

import polls.client.model.WorkerApiModel;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class WorkerApiModelGenerator {

    public static WorkerApiModel generateWorkerApiModel() {
        WorkerApiModel workerApiModel = new WorkerApiModel();


        return workerApiModel;
    }

    public static List<WorkerApiModel> generateWorkerApiModelCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateWorkerApiModel()));
    }
}
