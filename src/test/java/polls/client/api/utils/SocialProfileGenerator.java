package polls.client.api.utils;

import polls.client.model.SocialProfile;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class SocialProfileGenerator {

    public static SocialProfile generateSocialProfile() {
        SocialProfile socialProfile = new SocialProfile();


        return socialProfile;
    }

    public static List<SocialProfile> generateSocialProfileCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateSocialProfile()));
    }
}
