package polls.client.api.utils;

import polls.client.model.CitiesModel;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class CitiesModelGenerator {

    public static CitiesModel generateCitiesModel() {
        CitiesModel citiesModel = new CitiesModel();


        return citiesModel;
    }

    public static List<CitiesModel> generateCitiesModelCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateCitiesModel()));
    }
}
