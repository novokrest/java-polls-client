package polls.client.api.utils;

import polls.client.model.HttpResponseMessage;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class HttpResponseMessageGenerator {

    public static HttpResponseMessage generateHttpResponseMessage() {
        HttpResponseMessage httpResponseMessage = new HttpResponseMessage();


        return httpResponseMessage;
    }

    public static List<HttpResponseMessage> generateHttpResponseMessageCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateHttpResponseMessage()));
    }
}
