package polls.client.api.utils;

import polls.client.model.PollResult;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PollResultGenerator {

    public static PollResult generatePollResult() {
        PollResult pollResult = new PollResult();


        return pollResult;
    }

    public static List<PollResult> generatePollResultCollection() {
        return Collections.unmodifiableList(Arrays.asList(generatePollResult()));
    }
}
