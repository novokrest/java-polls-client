package polls.client.api.utils;

import polls.client.model.PollRequest;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PollRequestGenerator {

    public static PollRequest generatePollRequest() {
        PollRequest pollRequest = new PollRequest();


        return pollRequest;
    }

    public static List<PollRequest> generatePollRequestCollection() {
        return Collections.unmodifiableList(Arrays.asList(generatePollRequest()));
    }
}
