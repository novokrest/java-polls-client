package polls.client.api.utils;

import polls.client.model.StripeViewModel;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class StripeViewModelGenerator {

    public static StripeViewModel generateStripeViewModel() {
        StripeViewModel stripeViewModel = new StripeViewModel();

        stripeViewModel.setStripeToken(WordGenerator.generateWord());
        stripeViewModel.setStripeEmail(EmailGenerator.generateEmail());
        stripeViewModel.setPlanId(UuidGenerator.generateUUID());
        stripeViewModel.setUserId(UuidGenerator.generateUUID());
        stripeViewModel.setChargeAmount(Random.nextLong(1L, 10L));

        return stripeViewModel;
    }

    public static List<StripeViewModel> generateStripeViewModelCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateStripeViewModel()));
    }
}
