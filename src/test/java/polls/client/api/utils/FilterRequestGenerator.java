package polls.client.api.utils;

import polls.client.model.FilterRequest;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class FilterRequestGenerator {

    public static FilterRequest generateFilterRequest() {
        FilterRequest filterRequest = new FilterRequest();


        return filterRequest;
    }

    public static List<FilterRequest> generateFilterRequestCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateFilterRequest()));
    }
}
