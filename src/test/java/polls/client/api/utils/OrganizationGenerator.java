package polls.client.api.utils;

import polls.client.model.Organization;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class OrganizationGenerator {

    public static Organization generateOrganization() {
        Organization organization = new Organization();


        return organization;
    }

    public static List<Organization> generateOrganizationCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateOrganization()));
    }
}
