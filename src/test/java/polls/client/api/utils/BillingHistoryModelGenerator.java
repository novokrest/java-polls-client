package polls.client.api.utils;

import polls.client.model.BillingHistoryModel;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class BillingHistoryModelGenerator {

    public static BillingHistoryModel generateBillingHistoryModel() {
        BillingHistoryModel billingHistoryModel = new BillingHistoryModel();


        return billingHistoryModel;
    }

    public static List<BillingHistoryModel> generateBillingHistoryModelCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateBillingHistoryModel()));
    }
}
