package polls.client.api.utils;

import polls.client.model.LocationDeduced;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class LocationDeducedGenerator {

    public static LocationDeduced generateLocationDeduced() {
        LocationDeduced locationDeduced = new LocationDeduced();


        return locationDeduced;
    }

    public static List<LocationDeduced> generateLocationDeducedCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateLocationDeduced()));
    }
}
