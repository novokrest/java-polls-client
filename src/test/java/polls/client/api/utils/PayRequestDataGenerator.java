package polls.client.api.utils;

import polls.client.model.PayRequestData;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PayRequestDataGenerator {

    public static PayRequestData generatePayRequestData() {
        PayRequestData payRequestData = new PayRequestData();


        return payRequestData;
    }

    public static List<PayRequestData> generatePayRequestDataCollection() {
        return Collections.unmodifiableList(Arrays.asList(generatePayRequestData()));
    }
}
