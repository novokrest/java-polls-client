package polls.client.api.utils;

import polls.client.model.PollResponse;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PollResponseGenerator {

    public static PollResponse generatePollResponse() {
        PollResponse pollResponse = new PollResponse();


        return pollResponse;
    }

    public static List<PollResponse> generatePollResponseCollection() {
        return Collections.unmodifiableList(Arrays.asList(generatePollResponse()));
    }
}
