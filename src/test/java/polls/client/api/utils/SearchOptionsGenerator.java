package polls.client.api.utils;

import polls.client.model.SearchOptions;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class SearchOptionsGenerator {

    public static SearchOptions generateSearchOptions() {
        SearchOptions searchOptions = new SearchOptions();


        return searchOptions;
    }

    public static List<SearchOptions> generateSearchOptionsCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateSearchOptions()));
    }
}
