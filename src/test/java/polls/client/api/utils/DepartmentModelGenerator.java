package polls.client.api.utils;

import polls.client.model.DepartmentModel;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class DepartmentModelGenerator {

    public static DepartmentModel generateDepartmentModel() {
        DepartmentModel departmentModel = new DepartmentModel();


        return departmentModel;
    }

    public static List<DepartmentModel> generateDepartmentModelCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateDepartmentModel()));
    }
}
