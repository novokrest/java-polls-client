package polls.client.api.utils;

import polls.client.model.SingleImage;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class SingleImageGenerator {

    public static SingleImage generateSingleImage() {
        SingleImage singleImage = new SingleImage();

        singleImage.setFirstImagePath(WordGenerator.generateWord());
        singleImage.setPollTitle(WordGenerator.generateWord());
        singleImage.setQuestion(WordGenerator.generateWord());
        singleImage.setPollOverview(WordGenerator.generateWord());
        singleImage.setAssignmentDurationInSeconds(Random.nextLong(1L, 10L));
        singleImage.setKeywords(WordGenerator.generateWord());
        singleImage.setTags(WordGenerator.generateWord());
        singleImage.setLifeTimeInSeconds(Random.nextLong(1L, 10L));

        return singleImage;
    }

    public static List<SingleImage> generateSingleImageCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateSingleImage()));
    }
}
