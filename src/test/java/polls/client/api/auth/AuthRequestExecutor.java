package polls.client.api.auth;

import polls.client.ApiException;
import polls.client.api.ApiClientEx;
import polls.client.api.PollsApi;
import polls.client.api.utils.DeviceModelGenerator;
import polls.client.model.ApiLoginModel;
import polls.client.model.LoginResponse;


public class AuthRequestExecutor {

    private static final String VALID_USER_NAME = "samer";
    private static final String VALID_USER_PASSWORD = "123456";

    private final PollsApi api;

    public static AuthRequestExecutor create() {
        return new AuthRequestExecutor(new PollsApi());
    }

    private AuthRequestExecutor(PollsApi api) {
        this.api = api;
    }

    @FunctionalInterface
    public interface ApiMethodInvoker<ResponseT> {
        ResponseT makeRequest() throws ApiException;
    }

    public <ResponseT> ResponseT execute(ApiMethodInvoker<ResponseT> invoker) throws ApiException {
        setAuthorization();
        return invoker.makeRequest();
    }

    private void setAuthorization() throws ApiException {
        Authorization authorization = obtainAuthorization();
        ApiClientEx.setAuth(api.getApiClient(), authorization);
    }

    private Authorization obtainAuthorization() throws ApiException {
        ApiLoginModel loginModel = new ApiLoginModel();
        loginModel.setUserName(VALID_USER_NAME);
        loginModel.setPassword(VALID_USER_PASSWORD);
        loginModel.setNewDevice(DeviceModelGenerator.generateDeviceModel());

        LoginResponse response = api.login(loginModel);

        return Authorization.of(response.getUserId(), response.getToken());
    }
}
