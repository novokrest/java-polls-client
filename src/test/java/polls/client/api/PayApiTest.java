/*
 * Polls Web API
 * Poll Service Developers Web API
 *
 * OpenAPI spec version: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package polls.client.api;

import polls.client.ApiException;
import polls.client.model.AccessTokenViewModel;
import polls.client.model.AuthTokenViewModel;
import polls.client.model.BillingHistoryModel;
import polls.client.model.BillingInvoiceModel;
import polls.client.model.ErrorResponse;
import polls.client.model.InvoiceRequest;
import polls.client.model.ListBillingHistory;
import polls.client.model.PayResponse;
import polls.client.model.RequestId;
import polls.client.model.StatusResponse;
import polls.client.model.StripeViewModel;
import polls.client.model.VerifyPaymentViewModel;
import org.junit.Test;
import org.junit.Ignore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for PayApi
 */
@Ignore
public class PayApiTest {

    private final PayApi api = new PayApi();

    
    /**
     * Billings the history.
     *
     * Billings the history.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void billingHistoryTest() throws ApiException {
        RequestId requestId = null;
        ListBillingHistory response = api.billingHistory(requestId);

        // TODO: test validations
    }
    
    /**
     * Billings the invoice.
     *
     * Billings the invoice.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void billingInvoiceTest() throws ApiException {
        InvoiceRequest invoiceRequest = null;
        BillingInvoiceModel response = api.billingInvoice(invoiceRequest);

        // TODO: test validations
    }
    
    /**
     * Buying Credits history.
     *
     * Buying Credits history.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void buyingHistoryTest() throws ApiException {
        RequestId requestId = null;
        List<BillingHistoryModel> response = api.buyingHistory(requestId);

        // TODO: test validations
    }
    
    /**
     * Charges the payment.
     *
     * Charges the payment.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void chargePaymentTest() throws ApiException {
        StripeViewModel stripeViewModel = null;
        StatusResponse response = api.chargePayment(stripeViewModel);

        // TODO: test validations
    }
    
    /**
     * Verifies the authentication code.
     *
     * Verifies the authentication code.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getAccessTokenTest() throws ApiException {
        AuthTokenViewModel authTokenViewModel = null;
        AccessTokenViewModel response = api.getAccessToken(authTokenViewModel);

        // TODO: test validations
    }
    
    /**
     * Verifies the Payment.
     *
     * Verifies the Payment.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void verifyPaymentTest() throws ApiException {
        VerifyPaymentViewModel verifyPaymentViewModel = null;
        PayResponse response = api.verifyPayment(verifyPaymentViewModel);

        // TODO: test validations
    }
    
    /**
     * Gets the windows collection token.
     *
     * Gets the windows collection token.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void windowsCollectionTokenTest() throws ApiException {
        String response = api.windowsCollectionToken();

        // TODO: test validations
    }
    
}
