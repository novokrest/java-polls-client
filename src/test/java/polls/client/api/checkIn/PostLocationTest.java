package polls.client.api.checkIn;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.CheckinLocationViewModel;
import polls.client.model.StatusResponse;
import polls.client.api.utils.Random;
import polls.client.api.utils.UuidGenerator;
import java.util.UUID;
import java.util.List;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class PostLocationTest extends CheckInApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws ApiException {
        // given
        CheckinLocationViewModel checkinLocationViewModel = new CheckinLocationViewModel();

        checkinLocationViewModel.setLatitude(Random.nextDouble(1.00D, 100.00D));
        checkinLocationViewModel.setLongitude(Random.nextDouble(1.00D, 100.00D));
        checkinLocationViewModel.setUserId(UuidGenerator.generateUUID());

        // when
        StatusResponse response = makeRequest(checkinLocationViewModel);

        // then
        assertNotNull(response);
        assertNotNull(response.getStatus());
        assertNotNull(response.getStatusMessage());
    }

    private StatusResponse makeRequest(CheckinLocationViewModel checkinLocationViewModel) throws ApiException {
        return authRequestExecutor.execute(() -> {
            return api.postLocation(checkinLocationViewModel); 
        });
    }
}