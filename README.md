# android-polls-client

## Requirements

Building the API client library requires [Maven](https://maven.apache.org/) to be installed.

## Installation

To install the API client library to your local Maven repository, simply execute:

```shell
mvn install
```

To deploy it to a remote Maven repository instead, configure the settings of the repository and execute:

```shell
mvn deploy
```

Refer to the [official documentation](https://maven.apache.org/plugins/maven-deploy-plugin/usage.html) for more information.

### Maven users

Add this dependency to your project's POM:

```xml
<dependency>
    <groupId>polls.client</groupId>
    <artifactId>android-polls-client</artifactId>
    <version>1.0.0</version>
    <scope>compile</scope>
</dependency>
```

### Gradle users

Add this dependency to your project's build file:

```groovy
compile "polls.client:android-polls-client:1.0.0"
```

### Others

At first generate the JAR by executing:

    mvn package

Then manually install the following JARs:

* target/android-polls-client-1.0.0.jar
* target/lib/*.jar

## Getting Started

Please follow the [installation](#installation) instruction and execute the following Java code:

```java

import polls.client.*;
import polls.client.auth.*;
import polls.client.model.*;
import polls.client.api.BusinessApi;

import java.io.File;
import java.util.*;

public class BusinessApiExample {

    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        
        // Configure API key authorization: apiToken
        ApiKeyAuth apiToken = (ApiKeyAuth) defaultClient.getAuthentication("apiToken");
        apiToken.setApiKey("YOUR API KEY");
        // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
        //apiToken.setApiKeyPrefix("Token");

        // Configure API key authorization: apiUserId
        ApiKeyAuth apiUserId = (ApiKeyAuth) defaultClient.getAuthentication("apiUserId");
        apiUserId.setApiKey("YOUR API KEY");
        // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
        //apiUserId.setApiKeyPrefix("Token");

        BusinessApi apiInstance = new BusinessApi();
        CompanyUsers companyUsers = new CompanyUsers(); // CompanyUsers | 
        try {
            StatusResponse result = apiInstance.addNewEmployee(companyUsers);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling BusinessApi#addNewEmployee");
            e.printStackTrace();
        }
    }
}

```

## Documentation for API Endpoints

All URIs are relative to *http://demo.honeyshyam.com/api*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*BusinessApi* | [**addNewEmployee**](docs/BusinessApi.md#addNewEmployee) | **POST** /v1/Business/AddNewEmployee | Add Comapny Employee
*BusinessApi* | [**companyEmployees**](docs/BusinessApi.md#companyEmployees) | **POST** /v1/Business/CompanyEmployees | List of Company&#39;s employees.
*BusinessApi* | [**createCompany**](docs/BusinessApi.md#createCompany) | **POST** /v1/Business/CreateCompany | Creates the company.
*BusinessApi* | [**getPollCounters**](docs/BusinessApi.md#getPollCounters) | **POST** /v1/Business/GetPollCounters | Gets the poll counters.
*BusinessApi* | [**postPollAnswer**](docs/BusinessApi.md#postPollAnswer) | **POST** /v1/Business/PostPollAnswer | Posts the poll answer.CompanyEmployees
*BusinessApi* | [**removeEmployee**](docs/BusinessApi.md#removeEmployee) | **POST** /v1/Business/RemoveEmployee | Remove Employee.
*BusinessApi* | [**requestBusinessAccount**](docs/BusinessApi.md#requestBusinessAccount) | **POST** /v1/Business/RequestBusinessAccount | Creates the business account.
*BusinessApi* | [**responseInvitation**](docs/BusinessApi.md#responseInvitation) | **POST** /v1/Business/ResponseInvitation | Accept Decline Invitation.
*BusinessApi* | [**updateEmployee**](docs/BusinessApi.md#updateEmployee) | **POST** /v1/Business/UpdateEmployee | Updates the user.
*BusinessApi* | [**viewEmployee**](docs/BusinessApi.md#viewEmployee) | **POST** /v1/Business/ViewEmployee | Views the employee.
*CheckInApi* | [**checkinLocation**](docs/CheckInApi.md#checkinLocation) | **POST** /v1/CheckIn/CheckinLocation | Checkin Location of the user
*CheckInApi* | [**getNearByLocations**](docs/CheckInApi.md#getNearByLocations) | **POST** /v1/CheckIn/GetNearByLocations | Gets  NearBy Locations search results for the requset latitude, longiture with radius.
*CheckInApi* | [**postLocation**](docs/CheckInApi.md#postLocation) | **POST** /v1/CheckIn/PostLocation | Posts the location.
*PayApi* | [**billingHistory**](docs/PayApi.md#billingHistory) | **POST** /v1/Pay/BillingHistory | Billings the history.
*PayApi* | [**billingInvoice**](docs/PayApi.md#billingInvoice) | **POST** /v1/Pay/BillingInvoice | Billings the invoice.
*PayApi* | [**buyingHistory**](docs/PayApi.md#buyingHistory) | **POST** /v1/Pay/BuyingHistory | Buying Credits history.
*PayApi* | [**chargePayment**](docs/PayApi.md#chargePayment) | **POST** /v1/Pay/ChargePayment | Charges the payment.
*PayApi* | [**getAccessToken**](docs/PayApi.md#getAccessToken) | **POST** /v1/Pay/GetAccessToken | Verifies the authentication code.
*PayApi* | [**verifyPayment**](docs/PayApi.md#verifyPayment) | **POST** /v1/Pay/VerifyPayment | Verifies the Payment.
*PayApi* | [**windowsCollectionToken**](docs/PayApi.md#windowsCollectionToken) | **POST** /v1/Pay/WindowsCollectionToken | Gets the windows collection token.
*PollsApi* | [**bestImagePoll**](docs/PollsApi.md#bestImagePoll) | **POST** /v1/Polls/BestImagePoll | Create Best Image poll. Here you can compare two Images for any purpose.
*PollsApi* | [**cancelBillingAgreement**](docs/PollsApi.md#cancelBillingAgreement) | **POST** /v1/Polls/CancelBillingAgreement | Cancels the billing agreement.
*PollsApi* | [**changePassword**](docs/PollsApi.md#changePassword) | **POST** /v1/Polls/ChangePassword | Change Current Password
*PollsApi* | [**citiesByState**](docs/PollsApi.md#citiesByState) | **POST** /v1/Polls/CitiesByState | Cities the by state identifier or Name.
*PollsApi* | [**countries**](docs/PollsApi.md#countries) | **POST** /v1/Polls/Countries | Countries list
*PollsApi* | [**createBestTextPoll**](docs/PollsApi.md#createBestTextPoll) | **POST** /v1/Polls/CreateBestTextPoll | Creates the best text poll.
*PollsApi* | [**createSingleImagePoll**](docs/PollsApi.md#createSingleImagePoll) | **POST** /v1/Polls/CreateSingleImagePoll | Creates the Single Image Poll.
*PollsApi* | [**createTextPoll**](docs/PollsApi.md#createTextPoll) | **POST** /v1/Polls/CreateTextPoll | Create the text poll. Here you can get views e.g. Book Titles, movie title, punch line and many more.
*PollsApi* | [**deleteDraftPoll**](docs/PollsApi.md#deleteDraftPoll) | **POST** /v1/Polls/DeleteDraftPoll | Deletes the draft poll required PollId integer value
*PollsApi* | [**deletePoll**](docs/PollsApi.md#deletePoll) | **POST** /v1/Polls/DeletePoll | Deletes the poll from everywhere.
*PollsApi* | [**deleteProfilePic**](docs/PollsApi.md#deleteProfilePic) | **POST** /v1/Polls/DeleteProfilePic | Views the profile pics.
*PollsApi* | [**deleteUser**](docs/PollsApi.md#deleteUser) | **POST** /v1/Polls/DeleteUser | Deletes the user.
*PollsApi* | [**expirePoll**](docs/PollsApi.md#expirePoll) | **POST** /v1/Polls/ExpirePoll | Expires the poll Required PollId integer value.
*PollsApi* | [**extendExpireTime**](docs/PollsApi.md#extendExpireTime) | **POST** /v1/Polls/ExtendExpireTime | Extends the expire time.              Rules of extend.             1. Can only extend if not complete             2. Can only extend if last response is within 120 days.
*PollsApi* | [**filterViewByPollId**](docs/PollsApi.md#filterViewByPollId) | **POST** /v1/Polls/FilterViewByPollId | Gets Filtered View By PollId
*PollsApi* | [**fullFilterViewByPollId**](docs/PollsApi.md#fullFilterViewByPollId) | **POST** /v1/Polls/FullFilterViewByPollId | Gets Full Filter View By PollId
*PollsApi* | [**getAllDepartment**](docs/PollsApi.md#getAllDepartment) | **POST** /v1/Polls/GetAllDepartment | Gets All Department.
*PollsApi* | [**getAllFilterCategories**](docs/PollsApi.md#getAllFilterCategories) | **POST** /v1/Polls/GetAllFilterCategories | Gets all  Filter Categories.
*PollsApi* | [**getAllFilters**](docs/PollsApi.md#getAllFilters) | **POST** /v1/Polls/GetAllFilters | Gets all filters.
*PollsApi* | [**getBalance**](docs/PollsApi.md#getBalance) | **POST** /v1/Polls/GetBalance | Get Credit Balance to create Poll.
*PollsApi* | [**getCategories**](docs/PollsApi.md#getCategories) | **POST** /v1/Polls/GetCategories | Get all categories.
*PollsApi* | [**getDemographicStats**](docs/PollsApi.md#getDemographicStats) | **POST** /v1/Polls/GetDemographicStats | Gets the demographic stats and status.
*PollsApi* | [**getEducations**](docs/PollsApi.md#getEducations) | **POST** /v1/Polls/GetEducations | Get the educations.
*PollsApi* | [**getEmployeeFilterCount**](docs/PollsApi.md#getEmployeeFilterCount) | **POST** /v1/Polls/GetEmployeeFilterCount | Gets the employee filter count.
*PollsApi* | [**getFileDetails**](docs/PollsApi.md#getFileDetails) | **POST** /v1/Polls/GetFileDetails | Gets the file details.
*PollsApi* | [**getFilterDataCount**](docs/PollsApi.md#getFilterDataCount) | **POST** /v1/Polls/GetFilterDataCount | Gets the filter data count.
*PollsApi* | [**getFilterOptions**](docs/PollsApi.md#getFilterOptions) | **POST** /v1/Polls/GetFilterOptions | Gets all filters.
*PollsApi* | [**getFilterOptionsList**](docs/PollsApi.md#getFilterOptionsList) | **POST** /v1/Polls/GetFilterOptionsList | Gets the filter options list.
*PollsApi* | [**getListPublicProfile**](docs/PollsApi.md#getListPublicProfile) | **POST** /v1/Polls/GetListPublicProfile | Gets the list public profile.
*PollsApi* | [**getLoginHistory**](docs/PollsApi.md#getLoginHistory) | **POST** /v1/Polls/GetLoginHistory | Gets the login history.
*PollsApi* | [**getMetaTags**](docs/PollsApi.md#getMetaTags) | **POST** /v1/Polls/GetMetaTags | Gets the meta tags.
*PollsApi* | [**getMyAllPolls**](docs/PollsApi.md#getMyAllPolls) | **POST** /v1/Polls/GetMyAllPolls | Gets my all polls.
*PollsApi* | [**getMyBillingAgreement**](docs/PollsApi.md#getMyBillingAgreement) | **POST** /v1/Polls/GetMyBillingAgreement | Gets my billing agreement.
*PollsApi* | [**getMyPolls**](docs/PollsApi.md#getMyPolls) | **POST** /v1/Polls/GetMyPolls | Get the all Polls of the user. It will requires hearder kyes userId and token
*PollsApi* | [**getPaymentPlans**](docs/PollsApi.md#getPaymentPlans) | **POST** /v1/Polls/GetPaymentPlans | Gets the Payment Plans details.
*PollsApi* | [**getPollById**](docs/PollsApi.md#getPollById) | **POST** /v1/Polls/GetPollById | Gets the poll by identifier.
*PollsApi* | [**getPollCounters**](docs/PollsApi.md#getPollCounters) | **POST** /v1/Polls/GetPollCounters | Gets the poll counters of the user.
*PollsApi* | [**getPollResult**](docs/PollsApi.md#getPollResult) | **POST** /v1/Polls/GetPollResult | Get the data from our service. It will requires hearder kyes userId and token and key pollId
*PollsApi* | [**getPollStatus**](docs/PollsApi.md#getPollStatus) | **POST** /v1/Polls/GetPollStatus | Gets the poll status.
*PollsApi* | [**getPublicPollByQuery**](docs/PollsApi.md#getPublicPollByQuery) | **POST** /v1/Polls/GetPublicPollByQuery | Get Public Polls for the request parameter
*PollsApi* | [**getQuestions**](docs/PollsApi.md#getQuestions) | **POST** /v1/Polls/GetQuestions | Gets the questions.
*PollsApi* | [**getUserFullContact**](docs/PollsApi.md#getUserFullContact) | **POST** /v1/Polls/GetUserFullContact | Gets the user full contact.
*PollsApi* | [**getWorkerDetails**](docs/PollsApi.md#getWorkerDetails) | **POST** /v1/Polls/GetWorkerDetails | Gets the worker details.
*PollsApi* | [**listPartnerPoll**](docs/PollsApi.md#listPartnerPoll) | **POST** /v1/Polls/ListPartnerPoll | Lists the partner poll.
*PollsApi* | [**logOut**](docs/PollsApi.md#logOut) | **POST** /v1/Polls/LogOut | Logout User if successful remove any existing token, Need Header userId and token
*PollsApi* | [**login**](docs/PollsApi.md#login) | **POST** /v1/Polls/Login | No header required with \&quot;version\&quot; value for current version &#x3D; \&quot;1.0\&quot; now using /api/v1/ for version             Accepts a login json object and returns the LoginResponse object if successful.
*PollsApi* | [**makeCall**](docs/PollsApi.md#makeCall) | **POST** /v1/Polls/MakeCall | Makes the call.
*PollsApi* | [**metaTags**](docs/PollsApi.md#metaTags) | **POST** /v1/Polls/MetaTags | Save Meta tags for the poll
*PollsApi* | [**postAuth**](docs/PollsApi.md#postAuth) | **POST** /v1/Polls/PostAuth | Posts the authentication.
*PollsApi* | [**reSendVerifyEmail**](docs/PollsApi.md#reSendVerifyEmail) | **POST** /v1/Polls/ReSendVerifyEmail | Res the send verify email.
*PollsApi* | [**refreshSecretAccessKey**](docs/PollsApi.md#refreshSecretAccessKey) | **POST** /v1/Polls/RefreshSecretAccessKey | Gets the secret access key.             To Update SecretAccessKey request the method with extra header             To Refresh the SecretAccessKey request addition header values  \&quot;accessTokenKey\&quot; &#x3D; Existing Value             And refreshTokenKey &#x3D; \&quot;true\&quot;
*PollsApi* | [**registerNotifications**](docs/PollsApi.md#registerNotifications) | **POST** /v1/Polls/RegisterNotifications | Register Notifications Preference
*PollsApi* | [**registration**](docs/PollsApi.md#registration) | **POST** /v1/Polls/Registration | Register new user.
*PollsApi* | [**resendVerifyCode**](docs/PollsApi.md#resendVerifyCode) | **POST** /v1/Polls/ResendVerifyCode | Resend the verify code.
*PollsApi* | [**resetPassword**](docs/PollsApi.md#resetPassword) | **POST** /v1/Polls/ResetPassword | Resets the password.
*PollsApi* | [**resetPasswordRequest**](docs/PollsApi.md#resetPasswordRequest) | **POST** /v1/Polls/ResetPasswordRequest | Reset Password Request
*PollsApi* | [**saveBillingAgreement**](docs/PollsApi.md#saveBillingAgreement) | **POST** /v1/Polls/SaveBillingAgreement | Saves the billing agreement.
*PollsApi* | [**saveRemoteImage**](docs/PollsApi.md#saveRemoteImage) | **POST** /v1/Polls/SaveRemoteImage | Saves the remote image.
*PollsApi* | [**statesByCountry**](docs/PollsApi.md#statesByCountry) | **POST** /v1/Polls/StatesByCountry | States by country identifier or Name.
*PollsApi* | [**thumbsUpDown**](docs/PollsApi.md#thumbsUpDown) | **POST** /v1/Polls/ThumbsUpDown | Thumbs up down Selection [ThumbsUp&#x3D;1, otherwize ThumbsDown&#x3D;0].
*PollsApi* | [**updateProfile**](docs/PollsApi.md#updateProfile) | **POST** /v1/Polls/UpdateProfile | Update User Profile
*PollsApi* | [**updatePublishedPoll**](docs/PollsApi.md#updatePublishedPoll) | **POST** /v1/Polls/UpdatePublishedPoll | Update Published Poll Keywords and Description.
*PollsApi* | [**uploadFile**](docs/PollsApi.md#uploadFile) | **POST** /v1/Polls/UploadFile | Post File to server will return filename saved, for Profile image send header isProfilePic&#x3D;true
*PollsApi* | [**verifyEmail**](docs/PollsApi.md#verifyEmail) | **POST** /v1/Polls/VerifyEmail | Verify Email Addresss for the account
*PollsApi* | [**verifyPhone**](docs/PollsApi.md#verifyPhone) | **POST** /v1/Polls/VerifyPhone | Verifies Phone with the code received over phone to use your account.
*PollsApi* | [**viewNotificationsPreference**](docs/PollsApi.md#viewNotificationsPreference) | **POST** /v1/Polls/ViewNotificationsPreference | Register Notifications Preference
*PollsApi* | [**viewProfile**](docs/PollsApi.md#viewProfile) | **POST** /v1/Polls/ViewProfile | View User Profile details for the unique identifier, if the response exists  pictureId info, and for retrieve filename, you need to remove all (-) and add file extention to .png all files on server are saved into png format.
*PollsApi* | [**viewPublicProfile**](docs/PollsApi.md#viewPublicProfile) | **POST** /v1/Polls/ViewPublicProfile | View Public Profile details for the unique identifier, rest follow the ViewProfile.
*PublicPollApi* | [**getPollResult**](docs/PublicPollApi.md#getPollResult) | **POST** /v1/PublicPoll/GetPollResult | Get the data from our service. It will requires hearder kyes userId and token and key pollId
*PublicPollApi* | [**getPublic**](docs/PublicPollApi.md#getPublic) | **POST** /v1/PublicPoll/GetPublic | This is a public API to Get Public Polls for the request parameter             View all Public polls, View by Category, View by Username etc.
*PublicPollApi* | [**listPublicUsers**](docs/PublicPollApi.md#listPublicUsers) | **POST** /v1/PublicPoll/ListPublicUsers | Lists the public users.
*PublicPollApi* | [**viewPublicProfile**](docs/PublicPollApi.md#viewPublicProfile) | **POST** /v1/PublicPoll/ViewPublicProfile | View Public Profile details for the unique identifier, rest follow the ViewProfile.
*PushApi* | [**activeNotifications**](docs/PushApi.md#activeNotifications) | **POST** /v1/Push/ActiveNotifications | Gets the active custom notifications.
*PushApi* | [**listNotificationAlerts**](docs/PushApi.md#listNotificationAlerts) | **POST** /v1/Push/ListNotificationAlerts | Lists the notification alerts.
*PushApi* | [**registerSnsDevice**](docs/PushApi.md#registerSnsDevice) | **POST** /v1/Push/RegisterSnsDevice | Registers the SNS/Push Notification device.
*PushApi* | [**sendWebPushNotificationsFCMTest**](docs/PushApi.md#sendWebPushNotificationsFCMTest) | **POST** /v1/Push/SendWebPushNotificationsFCMTest | Sends the web push notifications FCM test.
*PushApi* | [**sendWebPushNotificationsTest**](docs/PushApi.md#sendWebPushNotificationsTest) | **POST** /v1/Push/SendWebPushNotificationsTest | Sends the web push notifications test.
*PushApi* | [**updateNotification**](docs/PushApi.md#updateNotification) | **POST** /v1/Push/UpdateNotification | Updates the notification.
*TokenApi* | [**rooms**](docs/TokenApi.md#rooms) | **POST** /v1/Token/Rooms | 
*TokenApi* | [**twilioToken**](docs/TokenApi.md#twilioToken) | **POST** /v1/Token/TwilioToken | Get TwilioToken to make a Audio/Video connection


## Documentation for Models

 - [AccessTokenViewModel](docs/AccessTokenViewModel.md)
 - [AgreementViewModel](docs/AgreementViewModel.md)
 - [AnswerModel](docs/AnswerModel.md)
 - [ApiLoginModel](docs/ApiLoginModel.md)
 - [AuthTokenViewModel](docs/AuthTokenViewModel.md)
 - [BestImage](docs/BestImage.md)
 - [BestText](docs/BestText.md)
 - [BillingAgreementViewModel](docs/BillingAgreementViewModel.md)
 - [BillingHistoryModel](docs/BillingHistoryModel.md)
 - [BillingInvoiceModel](docs/BillingInvoiceModel.md)
 - [CategoryList](docs/CategoryList.md)
 - [ChangePasswordModel](docs/ChangePasswordModel.md)
 - [CheckinLocationModel](docs/CheckinLocationModel.md)
 - [CheckinLocationViewModel](docs/CheckinLocationViewModel.md)
 - [CitiesModel](docs/CitiesModel.md)
 - [CompanyUsers](docs/CompanyUsers.md)
 - [Contact](docs/Contact.md)
 - [ContactList](docs/ContactList.md)
 - [CountriesModel](docs/CountriesModel.md)
 - [CountriesModelList](docs/CountriesModelList.md)
 - [CustomNotification](docs/CustomNotification.md)
 - [CustomNotificationViewModel](docs/CustomNotificationViewModel.md)
 - [DemographicStats](docs/DemographicStats.md)
 - [DepartmentModel](docs/DepartmentModel.md)
 - [Device](docs/Device.md)
 - [DeviceInfoModel](docs/DeviceInfoModel.md)
 - [DeviceModel](docs/DeviceModel.md)
 - [EducationList](docs/EducationList.md)
 - [EducationModel](docs/EducationModel.md)
 - [EmailViewModel](docs/EmailViewModel.md)
 - [ErrorResponse](docs/ErrorResponse.md)
 - [ExtendPollRequest](docs/ExtendPollRequest.md)
 - [ExternalProviderRequest](docs/ExternalProviderRequest.md)
 - [FileObject](docs/FileObject.md)
 - [FileRequest](docs/FileRequest.md)
 - [FilterCategoryModel](docs/FilterCategoryModel.md)
 - [FilterCountModel](docs/FilterCountModel.md)
 - [FilterData](docs/FilterData.md)
 - [FilterDataCount](docs/FilterDataCount.md)
 - [FilterDataModel](docs/FilterDataModel.md)
 - [FilterPairs](docs/FilterPairs.md)
 - [FilterRequest](docs/FilterRequest.md)
 - [FilterRequestObject](docs/FilterRequestObject.md)
 - [FilterResp](docs/FilterResp.md)
 - [FilterValues](docs/FilterValues.md)
 - [FullContact](docs/FullContact.md)
 - [FullContactModel](docs/FullContactModel.md)
 - [GCMViewModel](docs/GCMViewModel.md)
 - [Geometry](docs/Geometry.md)
 - [GoogleLocationViewModel](docs/GoogleLocationViewModel.md)
 - [HttpContent](docs/HttpContent.md)
 - [HttpMethod](docs/HttpMethod.md)
 - [HttpRequestMessage](docs/HttpRequestMessage.md)
 - [HttpResponseMessage](docs/HttpResponseMessage.md)
 - [InvoiceRequest](docs/InvoiceRequest.md)
 - [ListBillingHistory](docs/ListBillingHistory.md)
 - [ListCustomNotifications](docs/ListCustomNotifications.md)
 - [ListPoll](docs/ListPoll.md)
 - [ListPollModel](docs/ListPollModel.md)
 - [ListPollView](docs/ListPollView.md)
 - [Location](docs/Location.md)
 - [LocationViewModel](docs/LocationViewModel.md)
 - [LocationsList](docs/LocationsList.md)
 - [LoginResponse](docs/LoginResponse.md)
 - [LogoutResponse](docs/LogoutResponse.md)
 - [MetaTagViewModel](docs/MetaTagViewModel.md)
 - [Northeast](docs/Northeast.md)
 - [NotificationTrackingModel](docs/NotificationTrackingModel.md)
 - [NotificationViewModel](docs/NotificationViewModel.md)
 - [Obligation](docs/Obligation.md)
 - [PayResponse](docs/PayResponse.md)
 - [PaymentPlanViewModel](docs/PaymentPlanViewModel.md)
 - [PaymentPlansList](docs/PaymentPlansList.md)
 - [Photo](docs/Photo.md)
 - [PollRequest](docs/PollRequest.md)
 - [PollResponse](docs/PollResponse.md)
 - [PollResult](docs/PollResult.md)
 - [PollsStatsCounts](docs/PollsStatsCounts.md)
 - [ProfileViewModel](docs/ProfileViewModel.md)
 - [PwdResetResponse](docs/PwdResetResponse.md)
 - [RegisterDeviceSNS](docs/RegisterDeviceSNS.md)
 - [RegisterResponse](docs/RegisterResponse.md)
 - [RegisterViewModel](docs/RegisterViewModel.md)
 - [RemoteImageViewModel](docs/RemoteImageViewModel.md)
 - [RequestId](docs/RequestId.md)
 - [RequestNameModel](docs/RequestNameModel.md)
 - [RequestPollId](docs/RequestPollId.md)
 - [ResetPassword](docs/ResetPassword.md)
 - [ResetPasswordRequest](docs/ResetPasswordRequest.md)
 - [Result](docs/Result.md)
 - [SearchOptions](docs/SearchOptions.md)
 - [SendMessageViewModel](docs/SendMessageViewModel.md)
 - [SingleImage](docs/SingleImage.md)
 - [SingleText](docs/SingleText.md)
 - [Southwest](docs/Southwest.md)
 - [StatesModel](docs/StatesModel.md)
 - [StatusResponse](docs/StatusResponse.md)
 - [StripeViewModel](docs/StripeViewModel.md)
 - [SuggestQuestionViewModel](docs/SuggestQuestionViewModel.md)
 - [ThumbsRequest](docs/ThumbsRequest.md)
 - [ThumbsResponse](docs/ThumbsResponse.md)
 - [TokenContent](docs/TokenContent.md)
 - [TwilioVideoViewModel](docs/TwilioVideoViewModel.md)
 - [UpdatePollRequest](docs/UpdatePollRequest.md)
 - [User](docs/User.md)
 - [UserCreditModel](docs/UserCreditModel.md)
 - [UserIdModel](docs/UserIdModel.md)
 - [UserInvitation](docs/UserInvitation.md)
 - [UserRelation](docs/UserRelation.md)
 - [VerifyCodeViewModel](docs/VerifyCodeViewModel.md)
 - [VerifyPaymentViewModel](docs/VerifyPaymentViewModel.md)
 - [VerifyViewModel](docs/VerifyViewModel.md)
 - [Version](docs/Version.md)
 - [Viewport](docs/Viewport.md)
 - [WorkerApiModel](docs/WorkerApiModel.md)
 - [WorkerRequest](docs/WorkerRequest.md)


## Documentation for Authorization

Authentication schemes defined for the API:
### apiToken

- **Type**: API key
- **API key parameter name**: token
- **Location**: HTTP header

### apiUserId

- **Type**: API key
- **API key parameter name**: userId
- **Location**: HTTP header


## Recommendation

It's recommended to create an instance of `ApiClient` per thread in a multithreaded environment to avoid any potential issues.

## Author



